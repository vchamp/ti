#define ORBITER_MODULE

#include "PegasusAres.h"

GDIParams g_Param;
//InputCallbackParams gInputCallbackParams;

DLLCLBK void InitModule(HINSTANCE hModule) {
	g_Param.hDLL = hModule;
	//oapiRegisterCustomControls(hModule);

	g_Param.hFont[0] = CreateFont(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[1] = CreateFont(20, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[2] = CreateFont(14, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[3] = CreateFont(16, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[4] = CreateFont(10, 0, 0, 0, FW_LIGHT, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");

	g_Param.hPen[0] = CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
	g_Param.hPen[1] = CreatePen(PS_SOLID, 1, RGB(150, 150, 150));
	g_Param.hPen[2] = CreatePen(PS_SOLID, 1, RGB(150, 150, 0));

	g_Param.hBrush[0] = CreateSolidBrush(RGB(0, 0, 0));
	g_Param.hBrush[1] = CreateSolidBrush(RGB(0, 255, 0));
	g_Param.hBrush[2] = CreateSolidBrush(RGB(100, 100, 255));
}

// add vessel-specific pages into scenario editor
/*DLLCLBK void secInit(HWND hEditor, OBJHANDLE hVessel){
	PayloadManager::DefineScenarioEditorPage(hEditor, hVessel);
}*/

DLLCLBK void ExitModule(HINSTANCE hModule) {
	int i;
	// deallocate GDI resources
	for (i = 0; i < 5; i++) DeleteObject (g_Param.hFont[i]);
	for (i = 0; i < 3; i++) DeleteObject (g_Param.hPen[i]);
	for (i = 0; i < 3; i++) DeleteObject (g_Param.hBrush[i]);

	//oapiUnregisterCustomControls(g_Param.hDLL);
}

DLLCLBK VESSEL *ovcInit(OBJHANDLE hvessel, int flightmodel) {
	return new PegasusAres(hvessel, flightmodel);
}

DLLCLBK void ovcExit (VESSEL *vessel) {
	if (vessel) delete (PegasusAres*)vessel;
}

// --------------------------------------------------------------
// Constructor
// --------------------------------------------------------------
PegasusAres::PegasusAres(OBJHANDLE hVessel, int flightmodel) : VESSEL2 (hVessel, flightmodel) { // : baseClass(hVessel, flightmodel)
	for (int i = 0; i < nsurf; i++) {
		srf[i] = NULL;
	}

	gearStatus = CLOSED;
	gearProc = 0.0;
	chuteStatus = CLOSED;
	heatShieldStatus = OPEN;

	dockScreenStatus = CLOSED;
	dockScreenProc = 0.0;

	altitude = -1.0;
	vspeed = 0.0;
	groundAltitude = -1.0;
	azimuth = 0.0f;
	thrustIncIndex = 4;
	thrustIncStatus = STOPPED;
	dAutoThrust = 0;
	vspeedAlert = false;
	orientationSpeedVector = true;
	gearJettisonArmed = false;
	powerOn = true;
	damageAlert = false;

	dsSelectedVessel = NULL;
	dsSelectedDockIndex = -1;
	dsDockDist = 0;
	dsDockPos = _V(0, 0, 0);
	dsDockVel = _V(0, 0, 0);
	dsPosition = true;

	Message msg;
	msg.message = "Welcome onboard";
	msg.color = RGB(0, 255, 0);
	messages.push_back(msg);

	DefineAnimations();

	// UMMU
	ummuInitReturnCode = 1;
	selectedCrewMember = 0;
	selectedSuit = 0;
}

// --------------------------------------------------------------
// Destructor
// --------------------------------------------------------------
PegasusAres::~PegasusAres() {
	ReleaseSurfaces();
}

void PegasusAres::DefineAnimations() {
	// main thrust
	/*static UINT mainThrustGr[1] = {GR_THRUST_LEVER};
	static MGROUP_TRANSLATE mainThrustTr (1, mainThrustGr, 1, _V(0, 0, 0.156));
	animThrustLever = CreateAnimation(0);
	AddAnimationComponent(animThrustLever, 0, 1, &mainThrustTr);*/

	meshVC = oapiLoadMeshGlobal("TotalImmersion\\PegasusAresVC");

	// save initial positions of buttons
	MESHGROUP* gr = oapiMeshGroup(meshVC, GR_BTN_KILLROT);
	initPos[GR_BTN_KILLROT] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_PROGRADE);
	initPos[GR_BTN_PROGRADE] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_RETROGRADE);
	initPos[GR_BTN_RETROGRADE] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_ORBNORMAL);
	initPos[GR_BTN_ORBNORMAL] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_ORBANORMAL);
	initPos[GR_BTN_ORBANORMAL] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_HOLDALTITUDE);
	initPos[GR_BTN_HOLDALTITUDE] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);

	VECTOR3 vertex, axis;

	// rcs on/off
	gr = oapiMeshGroup(meshVC, GR_ATT_ONOFF);
	vertex = _V(gr->Vtx[27].x, gr->Vtx[27].y, gr->Vtx[27].z);
	static UINT rcsOnOffGr[1] = {GR_ATT_ONOFF};
	static MGROUP_ROTATE rcsOnOffTr (1, rcsOnOffGr, 1, vertex, _V(1, 0, 0), (float) (-60 * RAD));
	animRCSOnOff = CreateAnimation(0.5);
	AddAnimationComponent(animRCSOnOff, 0, 1, &rcsOnOffTr);

	// rcs mode
	gr = oapiMeshGroup(meshVC, GR_ATT_MODE);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	axis = _V(0, 0, 1);
	RotateVector(axis, _V(1, 0, 0), 86.528 * RAD);
	static UINT rcsModeGr[1] = {GR_ATT_MODE};
	static MGROUP_ROTATE rcsModeTr (1, rcsModeGr, 1, vertex, axis, (float) (90 * RAD));
	animRCSMode = CreateAnimation(0.5);
	AddAnimationComponent(animRCSMode, 0, 1, &rcsModeTr);

	// thrust inc step
	gr = oapiMeshGroup(meshVC, GR_THRUST_INC_STEP);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	axis = _V(0, 0, 1);
	RotateVector(axis, _V(1, 0, 0), 86.662 * RAD);
	static UINT thrIncStepGr[1] = {GR_THRUST_INC_STEP};
	static MGROUP_ROTATE thrIncStepTr (1, thrIncStepGr, 1, vertex, axis, (float) (150 * RAD));
	animThrustIncStep = CreateAnimation(0.5);
	AddAnimationComponent(animThrustIncStep, 0, 1, &thrIncStepTr);

	// thrust inc
	gr = oapiMeshGroup(meshVC, GR_THRUST_INC_STEP);
	vertex = _V(gr->Vtx[14].x, gr->Vtx[14].y, gr->Vtx[14].z);
	static UINT thrIncGr[1] = {GR_THRUST_INC};
	static MGROUP_ROTATE thrIncTr (1, thrIncGr, 1, vertex, _V(1, 0, 0), (float) (-10 * RAD));
	animThrustInc = CreateAnimation(0.5);
	AddAnimationComponent(animThrustInc, 0, 1, &thrIncTr);

	// rcs on/off
	gr = oapiMeshGroup(meshVC, GR_ORIENTATION_MODE);
	vertex = _V(gr->Vtx[27].x, gr->Vtx[27].y, gr->Vtx[27].z);
	axis = _V(0, 1, 0);
	RotateVector(axis, _V(1, 0, 0), -86.471 * RAD);
	static UINT orientModeGr[1] = {GR_ORIENTATION_MODE};
	static MGROUP_ROTATE orientModeTr (1, orientModeGr, 1, vertex, axis, (float) (-60 * RAD));
	animOrientationMode = CreateAnimation(0.5);
	AddAnimationComponent(animOrientationMode, 0, 1, &orientModeTr);

	// gear jettison cover
	gr = oapiMeshGroup(meshVC, GR_GEAR_JETTISON);
	vertex = _V(gr->Vtx[7].x, gr->Vtx[7].y, gr->Vtx[7].z);
	static UINT gearJettGr[1] = {GR_GEAR_JETTISON};
	static MGROUP_ROTATE gearJettTr (1, gearJettGr, 1, vertex, _V(1, 0, 0), (float) (-120 * RAD));
	animGearJettisonCover = CreateAnimation(0);
	AddAnimationComponent(animGearJettisonCover, 0, 1, &gearJettTr);

	// gear
	animGear = CreateAnimation(1);
	static UINT leg1Gr[24] = {32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 
		70, 71, 72, 73, 74, 75, 77, 78, 79, 80, 81, 82};
	axis = _V(3.601, 4.956, 0);
	RotateVector(axis, _V(0, 0, 1), PI / 2);
	static MGROUP_ROTATE leg1Tr (2, leg1Gr, 24, _V(2.048, 2.819, -1.99), axis / length(axis), (float) (149 * RAD));
	AddAnimationComponent(animGear, 0, 1, &leg1Tr);
	static UINT leg2Gr[12] = {57, 58, 59, 60, 61, 62, 64, 65, 66, 67, 68, 69};
	axis = _V(-3.601, 4.956, 0);
	RotateVector(axis, _V(0, 0, 1), PI / 2);
	static MGROUP_ROTATE leg2Tr (2, leg2Gr, 12, _V(-2.048, 2.819, -1.99), axis / length(axis), (float) (149 * RAD));
	AddAnimationComponent(animGear, 0, 1, &leg2Tr);
	static UINT leg3Gr[12] = {44, 45, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56};
	axis = _V(-5.827, -1.893, 0);
	RotateVector(axis, _V(0, 0, 1), PI / 2);
	static MGROUP_ROTATE leg3Tr (2, leg3Gr, 12, _V(-3.314, -1.077, -1.99), axis / length(axis), (float) (149 * RAD));
	AddAnimationComponent(animGear, 0, 1, &leg3Tr);
	static UINT leg4Gr[12] = {0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12};
	static MGROUP_ROTATE leg4Tr (2, leg4Gr, 12, _V(0, -3.485, -1.99), _V(1, 0, 0), (float) (149 * RAD));
	AddAnimationComponent(animGear, 0, 1, &leg4Tr);
	static UINT leg5Gr[12] = {83, 84, 85, 86, 87, 88, 90, 91, 92, 93, 94, 95};
	axis = _V(5.827, -1.893, 0);
	RotateVector(axis, _V(0, 0, 1), PI / 2);
	static MGROUP_ROTATE leg5Tr (2, leg5Gr, 12, _V(3.314, -1.077, -1.99), axis / length(axis), (float) (149 * RAD));
	AddAnimationComponent(animGear, 0, 1, &leg5Tr);

	// dock screen
	gr = oapiMeshGroup(meshVC, GR_DOCK_SCREEN_COVER);
	vertex = _V(gr->Vtx[6].x, gr->Vtx[6].y, gr->Vtx[6].z);
	axis = _V(1, 0, 0);
	RotateVector(axis, _V(0, 0, 1), -17.434 * RAD);
	static UINT dockScreenGr[2] = {GR_DOCK_SCREEN_COVER, GR_DOCK_SCREEN};
	static MGROUP_ROTATE dockScreenTr (1, dockScreenGr, 2, vertex, axis, (float) (-120 * RAD));
	animDockScreenCover = CreateAnimation(0);
	AddAnimationComponent(animDockScreenCover, 0, 1, &dockScreenTr);
}

void PegasusAres::ReleaseSurfaces() {
	for (int i = 0; i < nsurf; i++) {
		if (srf[i]) {
			oapiDestroySurface(srf[i]);
			srf[i] = NULL;
		}
	}
}

void PegasusAres::clbkSetClassCaps (FILEHANDLE cfg) {
	double rcsThrust, isp;

	SetEmptyMass(MASS_DRY_MAX);
	SetSize(10);
	SetPMI(_V(12, 12, 5));
	//SetCW(1, 1, 1, 1);
	//SetCrossSections(CROSS_SECTIONS_CAPSULE);
	//SetRotDrag(_V(0.2, 0.2, 0.1));
	SetMeshVisibilityMode(AddMesh(oapiLoadMeshGlobal("P_Ares\\P_Ares")), MESHVIS_EXTERNAL);
	ph_main = CreatePropellantResource(MASS_FUEL);
	rcsThrust = 5000;
	isp = 3840;

	//vertAirfoil = CreateAirfoil2(LIFT_VERTICAL, _V(0, 0, 0), VLiftCoeff, 1.4, 16, 1); // 1 1 1
	//horzAirfoil = CreateAirfoil2(LIFT_HORIZONTAL, _V(0, 0, 0), HLiftCoeff, 1.4, 16, 1); // 1 1 1

	SetSurfaceFrictionCoeff(0.5, 0.5);

	SetCameraRotationRange(RAD*170, RAD*170, RAD*80, RAD*80);

	SetMeshVisibilityMode(AddMesh(meshVC), MESHVIS_VC);
	AddMesh(oapiLoadMeshGlobal("P_Ares\\P_Ares-geardn"));
	VECTOR3 ofs = _V(0, 0, 5);
	AddMesh(oapiLoadMeshGlobal("P_Ares\\P_Ares-chute"), &ofs);
	AddMesh(oapiLoadMeshGlobal("P_Ares\\P_Ares-heat"));
/*	MESHHANDLE meshSuit = oapiLoadMeshGlobal("UMmu\\UMmu");
	ofs = _V(2.08, -0.626, 2.579);
	AddMesh(meshSuit, &ofs);
	ofs = _V(1.589, -1.228, 2.579);
	AddMesh(meshSuit, &ofs);
	ofs = _V(1.175, -1.893, 2.579);
	AddMesh(meshSuit, &ofs);*/

	SetDockParams(_V(0, 0, 7.88), _V(0,0,1), _V(0,1,0));
	SetTouchdownPoints(TDP_GEARDOWN, 6);

	th_main[0] = CreateThruster(_V(0,0,-5.3), _V(0,0,1), MAIN_THRUST, ph_main, isp);
	AddExhaust(th_main[0], 20, 5, _V(0,0,-3.5), _V(0,0,-1), oapiRegisterExhaustTexture("exhaust_crcs"));

	PARTICLESTREAMSPEC contrail = {
		0, 5.0, 8, 100, 0.15, 1.0, 5, 3.0, PARTICLESTREAMSPEC::DIFFUSE,
		PARTICLESTREAMSPEC::LVL_PSQRT, 0, 2,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-4, 1
	};
	AddExhaustStream(th_main[0], _V(0, 0, -7.3), &contrail);

	PARTICLESTREAMSPEC exhaust = {
		0, 2.0, 10, 150, 0.05, 0.05, 8, 1.0, PARTICLESTREAMSPEC::EMISSIVE,
		PARTICLESTREAMSPEC::LVL_SQRT, 0, 1,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-5, 0.1
	};
	AddExhaustStream(th_main[0], _V(0, 0, -6.3), &exhaust);

	thg_main = CreateThrusterGroup(th_main, 1, THGROUP_MAIN);

	// attitude thrusters
	THRUSTER_HANDLE th_att[4];
	double lscale = 2;
	double wscale = 0.2;

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(0, 0, 1));
	th_att[1] = CreateThruster(_V(0, -2.75, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, -2.75, 2.91), _V(0, 0, -1));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_PITCHUP);

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(0, 0, -1));
	th_att[1] = CreateThruster(_V(0, -2.75, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, -2.75, 2.91), _V(0, 0, 1));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_PITCHDOWN);
	th_pitch_down[0] = th_att[0];
	th_pitch_down[1] = th_att[1];

	th_att[0] = CreateThruster(_V(2.75, 0, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 0, -1));
	th_att[1] = CreateThruster(_V(-2.75, 0, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 0, -1));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_YAWLEFT);

	th_att[0] = CreateThruster(_V(2.75, 0, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 0, 1));
	th_att[1] = CreateThruster(_V(-2.75, 0, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 0, -1));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_YAWRIGHT);

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(-1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(1, 0, 0));
	th_att[1] = CreateThruster(_V(2.75, 0, 0), _V(0, 1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(2.75, 0, 2.91), _V(0, -1, 0));
	th_att[2] = CreateThruster(_V(0, -2.75, 0), _V(1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[2], lscale, wscale, _V(0, -2.75, 2.91), _V(-1, 0, 0));
	th_att[3] = CreateThruster(_V(-2.75, 0, 0), _V(0, -1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[3], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 1, 0));
	CreateThrusterGroup(th_att, 4, THGROUP_ATT_BANKLEFT);

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(-1, 0, 0));
	th_att[1] = CreateThruster(_V(2.75, 0, 0), _V(0, -1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 1, 0));
	th_att[2] = CreateThruster(_V(0, -2.75, 0), _V(-1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[2], lscale, wscale, _V(0, -2.75, 2.91), _V(1, 0, 0));
	th_att[3] = CreateThruster(_V(-2.75, 0, 0), _V(0, 1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[3], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, -1, 0));
	CreateThrusterGroup(th_att, 4, THGROUP_ATT_BANKRIGHT);

	th_att[0] = CreateThruster(_V(0, -2.75, 0), _V(1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, -2.75, 2.91), _V(-1, 0, 0));
	th_att[1] = CreateThruster(_V(0, 2.75, 0), _V(1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, 2.75, 2.91), _V(-1, 0, 0));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_RIGHT);

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(-1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(1, 0, 0));
	th_att[1] = CreateThruster(_V(0, -2.75, 0), _V(-1, 0, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, -2.75, 2.91), _V(1, 0, 0));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_LEFT);

	th_att[0] = CreateThruster(_V(2.75, 0, 0), _V(0, 1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(2.75, 0, 2.91), _V(0, -1, 0));
	th_att[1] = CreateThruster(_V(-2.75, 0, 0), _V(0, 1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, -1, 0));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_UP);

	th_att[0] = CreateThruster(_V(-2.75, 0, 0), _V(0, -1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 1, 0));
	th_att[1] = CreateThruster(_V(2.75, 0, 0), _V(0, -1, 0), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 1, 0));
	CreateThrusterGroup(th_att, 2, THGROUP_ATT_DOWN);

	th_att[0] = CreateThruster(_V(0, -2.75, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, -2.75, 2.91), _V(0, 0, -1));
	th_att[1] = CreateThruster(_V(0, 2.75, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, 2.75, 2.91), _V(0, 0, -1));
	th_att[2] = CreateThruster(_V(2.75, 0, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[2], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 0, -1));
	th_att[3] = CreateThruster(_V(-2.75, 0, 0), _V(0, 0, 1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[3], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 0, -1));
	CreateThrusterGroup(th_att, 4, THGROUP_ATT_FORWARD);

	th_att[0] = CreateThruster(_V(0, 2.75, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[0], lscale, wscale, _V(0, 2.75, 2.91), _V(0, 0, 1));
	th_att[1] = CreateThruster(_V(0, -2.75, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[1], lscale, wscale, _V(0, -2.75, 2.91), _V(0, 0, 1));
	th_att[2] = CreateThruster(_V(-2.75, 0, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[2], lscale, wscale, _V(-2.75, 0, 2.91), _V(0, 0, 1));
	th_att[3] = CreateThruster(_V(2.75, 0, 0), _V(0, 0, -1), rcsThrust, ph_main, isp);
	AddExhaust(th_att[3], lscale, wscale, _V(2.75, 0, 2.91), _V(0, 0, 1));
	CreateThrusterGroup(th_att, 4, THGROUP_ATT_BACK);

	// UMMU
	InitUmmu();
}

// ==============================================================
// Final initialization. Called after SetClassCaps and LoadState.
// ==============================================================
void PegasusAres::clbkPostCreation() {
	//soundId = ConnectToOrbiterSoundDLL3(GetHandle());
	//ReplaceStockSound3(MyID, "Sound\\Bullet\\main.wav", REPLACE_MAIN_THRUST);

	groundContact = GroundContact();
	altitude = GetAltitude();
	groundAltitude = GetAltitude(ALTMODE_GROUND) - ORIGIN_GROUND_HEIGHT;

	if (gearStatus != STOPPED) {
		SetMeshVisibilityMode(2, MESHVIS_EXTERNAL);
		SetAnimation(animGear, gearProc);
	} else {
		SetMeshVisibilityMode(2, MESHVIS_NEVER);
	}
	if (gearStatus != OPEN && !groundContact) {
		SetTouchdownPoints(TDP_GEARUP, 6);
	}

	if (chuteStatus == OPEN) {
		SetMeshVisibilityMode(3, MESHVIS_EXTERNAL);
		//SetPMI(_V(4, 4, 2));
	} else {
		SetMeshVisibilityMode(3, MESHVIS_NEVER);
	}

	SetMeshVisibilityMode(4, heatShieldStatus == OPEN ? MESHVIS_EXTERNAL : MESHVIS_NEVER);

	UpdateEmptyMass();
	UpdateAirfoils();
	UpdateCrossSections();
	UpdateDragElement();
	UpdateRotDrag();

/*	int menOnboard = crew.GetCrewTotalNumber();
	SetMeshVisibilityMode(5, menOnboard > 0 ? MESHVIS_VC : MESHVIS_NEVER);
	SetMeshVisibilityMode(6, menOnboard > 1 ? MESHVIS_VC : MESHVIS_NEVER);
	SetMeshVisibilityMode(7, menOnboard > 2 ? MESHVIS_VC : MESHVIS_NEVER);*/

	if (!groundContact) {
		crew.DefineAirLockShape(TRUE, -2, 2, -2, 2, 7, 10);
		crew.SetMembersPosRotOnEVA(_V(0,0,8.5), _V(0,0,0));
	}

	SetAnimation(animGear, gearProc);

	hNote = oapiCreateAnnotation(true, 1, _V(1, 0.8, 0));
	oapiAnnotationSetPos(hNote, 0.1, 0.4, 0.9, 0.9);
}

void PegasusAres::clbkVisualCreated(VISHANDLE vis, int refcount) {
	// suits
/*	int menOnboard = crew.GetCrewTotalNumber();
	MESHGROUP_TRANSFORM transform1, transform2;
	transform1.transform = transform2.transform = MESHGROUP_TRANSFORM::ROTATE;
	transform1.ngrp = transform2.ngrp = -1;
	transform1.P.rotparam.ref = transform2.P.rotparam.ref = _V(0, 0, 0);
	transform1.P.rotparam.axis = _V(1, 0, 0);
	transform2.P.rotparam.axis = _V(0, 0, 1);
	transform1.P.rotparam.angle = (float) (90 * RAD);
	transform2.P.rotparam.angle = (float) (135 * RAD); // 143

	transform1.nmesh = transform2.nmesh = 5;
	MeshgroupTransform(vis, transform1);
	MeshgroupTransform(vis, transform2);

	transform1.nmesh = transform2.nmesh = 6;
	MeshgroupTransform(vis, transform1);
	MeshgroupTransform(vis, transform2);

	transform1.nmesh = transform2.nmesh = 7;
	MeshgroupTransform(vis, transform1);
	MeshgroupTransform(vis, transform2);*/

	// navmode
	UpdateNavmodeButton(NAVMODE_KILLROT, GR_BTN_KILLROT);
	UpdateNavmodeButton(NAVMODE_PROGRADE, GR_BTN_PROGRADE);
	UpdateNavmodeButton(NAVMODE_RETROGRADE, GR_BTN_RETROGRADE);
	UpdateNavmodeButton(NAVMODE_NORMAL, GR_BTN_ORBNORMAL);
	UpdateNavmodeButton(NAVMODE_ANTINORMAL, GR_BTN_ORBANORMAL);
	UpdateNavmodeButton(NAVMODE_HOLDALT, GR_BTN_HOLDALTITUDE);
}

// ==============================================================
// Write status to scenario file
// ==============================================================
void PegasusAres::clbkSaveState (FILEHANDLE scn) {
	VESSEL2::clbkSaveState(scn);
	
	char buf[100];

	// animations
	sprintf(buf, "%d %lf", gearStatus, gearProc);
	oapiWriteScenario_string(scn, "GEAR", buf);
	sprintf(buf, "%d", chuteStatus);
	oapiWriteScenario_string(scn, "CHUTE", buf);
	sprintf(buf, "%d", heatShieldStatus);
	oapiWriteScenario_string(scn, "HEATSHIELD", buf);
	sprintf(buf, "%d", powerOn);
	oapiWriteScenario_string(scn, "POWER", buf);
	sprintf(buf, "%d %d %d %d %d %d %d", (int)heatShieldTemperature,
		(int)temperatures[0], (int)temperatures[1], (int)temperatures[2], (int)temperatures[3], (int)temperatures[4], (int)temperatures[5]);
	oapiWriteScenario_string(scn, "TEMPERATURES", buf);
	sprintf(buf, "%d %d %d %d %d %d %d", (int)heatShieldStrength,
		(int)strength[0], (int)strength[1], (int)strength[2], (int)strength[3], (int)strength[4], (int)strength[5]);
	oapiWriteScenario_string(scn, "STRENGTH", buf);

	crew.SaveAllMembersInOrbiterScenarios(scn);
}

// ==============================================================
// Read status from scenario file. Called after SetClassCaps.
// ==============================================================
void PegasusAres::clbkLoadStateEx (FILEHANDLE scn, void *status) {
	char *line;
	while (oapiReadScenario_nextline(scn, line)) {
		if (!strnicmp(line, "GEAR", 4)) {
			sscanf(line + 4, "%d %lf", &gearStatus, &gearProc);
		} else if (!strnicmp(line, "CHUTE", 5)) {
			sscanf(line + 5, "%d", &chuteStatus);
		} else if (!strnicmp(line, "HEATSHIELD", 10)) {
			sscanf(line + 10, "%d", &heatShieldStatus);
		} else if (!strnicmp(line, "POWER", 5)) {
			sscanf(line + 5, "%d", &powerOn);
		} else if (!strnicmp(line, "TEMPERATURES", 12)) {
			sscanf(line + 12, "%lf %lf %lf %lf %lf %lf %lf", &heatShieldTemperature,
				&temperatures[0], &temperatures[1], &temperatures[2], &temperatures[3], &temperatures[4], &temperatures[5]);
		} else if (!strnicmp(line, "STRENGTH", 8)) {
			sscanf(line + 8, "%lf %lf %lf %lf %lf %lf %lf", &heatShieldStrength,
				&strength[0], &strength[1], &strength[2], &strength[3], &strength[4], &strength[5]);
		} else if (!strnicmp(line, "UMMUCREW", 8)) {
			crew.LoadAllMembersFromOrbiterScenario(line);
		} else {
			//PM_LoadState(line);
			ParseScenarioLineEx(line, status);
		}
	}
}

void PegasusAres::clbkPreStep(double simt, double simdt, double mjd) {
	if (GetNavmodeState(NAVMODE_HOLDALT)) {
		VECTOR3 weight;
		GetWeightVector(weight);
		SetThrusterGroupLevel(thg_main, length(weight) / MAIN_THRUST + dAutoThrust);
	}

	// pitch down to avoid rotation after landing
	/*if (!GroundContact() && groundAltitude < 0.2 && vspeed < 0) {
		SetThrusterLevel_SingleStep(th_pitch_down[0], 1);
		SetThrusterLevel_SingleStep(th_pitch_down[1], 1);
	}*/

	// get azimuth
	VECTOR3 northVector;
	HorizonInvRot(_V(0, 0, 1), northVector);
	northVector.x = -northVector.x;
	northVector.z = 0;
	azimuth = GetFullAngle(_V(0, 1, 0), northVector, _V(0, 0, 1));

	if (temperatures[0] == -1) {
		heatShieldTemperature = GetAtmTemperature() * min(GetAtmPressure(), 1.0);
		fill_n(temperatures, 6, heatShieldTemperature);
	}
}

// ==============================================================
// Post step processings
// ==============================================================
void PegasusAres::clbkPostStep(double simt, double simdt, double mjd) {
	if (abs(simdt) > 1e3) {
		return;
	}
	// damage
	if (simt - prevPressureCheckSimt > PRESSURE_CHECK_INTERVAL) {
		double pressure = GetDynPressure();
		double chuteMaxPressure = 12e3 + 4e3 * (1 - GetMass() / MASS_WET_MAX);
		//sprintf(oapiDebugString(), "%f", chuteMaxPressure);
		if (chuteStatus == OPEN && (pressure > chuteMaxPressure)
				&& oapiRand() < (pressure - chuteMaxPressure) / chuteMaxPressure) {
			JettisonChute();
			ShowMessage("!!! Parachute failure !!!", RGB(255, 0, 0));
		}

		VECTOR3 airflow;
		GetAirspeedVector(FRAME_LOCAL, airflow);
		double airflowZAngle;
		double airflowYAngle;
		if (length(airflow) != 0) {
			airflow /= length(airflow);
			airflow *= -1;
			airflowZAngle = GetAngle(_V(0, 0, 1), airflow); // [0, PI]
			airflowYAngle = GetAngle(_V(0, 1, 0), GetProjection(airflow, _V(0, 0, 1)), _V(0, 0, 1)); // [-PI, PI]
		} else {
			airflowZAngle = 0;
			airflowYAngle = 0;
		}

		double timeElapsed = simt - prevPressureCheckSimt;
		double atmTemperature = GetAtmTemperature() * min(GetAtmPressure(), 1.0);
		double outTemperature = boundaryT(atmTemperature, pressure, 1);

		double zCoeff = dTCoeff(airflowZAngle);
		heatShieldTemperature += dT(heatShieldTemperature, atmTemperature, pressure, zCoeff, timeElapsed);
		temperatures[1] += dT(temperatures[1], atmTemperature, pressure, 1 - zCoeff + MIN_DT_COEFF, timeElapsed);
		for (int i = 2; i < 6; i++) {
			double angle = airflowYAngle + (i - 2) * PI * 0.5;
			if (angle > PI) {
				angle -= 2 * PI;
			}
			temperatures[i] += dT(temperatures[i], atmTemperature, pressure, MIN_DT_COEFF + (dTCoeff(angle) - MIN_DT_COEFF) 
				//* sin(cos(airflowZAngle - PI * 0.5) * PI * 0.5), 
				* sin(airflowZAngle),
				timeElapsed);
		}
		temperatures[0] += (heatShieldStatus == OPEN && heatShieldStrength > 0 ? 
			((temperatures[2] + temperatures[3] + temperatures[4] + temperatures[5]) * 0.25 - temperatures[0]) * timeElapsed * 0.04
			: dT(temperatures[0], atmTemperature, pressure, zCoeff, timeElapsed));
		heatShieldStrength -= dStrength(heatShieldStrength, heatShieldTemperature, BOUNDARY_TEMPERATURE_HEAT_SHIELD, timeElapsed);
		for (int i = 0; i < 6; i++) {
			strength[i] -= dStrength(strength[i], temperatures[i], BOUNDARY_TEMPERATURE_HULL, timeElapsed);
			if (strength[i] <= 0 && pressure > 10e3) {
				char nameSuffix[7];
				OBJHANDLE focus;
				for (int debrisIndex = 0; debrisIndex < 6; debrisIndex++) {
					sprintf(nameSuffix, "debris%d", debrisIndex + 1);
					focus = CreateVessel("Vessels\\TotalImmersion\\debris", nameSuffix, true);
					VESSEL* debris = oapiGetVesselInterface(focus);
					debris->SetEmptyMass(400 + 300 * oapiRand());
					debris->SetGlobalOrientation(_V(2 * PI * oapiRand(), 2 * PI * oapiRand(), 2 * PI * oapiRand()));
				}
				oapiDeleteVessel(GetHandle(), focus);
				oapiCameraScaleDist(15);
				return;
			}
		}
		bool damage = heatShieldStatus == OPEN && heatShieldStrength < 100;
		if (!damage) {
			for (int i = 0; i < 6; i++) {
				if (strength[i] < 100) {
					damage = true;
					break;
				}
			}
		}
		if (damage != damageAlert) {
			damageAlert = damage;
			oapiVCTriggerRedrawArea(-1, AID_DAMAGE_BUTTON);
		}
		oapiVCTriggerRedrawArea(-1, AID_TEMPERATURES);
		/*sprintf(oapiDebugString(), "%d %d>%d (%d, %d, %d, %d, %d, %d) %d (%d, %d, %d, %d, %d, %d)",
			(int)tC(atmTemperature), (int)tC(outTemperature), (int)tC(heatShieldTemperature),
			(int)tC(temperatures[0]), (int)tC(temperatures[1]), (int)tC(temperatures[2]), (int)tC(temperatures[3]), (int)tC(temperatures[4]), (int)tC(temperatures[5]),
			(int)heatShieldStrength,
			(int)strength[0], (int)strength[1], (int)strength[2], (int)strength[3], (int)strength[4], (int)strength[5]);*/

		//VECTOR3 s = GetProjection(GetSurfaceNormal(), _V(0, 1, 0));
		float slope = GetAngle(GetSurfaceNormal(), _V(0, 1, 0));

		double longitude, latitude, radius;
		GetEquPos(longitude, latitude, radius);
		double elevNorth = oapiSurfaceElevation(GetSurfaceRef(), longitude, latitude + 0.001);
		double elevEast = oapiSurfaceElevation(GetSurfaceRef(), longitude - 0.001, latitude);
		double elevSouth = oapiSurfaceElevation(GetSurfaceRef(), longitude, latitude - 0.001);
		double elevWest = oapiSurfaceElevation(GetSurfaceRef(), longitude + 0.001, latitude);
		//sprintf(oapiDebugString(), "s:%f %f %f %f %f", slope, elevNorth, elevEast, elevSouth, elevWest);

		prevPressureCheckSimt = simt;
	}

	if (damageAlert) {
		oapiVCTriggerRedrawArea(-1, AID_DAMAGE_BUTTON);
	}
	if (annotationShowTime > 0) {
		annotationShowTime -= min(simdt, annotationShowTime);
		if (IsEqual(annotationShowTime, 0)) {
			oapiAnnotationSetText(hNote, "");
		}
	}

	// UMMU
	int ummuCheckResult = crew.ProcessUniversalMMu();
	if (ummuCheckResult == UMMU_TRANSFERED_TO_OUR_SHIP || ummuCheckResult == UMMU_RETURNED_TO_OUR_SHIP) {
		string msg = crew.GetLastEnteredCrewName();
		msg.append(" has entered the ship");
		ShowMessage(msg, RGB(0, 255, 0));
		oapiVCTriggerRedrawArea(-1, AID_CREW_SCREEN1);
		int crewNumber = crew.GetCrewTotalNumber();
		switch (crewNumber) {
			case 3:
				SetMeshVisibilityMode(7, MESHVIS_VC);
			case 2:
				SetMeshVisibilityMode(6, MESHVIS_VC);
			case 1:
				SetMeshVisibilityMode(5, MESHVIS_VC);
		}
	}

	// vertical speed
	if (groundAltitude > 0) {
		vspeed = (GetAltitude() - altitude) / simdt;
	} else {
		vspeed = 0;
	}
	altitude = GetAltitude();
	groundAltitude = GetAltitude(ALTMODE_GROUND) - ORIGIN_GROUND_HEIGHT;
	if (groundAltitude > 1 && ((groundAltitude < 10 && vspeed < -1) || (groundAltitude < 100 && vspeed < -10) ||
			(groundAltitude < 10000 && vspeed < -100))) {
		vspeedAlert = true;
		oapiVCTriggerRedrawArea(-1, AID_VSPEED_ALERT);
	} else if (vspeedAlert) {
		vspeedAlert = false;
		oapiVCTriggerRedrawArea(-1, AID_VSPEED_ALERT);
	}

	// ground contact
	if (groundContact != GroundContact()) {
		groundContact = GroundContact();
		oapiVCTriggerRedrawArea(-1, AID_GROUND_CONTACT);
		if (gearStatus != OPEN && !groundContact) {
			SetTouchdownPoints(TDP_GEARUP, 6);
		}
		if (gearStatus == OPEN && groundContact && vspeed < -5) {
			SetTouchdownPoints(TDP_GEARUP, 6);
			SetAnimation(animGear, 0.3);
		}
		if (groundContact) {
			crew.DefineAirLockShape(TRUE, 1, 6, 2, 8, -6.3f, -4.3f);
			crew.SetMembersPosRotOnEVA(_V(4.5,6,-5.5), _V(0,0,0));
		} else {
			crew.DefineAirLockShape(TRUE, -2, 2, -2, 2, 7, 10);
			crew.SetMembersPosRotOnEVA(_V(0,0,8.5), _V(0,0,0));
		}
	}

	if (powerOn) {
		oapiVCTriggerRedrawArea(-1, AID_HORIZONTAL_SPEED);
		oapiVCTriggerRedrawArea(-1, AID_ORIENTATION);
		oapiVCTriggerRedrawArea(-1, AID_DYNAMIC_STATUS);
		oapiVCTriggerRedrawArea(-1, AID_FUEL_LEVEL_SCREEN);
		oapiVCTriggerRedrawArea(-1, AID_TEMPERATURES);
	}

	// animations
	if (gearStatus >= CLOSING) {
		double da = simdt * 0.1;
		if (gearStatus == CLOSING) {
			if (gearProc > 0.0) {
				gearProc = max(0.0, gearProc - da);
			} else {
				gearStatus = CLOSED;
			}
		} else { // opening
			if (gearProc < 1.0) {
				gearProc = min(1.0, gearProc + da);
			} else {
				gearStatus = OPEN;
				SetTouchdownPoints(TDP_GEARDOWN, 6);
			}
		}
		SetAnimation(animGear, gearProc);
		oapiVCTriggerRedrawArea(-1, AID_GEAR_STATUS);
		oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
	} else if (gearStatus == CLOSED && groundAltitude < 50) {
		oapiVCTriggerRedrawArea(-1, AID_GEAR_STATUS);
	}

	// dock screen
	if (dockScreenStatus >= CLOSING) {
		double da = simdt * 0.5;
		if (dockScreenStatus == CLOSING) {
			if (dockScreenProc > 0.0) {
				dockScreenProc = max(0.0, dockScreenProc - da);
			} else {
				dockScreenStatus = CLOSED;
			}
		} else { // opening
			if (dockScreenProc < 1.0) {
				dockScreenProc = min(1.0, dockScreenProc + da);
			} else {
				dockScreenStatus = OPEN;
			}
		}
		SetAnimation(animDockScreenCover, dockScreenProc);
		oapiVCTriggerRedrawArea(-1, AID_DOCK_SCREEN);
	} else if (dockScreenStatus == OPEN) {
		if (dsSelectedVessel != NULL) {
			VESSEL* vessel = oapiGetVesselInterface(dsSelectedVessel);
			if (vessel != NULL) {
				VECTOR3 globalPos, pos;
				int dockCount = vessel->DockCount();
				if (dockCount > 0) {
					if (dsSelectedDockIndex >= dockCount) {
						dsSelectedDockIndex = dockCount - 1;
					}
					if (dsSelectedDockIndex < 0) {
						dsSelectedDockIndex = 0;
					}
					VECTOR3 dir, rot;
					vessel->GetDockParams(vessel->GetDockHandle(dsSelectedDockIndex), pos, dir, rot);
					pos += dir * dsDockDist;
				} else {
					pos = _V(0, 0, 0);
				}
				vessel->Local2Global(pos, globalPos);
				Global2Local(globalPos, pos);

				if (length(pos) < 10000) {
					dsDockPos = pos;
					VECTOR3 vel1, vel2;
					vessel->GetHorizonAirspeedVector(vel1);
					HorizonInvRot(vel1, vel2);
					vel1 = vel2;
					GetShipAirspeedVector(vel2);
					dsDockVel = vel2 - vel1;
				} else {
					dsSelectedVessel = NULL;
				}
			} else {
				dsSelectedVessel = NULL;
			}
		} else {
			//dsSelectedVesselIndex = 0;
			dsSelectedDockIndex = -1;
			DWORD vCount = oapiGetVesselCount();
			OBJHANDLE vessel;
			VECTOR3 pos;
			for (DWORD i = 0; i < vCount; i++) {
				vessel = oapiGetVesselByIndex(i);
				if (vessel == GetHandle()) continue;
				GetRelativePos(vessel, pos);
				if (length(pos) < 10000) {
					dsSelectedVessel = vessel;
					break;
				}
			}
		}
		oapiVCTriggerRedrawArea(-1, AID_DOCK_SCREEN);
	}
}

void PegasusAres::clbkFocusChanged(bool getfocus, OBJHANDLE hNewVessel, OBJHANDLE hOldVessel) {
	if (getfocus == false) oapiDelAnnotation(hNote);
}

int PegasusAres::clbkConsumeBufferedKey(DWORD key, bool down, char *kstate) {
	/*if (KEYDOWN(kstate, OAPI_KEY_DIVIDE)) { // switch RCS mode
		//if (ToggleAttitudeMode()) oapiTriggerRedrawArea (0, 0, AID_ATT_MODE);
		//return 1;
	} else*/ if (KEYMOD_LCONTROL(kstate) && KEYDOWN(kstate, OAPI_KEY_L)) {
		UINT meshCount = 1;
		MATERIAL* material;
		MESHHANDLE mesh;
		for (UINT i = 0; i < meshCount; i++) {
			mesh = GetMesh(*oapiObjectVisualPtr(GetHandle()), i);
			DWORD materialCount = oapiMeshMaterialCount(mesh);
			for (DWORD mi = 0; mi < materialCount; mi++) {
				material = oapiMeshMaterial(mesh, mi);
				if (material->emissive.g <= 0.1) {
					material->emissive.r = material->ambient.r;
					material->emissive.g = material->ambient.g;
					material->emissive.b = material->ambient.b;
				}
			}
		}
		int vesselCount = oapiGetVesselCount();
		VECTOR3 relPos;
		for (int vesselIndex = 0; vesselIndex < vesselCount; vesselIndex++) {
			OBJHANDLE hObject = oapiGetVesselByIndex(vesselIndex);
			if (hObject == GetHandle()) {
				continue;
			}
			GetRelativePos(hObject, relPos);
			if (length(relPos) < 500) {
				VESSEL* vessel = oapiGetVesselInterface(hObject);
				int meshIndex = 0;
				while (true) {
					mesh = vessel->GetMesh(*oapiObjectVisualPtr(hObject), meshIndex);
					if (mesh == NULL) break;
					DWORD materialCount = oapiMeshMaterialCount(mesh);
					for (DWORD mi = 0; mi < materialCount; mi++) {
						material = oapiMeshMaterial(mesh, mi);
						if (material->emissive.g <= 0.1) {
							material->emissive.r = material->ambient.r;
							material->emissive.g = material->ambient.g;
							material->emissive.b = material->ambient.b;
						}
					}
					meshIndex++;
				}
			}
		}
		return 1;
	}
	return 0;
}

int PegasusAres::clbkConsumeDirectKey(char *kstate) {
	/*if (KEYDOWN(kstate, OAPI_KEY_INSERT) || KEYDOWN(kstate, OAPI_KEY_DELETE)) {
		//oapiVCTriggerRedrawArea(-1, AID_TRIM_STATUS);
	} else if (KEYDOWN(kstate, OAPI_KEY_ADD) || KEYDOWN(kstate, OAPI_KEY_SUBTRACT)
			|| KEYDOWN(kstate, OAPI_KEY_MULTIPLY) || KEYDOWN(kstate, OAPI_KEY_NUMPAD0)
			|| KEYDOWN(kstate, OAPI_KEY_PERIOD)) {
		// redraw engines indicators
	} else*/ if (KEYDOWN(kstate, OAPI_KEY_DOWN) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 pos;
		GetCameraOffset(pos);
		if (KEYMOD_RCONTROL(kstate)) {
			if (pos.z > -10) pos.z -= 0.02;
		} else {
			VECTOR3 dir;
			GetCameraDefaultDirection(dir);
			pos -= dir * 0.02;
		}
		if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_UP) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 pos;
		GetCameraOffset(pos);
		if (KEYMOD_RCONTROL(kstate)) {
			if (pos.z < 10) pos.z += 0.02;
		} else {
			VECTOR3 dir;
			GetCameraDefaultDirection(dir);
			pos += dir * 0.02;
		}
		if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_LEFT) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 dir;
		GetCameraDefaultDirection(dir);
		if (KEYMOD_RALT(kstate)) {
			RotateVector(dir, _V(0, 0, 1), -0.05);
			SetCameraDefaultDirection(dir, dir.x > 0 ? PI / 2 : -PI / 2);
		} else {
			VECTOR3 pos;
			GetCameraOffset(pos);
			RotateVector(dir, _V(0, 0, 1), -PI/2);
			pos += dir * 0.02;
			if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		}
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_RIGHT) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 dir;
		GetCameraDefaultDirection(dir);
		if (KEYMOD_RALT(kstate)) {
			RotateVector(dir, _V(0, 0, 1), 0.05);
			SetCameraDefaultDirection(dir, dir.x > 0 ? PI / 2 : -PI / 2);
		} else {
			VECTOR3 pos;
			GetCameraOffset(pos);
			RotateVector(dir, _V(0, 0, 1), PI/2);
			pos += dir * 0.02;
			if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		}
		return 1;
	}

	return 0;
}

bool PegasusAres::IsCameraPositionCorrect(const VECTOR3& pos) {
	return pos.z <= 6.5 && pos.z >= 1.5 && 
		((pos.z < 4 && (pow(pos.x, 2) + pow(pos.y, 2) < 6.15)) ||
		(pow(pos.x, 2) + pow(pos.y, 2) < 3.3));
}

void PegasusAres::InitUmmu() {
	ummuInitReturnCode = crew.InitUmmu(GetHandle());
	if (ummuInitReturnCode != 1) {
		return;
	}

	crew.DefineAirLockShape(TRUE, 1, 6, 2, 8, -6.3f, -4.3f);
	crew.SetMembersPosRotOnEVA(_V(4.5,6,-5.5), _V(0,0,0));

	crew.SetMaxSeatAvailableInShip(3);

	crew.AddCrewMember("Peter Falcon",41,65,74,"Capt");
	crew.AddCrewMember("Fanny Gorgeous",27,67,55,"Eng");
	crew.AddCrewMember("George HealGood",15,70,45,"Doc");
}

void PegasusAres::clbkDockEvent(int dock, OBJHANDLE mate) {
	char msg[50];
	if (mate != NULL) {
		sprintf(msg, "Docked with %s", oapiGetVesselInterface(mate)->GetName());
	} else {
		sprintf(msg, "Undocking confirmed");
	}
	ShowMessage(msg);
	oapiVCTriggerRedrawArea(-1, AID_BTN_UNDOCK);
}

bool PegasusAres::IsAttached() {
	DWORD aCount = AttachmentCount(true);
	if (aCount == 0) {
		return false;
	}
	for (DWORD i = 0; i < aCount; i++) {
		if (GetAttachmentStatus(GetAttachmentHandle(true, i)) != NULL) {
			return true;
		}
	}
	return false;
}

void PegasusAres::JettisonChute() {
	// update vessel
	chuteStatus = STOPPED;
	SetMeshVisibilityMode(3, MESHVIS_NEVER);
	UpdateWithChuteStatus();
	// create chute vessel
	CreateVessel("P_Ares-Chute", "chute");
	// redraw status
	oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
}

OBJHANDLE PegasusAres::CreateVessel(char* className, char* nameSuffix, bool randomizeVelocity) {
	char name[50];
	sprintf(name, "%s_%s", GetName(), nameSuffix);
	OBJHANDLE obj = oapiGetObjectByName(name);
	if (obj != NULL) {
		oapiDeleteVessel(obj);
	}
	VESSELSTATUS vStatus;
	GetStatus(vStatus);
	if (randomizeVelocity) {
		vStatus.rvel += _V(oapiRand() * 0.5, oapiRand() * 0.5, oapiRand() * 0.5);
	}
	return oapiCreateVessel(name, className, vStatus);
}

double PegasusAres::tC(double tK) {
	return tK - 273.15;
}

double PegasusAres::dTCoeff(double angle) {
	// flatten cosine, https://math.stackexchange.com/questions/100655/cosine-esque-function-with-flat-peaks-and-valleys
	//return (sin(cos(angle) * PI * 0.5) * 0.4) + 0.6;
	//double bParam = 0.8;
	//return sqrt((1 + pow(bParam, 2)) / (1 + pow(bParam, 2) * pow(cos(angle), 2))) * cos(angle) * 0.4 + 0.6;
	// graphsketch.com cos(x * 1.3) * (1 - minCoeff) * 0.5 + (1 + minCoeff) * 0.5
	double decreaseCoeff = 1.3;
	if (abs(angle) > PI / decreaseCoeff) {
		return MIN_DT_COEFF;
	} else {
		return cos(angle * decreaseCoeff) * (1 - MIN_DT_COEFF) * 0.5 + (1 + MIN_DT_COEFF) * 0.5;
	}
}

double PegasusAres::boundaryT(double atmTemperature, double dynamicPressure, double coeff) {
	//return atmTemperature + pow(dynamicPressure * 0.001, 2) * coeff;
	return atmTemperature + min(pow(GetMachNumber(), 2), 1) * pow(dynamicPressure * GetAirspeed() / (SF * 400), 0.25) * coeff;
	//return atmTemperature + pow(0.5 * pow(GetAtmPressure(), 2) * pow(GetAirspeed(), 2) / (SF * 2e7), 0.25) * coeff;
}

double PegasusAres::dT(double currentTemperature, double atmTemperature, double dynamicPressure, double coeff, double timeElapsed) {
	return (boundaryT(atmTemperature, dynamicPressure, coeff) - currentTemperature) * timeElapsed * 0.02; // 0.05
}

double PegasusAres::dStrength(double currentStrength, double temperature, double boundaryTemperature, double timeElapsed) {
	return min(pow(max(temperature - boundaryTemperature, 0), 2) * timeElapsed * 1e-5, currentStrength); // 1e-5
}

/*bool InputCallback(void *id, char *str, void *usrdata) {
	if (strlen(str) == 0) return false;
	PegasusAres* vessel = (PegasusAres*)usrdata;
	if (gInputCallbackParams.action == INPCLBK_[action_id]) {
	} else if (gInputCallbackParams.action == INPCLBK_NAME_CREWMEMBER) {
		vessel->RenameSelectedCrewMember(str);
	}
	return true;
}*/
