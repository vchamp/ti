#include "Utils.h"
#include "Math.h"

int Round(double d) {
	return (int)(d > 0.0 ? d + 0.5 : d - 0.5);
	//return ((d - floor(d)) >= 0.5) ? ceil(d) : floor(d);
}

void RoundVector(VECTOR3& v, double precision) {
	v.x = Round(v.x * precision) / precision;
	v.y = Round(v.y * precision) / precision;
	v.z = Round(v.z * precision) / precision;
}

void DebugVector(char* header, const VECTOR3& v) {
	char buf[100];
	sprintf(buf, "%s: %lf %lf %lf", header, v.x, v.y, v.z);
	oapiWriteLog(buf);
}

void DebugInt(char* header, int val) {
	char buf[50];
	sprintf(buf, "%s: %d", header, val);
	oapiWriteLog(buf);
}

void DebugFloat(char* header, double val) {
	char buf[50];
	sprintf(buf, "%s: %f", header, val);
	oapiWriteLog(buf);
}

void DebugString(char* header, string& val) {
	char buf[100];
	sprintf(buf, "%s: %s", header, val.data());
	oapiWriteLog(buf);
}

void DebugString(char* header, char* val) {
	char buf[100];
	sprintf(buf, "%s: %s", header, val);
	oapiWriteLog(buf);
}

void DebugBool(char* header, bool val) {
	char buf[50];
	sprintf(buf, "%s: %s", header, val ? "true" : "false");
	oapiWriteLog(buf);
}

bool VectorContainsInt(vector<int>& v, int val) {
	for (UINT i = 0; i < v.size(); i++) {
		if (v[i] == val) {
			return true;
		}
	}
	return false;
}

void Tokenize(const string& str, vector<string>& tokens, const string& delimiters) {
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos = str.find_first_of(delimiters, lastPos);

    while (pos != string::npos || lastPos != string::npos) {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

void RotateVector(VECTOR3& v, const VECTOR3& axis, const double angle) {
	double c = cos(angle); double s = sin(angle); double C = 1 - c;
	double xs = axis.x * s; double ys = axis.y * s; double zs = axis.z * s;
	double xC = axis.x * C; double yC = axis.y * C; double zC = axis.z * C;
	double xyC = axis.x * yC; double yzC = axis.y * zC; double zxC = axis.z * xC;
	MATRIX3 m = _M(
		axis.x * xC + c,	xyC - zs,			zxC + ys,
		xyC + zs,			axis.y * yC + c,	yzC - xs,
		zxC - ys,			yzC + xs,			axis.z * zC + c);
	v = mul(m, v);
}

// -----------------------------------------------------------
// Returns a projection of a vector on a plane
// -----------------------------------------------------------
VECTOR3 GetProjection(const VECTOR3& v, const VECTOR3& planeNormal) {
	// projection of v1 on v2 = v2 * (dotp(v1, v2) / |v2|^2)
	return v - planeNormal * (dotp(v, planeNormal) / pow(length(planeNormal), 2));
}

float GetAngle(const VECTOR3& v1, const VECTOR3& v2) {
	return (float)acos(dotp(v1, v2) / (length(v1) * length(v2)));
}

float GetAngle(const VECTOR3& v1, const VECTOR3& v2, const VECTOR3& signRef) {
	float angle = GetAngle(v1, v2);
	if (dotp(signRef, crossp(v1, v2)) < 0) {
		angle = -angle;
	}
	return angle;
}

float GetFullAngle(const VECTOR3& v1, const VECTOR3& v2, const VECTOR3& signRef) {
	float angle = GetAngle(v1, v2, signRef);
	if (angle < 0) angle = (float)(2 * PI + angle);
	return angle;
}

bool IsNear(const double number, const double compare, const double inaccuracy) {
	return ((number >= compare - inaccuracy) && (number <= compare + inaccuracy));
}

bool IsNear(const int number, const int compare, const int inaccuracy) {
	return ((number >= compare - inaccuracy) && (number <= compare + inaccuracy));
}

bool IsEqual(const double n1, const double n2) {
	return IsNear(n1, n2, 0e-6);
}

bool IsEqual(const VECTOR3& v1, const VECTOR3& v2) {
	return IsEqual(v1.x, v2.x) && IsEqual(v1.y, v2.y) && IsEqual(v1.z, v2.z);
}

double Distance(const double d1, const double d2) {
	return max(d1, d2) - min(d1, d2);
}

bool IsPointInRect(int x, int y, int rLeft, int rTop, int rRight, int rBottom) {
	return ((x >= rLeft) && (x <= rRight) && (y >= rTop) && (y <= rBottom));
}

void RemoveVectorFromVector(vector<VECTOR3>& coll, const VECTOR3& v) {
	vector<VECTOR3>::iterator iter = coll.begin();
	while (iter != coll.end()) {
		if (IsEqual(*iter, v)) {
			coll.erase(iter);
			break;
		}
		++iter;
	}
}

// ===================================================
// Class Rect
// ===================================================
Rect::Rect(int aLeft, int aTop, int aWidth, int aHeight) {
	left = aLeft;
	top = aTop;
	width = aWidth;
	height = aHeight;
}

Rect::Rect() {
	Rect(0, 0, 100, 100);
}

int Rect::Left() {
	return left;
}

int Rect::Right() {
	return left + width;
}

int Rect::Top() {
	return top;
}

int Rect::Bottom() {
	return top + height;
}

int Rect::Width() {
	return width;
}

int Rect::Height() {
	return height;
}

int Rect::CenterX() {
	return left + width / 2;
}

int Rect::CenterY() {
	return top + height / 2;
}

bool Rect::IsPointIn(int x, int y) {
	return IsPointInRect(x, y, Left(), Top(), Right(), Bottom());
}
