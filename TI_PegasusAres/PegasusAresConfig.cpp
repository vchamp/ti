#include "PegasusAres.h"

void VLiftCoeff(double aoa, double M, double Re, double *cl, double *cm, double *cd) {

	static const double step = RAD * 22.5;
	static const double istep = 1.0 / step;
	static const int nabsc = 17;
//	static const double CL[nabsc] = { 0, 0.1, 0.2, 0.1, 0, 0.1, 0.2, 0.1, 0, -0.1, -0.2, -0.1, 0, -0.1, -0.2, -0.1, 0 };
	static const double CL[nabsc] = { 0, 0.2, 0.3, 0.2, 0, 0.2, 0.3, 0.2, 0, -0.2, -0.3, -0.2, 0, -0.2, -0.3, -0.2, 0 };

	double aoa0 = aoa;

	// simulate CoP behind CoG
	//*cm = sin(aoa);// *M * Re * 1e-5;
	*cm = 0;

	aoa += PI;
	int idx = max(0, min(15, (int)(aoa*istep)));
	double d = aoa * istep - idx;
	*cl = CL[idx] + (CL[idx + 1] - CL[idx])*d;
	*cd = 1 + oapiGetInducedDrag(*cl, 0.16, 0.2);
	//*cd = 2.5 + oapiGetInducedDrag(*cl, 0.16, 0.2);

	if (cos(aoa0) < 0) {
		*cl *= -4;
	} else {
		*cl = 0;
	}

	//sprintf(oapiDebugString(), "%f %f %f %f %f %f", aoa0, M, Re, *cl, *cm, *cd);
}

void HLiftCoeff(double aoa, double M, double Re, double *cl, double *cm, double *cd) {

	static const double step = RAD * 22.5;
	static const double istep = 1.0 / step;
	static const int nabsc = 17;
//	static const double CL[nabsc] = { 0, 0.1, 0.2, 0.1, 0, 0.1, 0.2, 0.1, 0, -0.1, -0.2, -0.1, 0, -0.1, -0.2, -0.1, 0 };
	static const double CL[nabsc] = { 0, 0.2, 0.3, 0.2, 0, 0.2, 0.3, 0.2, 0, -0.2, -0.3, -0.2, 0, -0.2, -0.3, -0.2, 0 };

	double aoa0 = aoa;

	// simulate CoP behind CoG
	//*cm = -sin(aoa);// *M * Re * 1e-5;
	*cm = 0;

	aoa += PI;
	int idx = max(0, min(15, (int)(aoa*istep)));
	double d = aoa * istep - idx;
	*cl = CL[idx] + (CL[idx + 1] - CL[idx])*d;
	*cd = 1 + oapiGetInducedDrag(*cl, 0.16, 0.2);
	//*cd = 2.5 + oapiGetInducedDrag(*cl, 0.16, 0.2);

	if (cos(aoa0) < 0) {
		*cl *= -4;// * M * Re * 1e-5;
	} else {
		*cl = 0;
	}

	//sprintf(oapiDebugString(), "%f %f %f %f %f %f", aoa0, M, Re, *cl, *cm, *cd);
}

void NoLiftCoeff(double aoa, double M, double Re, double *cl, double *cm, double *cd) {
	*cm = 0;
	*cl = 0;
	*cd = 1 + oapiGetInducedDrag(*cl, 0.16, 0.2);
}

void PegasusAres::UpdateWithChuteStatus() {
	UpdateEmptyMass();
	UpdateCrossSections();
	UpdateDragElement();
	UpdateRotDrag();
}

void PegasusAres::UpdateWithShieldStatus() {
	UpdateEmptyMass();
	UpdateCrossSections();
	UpdateAirfoils();
}

void PegasusAres::UpdateWithGearStatus() {
	UpdateEmptyMass();
}

void PegasusAres::UpdateEmptyMass() {
	double mass = MASS_DRY_BASE;
	if (chuteStatus != STOPPED) {
		mass += MASS_CHUTE;
	}
	if (heatShieldStatus != STOPPED) {
		mass += MASS_HEATSHIELD;
	}
	if (gearStatus != STOPPED) {
		mass += MASS_GEAR;
	}
	SetEmptyMass(mass);
}

void PegasusAres::UpdateCrossSections() {
	VECTOR3 crossSections = _V(0, 0, 0);
	crossSections += CROSS_SECTIONS_CAPSULE;
	if (heatShieldStatus == OPEN) {
		crossSections += CROSS_SECTIONS_HSHIELD;
	}
	if (chuteStatus == OPEN) {
		crossSections += CROSS_SECTIONS_CHUTE;
	}
	DebugVector("PAres set cross sections", crossSections);
	SetCrossSections(crossSections);
}

void PegasusAres::UpdateDragElement() {
	ClearVariableDragElements();
	if (chuteStatus == OPEN) {
		CreateVariableDragElement(&vde, 5, _V(0, 0, 80));
	}
	CreateVariableDragElement(&vde1, 0.5, _V(0, 0, 8));
}

void PegasusAres::UpdateRotDrag() {
	if (chuteStatus == OPEN) {
		SetRotDrag(_V(2, 2, 1));
	} else {
		SetRotDrag(_V(0.2, 0.2, 0.1));
	}
}

void PegasusAres::UpdateAirfoils() {
	if (heatShieldStatus == OPEN) {
		if (vertAirfoil == NULL || horzAirfoil == NULL) {
			DebugBool("create airfoils", true);
			ClearAirfoilDefinitions();
			vertAirfoil = CreateAirfoil2(LIFT_VERTICAL, _V(0, 0, 0), VLiftCoeff, 1.4, 16, 1);
			horzAirfoil = CreateAirfoil2(LIFT_HORIZONTAL, _V(0, 0, 0), HLiftCoeff, 1.4, 16, 1);
			noVertLiftAirfoil = NULL;
			noHorzLiftAirfoil = NULL;
		}
	} else if (noVertLiftAirfoil == NULL || noHorzLiftAirfoil == NULL) {
		ClearAirfoilDefinitions();
		noVertLiftAirfoil = CreateAirfoil2(LIFT_VERTICAL, _V(0, 0, 0), NoLiftCoeff, 0.6, 4, 1);
		noHorzLiftAirfoil = CreateAirfoil2(LIFT_HORIZONTAL, _V(0, 0, 0), NoLiftCoeff, 0.6, 4, 1);
		vertAirfoil = NULL;
		horzAirfoil = NULL;
	}
}
