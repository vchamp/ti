#ifndef __UTILS_H__
#define __UTILS_H__

#include "Common.h"

int Round(double d);
void RoundVector(VECTOR3& v, double precision = 10000.0);

void DebugVector(char* header, const VECTOR3& v);
void DebugInt(char* header, int val);
void DebugFloat(char* header, double val);
void DebugString(char* header, string& val);
void DebugString(char* header, char* val);
void DebugBool(char* header, bool val);

bool VectorContainsInt(vector<int>& v, int val);
void Tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ");

void RotateVector(VECTOR3& v, const VECTOR3& axis, const double angle);
VECTOR3 GetProjection(const VECTOR3& v, const VECTOR3& planeNormal);
float GetAngle(const VECTOR3& v1, const VECTOR3& v2);
float GetAngle(const VECTOR3& v1, const VECTOR3& v2, const VECTOR3& signRef);
float GetFullAngle(const VECTOR3& v1, const VECTOR3& v2, const VECTOR3& signRef);

bool IsNear(const double number, const double compare, const double inaccuracy);
bool IsNear(const int number, const int compare, const int inaccuracy);
bool IsEqual(double n1, double n2);
bool IsEqual(const VECTOR3& v1, const VECTOR3& v2);
double Distance(const double d1, const double d2);
bool IsPointInRect(int x, int y, int rLeft, int rTop, int rRight, int rBottom);

void RemoveVectorFromVector(vector<VECTOR3>& coll, const VECTOR3& v);

class Rect {
public:
	Rect();
	Rect(int aLeft, int aTop, int aWidth, int aHeight);
	int Left();
	int Right();
	int Top();
	int Bottom();
	int Width();
	int Height();
	int CenterX();
	int CenterY();
	bool IsPointIn(int x, int y);

private:
	int left, top, width, height;
};


#endif //__UTILS_H__
