#ifndef __PegasusAres_H__
#define __PegasusAres_H__

#include "Common.h"
#include "Utils.h"

// -----------------------------------
// Global GDI objects
// -----------------------------------
typedef struct {
	HINSTANCE hDLL;
	HFONT hFont[5];
	HPEN hPen[3];
	HBRUSH hBrush[3];
} GDIParams;
extern GDIParams g_Param;
#define LOADBMP(id) (LoadBitmap (g_Param.hDLL, MAKEINTRESOURCE (id)))

const int GR_MFD1_SCREEN = 15;
const int GR_MFD1_BUTTONS = 14;
const int GR_MFD1_MBUTTONS = 13;
const int GR_MFD2_SCREEN = 19;
const int GR_MFD2_BUTTONS = 17;
const int GR_MFD2_MBUTTONS = 18;
const int GR_MFD3_SCREEN = 23;
const int GR_MFD3_BUTTONS = 21;
const int GR_MFD3_MBUTTONS = 22;

//const int GR_HUD1 = 130;

const int GR_BTN_KILLROT = 65;
const int GR_BTN_PROGRADE = 66;
const int GR_BTN_RETROGRADE = 67;
const int GR_BTN_ORBNORMAL = 68;
const int GR_BTN_ORBANORMAL = 69;
const int GR_BTN_HOLDALTITUDE = 41;

const int GR_THRUST_INC = 33;
const int GR_THRUST_INC_STEP = 38;
const int GR_THRUST_BUTTONS = 34;
const int GR_ATT_ONOFF = 24;
const int GR_ATT_MODE = 37;
const int GR_ORIENTATION_MODE = 43;
const int GR_GEAR_UP = 49;
const int GR_GEAR_DOWN = 50;
const int GR_GEAR_JETTISON = 60;
const int GR_CHUTE_DEPLOY = 53;
const int GR_CHUTE_JETTISON = 54;
const int GR_HEAT_SHIELD_JETTISON = 55;
const int GR_ATTACH_NEW_HEAT_SHIELD = 61;
const int GR_ATTACH_NEW_CHUTE = 62;
const int GR_ATTACH_NEW_GEAR = 63;

const int GR_CREW_SCREEN1 = 71;
//const int GR_HUD_BUTTONS = 42;
const int GR_BTN_UNDOCK = 73;
const int GR_POWER = 124;
const int GR_DOCK_SCREEN_COVER = 128;
const int GR_DOCK_SCREEN = 133;
const int GR_DAMAGE_BUTTON = 140;

const int TEX_DYNAMIC1 = 6;
const int TEX_DYNAMIC2 = 9;

const int CREW_SCREEN_LINE_HEIGHT = 15;
const int CREW_SCREEN_FIRST_LINE = CREW_SCREEN_LINE_HEIGHT;
const int CREW_SUITS_FIRST_LINE = CREW_SCREEN_LINE_HEIGHT * 13;

const int nsurf = 2;

const double SF = 5.670367e-8;

// Input callback
/*typedef struct {
	int action;
	void* param;
} InputCallbackParams;
extern InputCallbackParams gInputCallbackParams;
bool InputCallback(void *id, char *str, void *usrdata);*/

class PegasusAres : public VESSEL2 { // : public VesselWithPM<PegasusAres>
public:
	PegasusAres(OBJHANDLE hVessel, int flightmodel);
	~PegasusAres();
	void clbkSetClassCaps(FILEHANDLE cfg);
	void clbkSaveState(FILEHANDLE scn);
	void clbkLoadStateEx(FILEHANDLE scn, void *status);
	void clbkPostCreation();
	bool clbkLoadVC(int id);
	bool clbkVCMouseEvent(int id, int event, VECTOR3 &p);
	bool clbkVCRedrawEvent(int id, int event, SURFHANDLE surf);
	void clbkMFDMode(int mfd, int mode);
	void clbkNavMode(int mode, bool active);
	void clbkRCSMode(int mode);
	void clbkPreStep(double simt, double simdt, double mjd);
	void clbkPostStep(double simt, double simdt, double mjd);
	void clbkFocusChanged(bool getfocus, OBJHANDLE hNewVessel, OBJHANDLE hOldVessel);
	int clbkConsumeBufferedKey(DWORD key, bool down, char *kstate);
	int clbkConsumeDirectKey(char *kstate);
	void clbkDockEvent(int dock, OBJHANDLE mate);
	void clbkVisualCreated(VISHANDLE vis, int refcount);

	//void PM_clbkPayloadJettisoned(int slot_idx, OBJHANDLE jettisoned_ship);
	//void PM_clbkPayloadAttached(int slot_idx, OBJHANDLE attached_ship);

private:
	const int MASS_DRY_BASE = 7000;
	const int MASS_CHUTE = 500;
	const int MASS_HEATSHIELD = 1000;
	const int MASS_GEAR = 1500;
	const int MASS_DRY_MAX = MASS_DRY_BASE + MASS_CHUTE + MASS_HEATSHIELD + MASS_GEAR;
	const int MASS_FUEL = 35000;
	const int MASS_WET_MAX = MASS_DRY_MAX + MASS_FUEL;

	const double MAIN_THRUST = 450000;
	//const double ORIGIN_GROUND_HEIGHT = 6.3;
	const double ORIGIN_GROUND_HEIGHT = 6.1;

	/*const double TDP_FRICTION = 3;
	const double TDP_STIFFNESS = 5e6;
	const double TDP_DAMPING = 1e5;*/
	const double TDP_FRICTION = 3;
	const double TDP_STIFFNESS = 5e6;
	const double TDP_DAMPING = 1e5;
	const double TDP_UP_STIFFNESS = 1e6;
	const double TDP_UP_DAMPING = 5e5;
	const TOUCHDOWNVTX TDP_GEARDOWN[6] = {
		{ _V(4.32, 5.24, -ORIGIN_GROUND_HEIGHT), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(6.32, -2.49, -ORIGIN_GROUND_HEIGHT), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(-0.41, -6.78, -ORIGIN_GROUND_HEIGHT), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(-6.58, -1.71, -ORIGIN_GROUND_HEIGHT), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(-3.65, 5.73, -ORIGIN_GROUND_HEIGHT), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(0, 0, 13), TDP_STIFFNESS, TDP_DAMPING, TDP_FRICTION, TDP_FRICTION }};
	const TOUCHDOWNVTX TDP_GEARUP[6] = {
		{ _V(0, 0, -4.72), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(3, 3, -3.39), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(3, -3, -3.39), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(-3, -3, -3.39), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(-3, 3, -3.39), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION },
		{ _V(0, 0, 18), TDP_UP_STIFFNESS, TDP_UP_DAMPING, TDP_FRICTION, TDP_FRICTION } };

	const int PRESSURE_CHECK_INTERVAL = 1;
	const double BOUNDARY_TEMPERATURE_HEAT_SHIELD = 2100;
	const double BOUNDARY_TEMPERATURE_HULL = 800;
	const double MIN_DT_COEFF = 0.2; // 0.2
	
	list<Message> messages;
	double altitude, vspeed, groundAltitude;
	int thrustIncIndex;
	double dAutoThrust;
	float azimuth;
	bool groundContact, orientationSpeedVector, vspeedAlert, gearJettisonArmed, powerOn, damageAlert;
	//int selectedPayload;
	//int soundId;
	OBJHANDLE dsSelectedVessel;
	int dsSelectedDockIndex, dsDockDist; // dsSelectedVesselIndex
	VECTOR3 dsDockPos, dsDockVel;
	bool dsPosition;

	double prevPressureCheckSimt = 0;

	double temperatures[6] = { -1, -1, -1, -1, -1, -1 };
	double strength[6] = { 100, 100, 100, 100, 100, 100 };
	double heatShieldTemperature = -1;
	double heatShieldStrength = 100;

	double annotationShowTime = 0;

	MESHHANDLE meshVC;
	SURFHANDLE srf[nsurf];

	PROPELLANT_HANDLE ph_main;
	THRUSTER_HANDLE th_main[1], th_pitch_down[2];
	THGROUP_HANDLE thg_main;

	NOTEHANDLE hNote;

	map<int, VECTOR3> initPos;

	// animations
	/*UINT animName;
	double animProc;
	AnimStatus animStatus;*/
	UINT animRCSOnOff, animRCSMode, animThrustIncStep, animThrustInc, animOrientationMode, animGear,
		animGearJettisonCover, animDockScreenCover;
	AnimStatus thrustIncStatus, gearStatus, chuteStatus, heatShieldStatus, dockScreenStatus;
	double gearProc, dockScreenProc;

	void RedrawMFDButtons(SURFHANDLE surf, int mfd);
	void RedrawMFDControls(SURFHANDLE surf, int mfd);
	void RedrawFuelScreen(SURFHANDLE surf);
	void RedrawMessageScreen(SURFHANDLE surf);
	void RedrawCrewScreen(SURFHANDLE surf);
	void RedrawDynamicStatus(SURFHANDLE surf);
	void RedrawHorizontalSpeed(SURFHANDLE surf);
	void RedrawOrientation(SURFHANDLE surf);
	void RedrawJettisonStatus(SURFHANDLE surf);
	void RedrawDockScreen(SURFHANDLE surf);
	void RedrawTemperatures(SURFHANDLE surf);

	void DrawTemperature(HDC hDC, double temperature, double boundaryTemperature, int x, int y);

	void ShowMessage(string newMessage, COLORREF newColor = RGB(0, 255, 0));
	void ShowAnnotation(char* message);
	void UpdateNavmodeButton(int navmode, int meshGroup);

	void SetClickArea(MESHGROUP* gr, int id, bool rect = true, const VECTOR3& revert = _V(0,0,0), const VECTOR3& maxShift = _V(0,0,0));
	void SetClickArea(MESHGROUP* gr, int id, int topLeftVertex, int topRightVertex, int bottomLeftVertex, int bottomRightVertex);

	void HandleCrewScreenClick(VECTOR3 &p);
	void HandleDockScreenBtnsClick(VECTOR3 &p);

	void DefineAnimations();
	void ReleaseSurfaces();

	bool IsAttached();
	bool IsCameraPositionCorrect(const VECTOR3& pos);

	void JettisonChute();
	OBJHANDLE CreateVessel(char* className, char* nameSuffix, bool randomizeVelocity = false);

	// UMMU
	//UMMUCREWMANAGMENT crew;
	Crew crew;
	int ummuInitReturnCode;
	void InitUmmu();
	UINT selectedCrewMember;
	UINT selectedSuit;

	// Config
	const VECTOR3 CROSS_SECTIONS_CAPSULE = _V(56, 56, 33);
	const VECTOR3 CROSS_SECTIONS_HSHIELD = _V(7, 7, 19);
	const VECTOR3 CROSS_SECTIONS_CHUTE = _V(357, 357, 1178);

	const double vde = 30; // 20
	const double vde1 = 0.1;
	AIRFOILHANDLE vertAirfoil = NULL, horzAirfoil = NULL, noVertLiftAirfoil = NULL, noHorzLiftAirfoil = NULL;
	
	void UpdateEmptyMass();
	void UpdateCrossSections();
	void UpdateDragElement();
	void UpdateRotDrag();
	void UpdateAirfoils();

	void UpdateWithChuteStatus();
	void UpdateWithShieldStatus();
	void UpdateWithGearStatus();

	// Utils
	double tC(double tK);
	double dTCoeff(double angle);
	double boundaryT(double atmTemperature, double dynamicPressure, double coeff);
	double dT(double currentTemperature, double atmTemperature, double dynamicPressure, double coeff, double timeElapsed);
	double dStrength(double currentStrength, double temperature, double boundaryTemperature, double timeElapsed);
};

const double thrustIncs[5] = { 0.00001, 0.0001, 0.001, 0.01, 0.1 };
const int EVASuitIndexes[8] = { 2, 1, 3, 4, 5, 7, 13, 15 };

#define AID_MFD1_BUTTONS 0
#define AID_MFD1_MBUTTONS 1
#define AID_MFD2_BUTTONS 2
#define AID_MFD2_MBUTTONS 3
#define AID_MFD3_BUTTONS 4
#define AID_MFD3_MBUTTONS 5

#define AID_BTN_KILLROT 11
#define AID_BTN_HORLEVEL 12
#define AID_BTN_PROGRADE 13
#define AID_BTN_RETROGRADE 14
#define AID_BTN_ORBNORMAL 15
#define AID_BTN_ORBANORMAL 16
#define AID_BTN_HOLDALTITUDE 17

#define AID_FUEL_LEVEL_SCREEN 21
#define AID_ATT_ONOFF 22
#define AID_ATT_MODE 23
#define AID_MESSAGE_SCREEN 24
#define AID_CREW_SCREEN1 25
#define AID_CREW_SCREEN2 26
#define AID_BTN_UNDOCK 27

#define AID_DYNAMIC_STATUS 28
#define AID_GROUND_CONTACT 30
#define AID_THRUST_INC 31
#define AID_THRUST_INC_STEP 32
#define AID_THRUST_BUTTONS 33
#define AID_HORIZONTAL_SPEED 34
#define AID_ORIENTATION 35
#define AID_ORIENTATION_MODE 36
#define AID_VSPEED_ALERT 37

#define AID_GEAR_UP 38
#define AID_GEAR_DOWN 39
#define AID_GEAR_STATUS 40
#define AID_GEAR_JETTISON 41
#define AID_CHUTE_DEPLOY 42
#define AID_CHUTE_JETTISON 43
#define AID_HEAT_SHIELD_JETTISON 44
#define AID_JETTISON_STATUS 45
#define AID_ATTACH_NEW_HEAT_SHIELD 46
#define AID_ATTACH_NEW_CHUTE 47
#define AID_ATTACH_NEW_GEAR 48
#define AID_POWER 49
#define AID_DOCK_SCREEN 50
#define AID_DOCK_SCREEN_BTNS 51
#define AID_TEMPERATURES 52
#define AID_DAMAGE_BUTTON 53

//#define INPCLBK_NAME_CREWMEMBER 0

#endif //__PegasusAres_H__
