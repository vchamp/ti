#include "PegasusAres.h"
#include "resource.h"

bool PegasusAres::clbkLoadVC(int id) {
	ReleaseSurfaces();

	switch (id) {
		case 0:
			SetCameraOffset(_V(0, 1, 5.8));
			SetCameraDefaultDirection(_V(0, 1, 0), PI);
			oapiVCSetNeighbours(1, 2, 0, 3);
			break;
		case 1:
			SetCameraOffset(_V(0.65, 1.2, 6));
			SetCameraDefaultDirection(_V(0.51, 0.86, 0), PI / 2);
			oapiVCSetNeighbours(1, 2, 0, 3);
			break;
		case 2:
			SetCameraOffset(_V(-0.65, 1.2, 6));
			SetCameraDefaultDirection(_V(-0.51, 0.86, 0), -PI / 2);
			oapiVCSetNeighbours(1, 2, 0, 3);
			break;
		case 3:
			SetCameraOffset(_V(0, -1, 3));
			SetCameraDefaultDirection(_V(0, 1, 0), PI);
			oapiVCSetNeighbours(1, 2, 0, 4);
			break;
		case 4:
			SetCameraOffset(_V(1.175, 1.6, 2.95));
			SetCameraDefaultDirection(_V(0.5, 0.6, 0) / length(_V(0.5, 0.6, 0)), PI / 2);
			oapiVCSetNeighbours(1, 2, 0, 3);
			break;
	}

	// camera shift
	SetCameraMovement(_V(0,0,0), 0, -20 * RAD, 
		_V(0,0,0), 30 * RAD, -20 * RAD, 
		_V(0,0,0), -30 * RAD, -20 * RAD);

	// register mfd
	static VCMFDSPEC mfds_left = {1, GR_MFD1_SCREEN};
	oapiVCRegisterMFD(MFD_LEFT, &mfds_left);
	static VCMFDSPEC mfds_right = {1, GR_MFD2_SCREEN};
	oapiVCRegisterMFD(MFD_RIGHT, &mfds_right);
	static VCMFDSPEC mfds_user1 = {1, GR_MFD3_SCREEN};
	oapiVCRegisterMFD(MFD_USER1, &mfds_user1);

	// register HUD
	/*MESHGROUP* gr = oapiMeshGroup(meshVC, GR_HUD1);
	VECTOR3 hudCenter = _V(gr->Vtx[0].x + (gr->Vtx[1].x - gr->Vtx[0].x) / 2.0, 
		gr->Vtx[0].y + (gr->Vtx[2].y - gr->Vtx[0].y) / 2.0,
		gr->Vtx[0].z);
	gr->UsrFlag = 2;
	static VCHUDSPEC hudSpec;
	hudSpec.nmesh = 1;
	hudSpec.ngroup = GR_HUD1;
	hudSpec.hudcnt = hudCenter;
	hudSpec.size = gr->Vtx[1].x - gr->Vtx[0].x;
	oapiVCRegisterHUD(&hudSpec);*/

	// clickable areas
	SURFHANDLE texDyn = oapiGetTextureHandle(meshVC, TEX_DYNAMIC1);

	oapiVCRegisterArea(AID_MFD1_BUTTONS, _R(0,0,60,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD1_BUTTONS), AID_MFD1_BUTTONS, 0, 121, 102, 223);
	oapiVCRegisterArea(AID_MFD1_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD1_MBUTTONS), AID_MFD1_MBUTTONS, 0, 41, 2, 43);

	oapiVCRegisterArea(AID_MFD2_BUTTONS, _R(60,0,120,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD2_BUTTONS), AID_MFD2_BUTTONS, 0, 121, 102, 223);
	oapiVCRegisterArea(AID_MFD2_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD2_MBUTTONS), AID_MFD2_MBUTTONS, 0, 41, 2, 43);

	oapiVCRegisterArea(AID_MFD3_BUTTONS, _R(120,0,180,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD3_BUTTONS), AID_MFD3_BUTTONS, 0, 121, 102, 223);
	oapiVCRegisterArea(AID_MFD3_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD3_MBUTTONS), AID_MFD3_MBUTTONS, 0, 41, 2, 43);

	oapiVCRegisterArea(AID_BTN_KILLROT, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_KILLROT), AID_BTN_KILLROT, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_BTN_PROGRADE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_PROGRADE), AID_BTN_PROGRADE, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_BTN_RETROGRADE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_RETROGRADE), AID_BTN_RETROGRADE, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_BTN_ORBNORMAL, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_ORBNORMAL), AID_BTN_ORBNORMAL, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_BTN_ORBANORMAL, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_ORBANORMAL), AID_BTN_ORBANORMAL, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_BTN_HOLDALTITUDE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_HOLDALTITUDE), AID_BTN_HOLDALTITUDE, true, _V(1, 0, 0));

	oapiVCRegisterArea(AID_ATT_ONOFF, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATT_ONOFF), AID_ATT_ONOFF, false);
	oapiVCRegisterArea(AID_ATT_MODE, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_RBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATT_MODE), AID_ATT_MODE, false);

	oapiVCRegisterArea(AID_BTN_UNDOCK, _R(125, 123, 187, 185), PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_UNDOCK), AID_BTN_UNDOCK, false);

	oapiVCRegisterArea(AID_THRUST_INC, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP);
	SetClickArea(oapiMeshGroup(meshVC, GR_THRUST_INC), AID_THRUST_INC, true, _V(1, 0, 1));
	oapiVCRegisterArea(AID_THRUST_INC_STEP, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_RBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_THRUST_INC_STEP), AID_THRUST_INC_STEP, false);
	oapiVCRegisterArea(AID_THRUST_BUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_THRUST_BUTTONS), AID_THRUST_BUTTONS, true, _V(1, 0, 1));

	oapiVCRegisterArea(AID_ORIENTATION_MODE, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_RBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ORIENTATION_MODE), AID_ORIENTATION_MODE, false);

	SURFHANDLE texDyn2 = oapiGetTextureHandle(meshVC, TEX_DYNAMIC2);
	oapiVCRegisterArea(AID_CREW_SCREEN1, _R(0, 0, 256, 256), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn2);
	SetClickArea(oapiMeshGroup(meshVC, GR_CREW_SCREEN1), AID_CREW_SCREEN1, true, _V(1, 0, 0));

	oapiVCRegisterArea(AID_MESSAGE_SCREEN, _R(0, 398, 340, 511), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);
	oapiVCRegisterArea(AID_DYNAMIC_STATUS, _R(0, 256, 340, 340), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);
	oapiVCRegisterArea(AID_FUEL_LEVEL_SCREEN, _R(1, 103, 179, 120), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);
	oapiVCRegisterArea(AID_HORIZONTAL_SPEED, _R(341, 0, 511, 170), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);
	oapiVCRegisterArea(AID_ORIENTATION, _R(341, 170, 511, 340), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);

	oapiVCRegisterArea(AID_GROUND_CONTACT, _R(0, 123, 30, 153), PANEL_REDRAW_INIT|PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);
	oapiVCRegisterArea(AID_VSPEED_ALERT, _R(29, 123, 59, 153), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);
	oapiVCRegisterArea(AID_GEAR_STATUS, _R(58, 123, 88, 153), PANEL_REDRAW_INIT|PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);

	oapiVCRegisterArea(AID_GEAR_UP, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_GEAR_UP), AID_GEAR_UP, false);
	oapiVCRegisterArea(AID_GEAR_DOWN, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_GEAR_DOWN), AID_GEAR_DOWN, false);
	oapiVCRegisterArea(AID_GEAR_JETTISON, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_GEAR_JETTISON), AID_GEAR_JETTISON, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_CHUTE_DEPLOY, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_CHUTE_DEPLOY), AID_CHUTE_DEPLOY, false);
	oapiVCRegisterArea(AID_CHUTE_JETTISON, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_CHUTE_JETTISON), AID_CHUTE_JETTISON, false);
	oapiVCRegisterArea(AID_HEAT_SHIELD_JETTISON, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_HEAT_SHIELD_JETTISON), AID_HEAT_SHIELD_JETTISON, false);

	oapiVCRegisterArea(AID_JETTISON_STATUS, _R(0, 237, 234, 255), PANEL_REDRAW_INIT|PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);

	oapiVCRegisterArea(AID_ATTACH_NEW_HEAT_SHIELD, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATTACH_NEW_HEAT_SHIELD), AID_ATTACH_NEW_HEAT_SHIELD, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_ATTACH_NEW_CHUTE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATTACH_NEW_CHUTE), AID_ATTACH_NEW_CHUTE, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_ATTACH_NEW_GEAR, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATTACH_NEW_GEAR), AID_ATTACH_NEW_GEAR, true, _V(1, 0, 0));

	oapiVCRegisterArea(AID_POWER, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_POWER), AID_POWER, true, _V(1, 0, 0));

	oapiVCRegisterArea(AID_DOCK_SCREEN_BTNS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_DOCK_SCREEN_COVER), AID_DOCK_SCREEN_BTNS, 3, 2, 1, 0);
	//SetClickArea(oapiMeshGroup(meshVC, GR_DOCK_SCREEN_COVER), AID_DOCK_SCREEN_BTNS, true, _V(1, 0, 0));
	oapiVCRegisterArea(AID_DOCK_SCREEN, _R(341, 340, 511, 510), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);

	oapiVCRegisterArea(AID_TEMPERATURES, _R(0, 277, 79, 396), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_BACKGROUND, texDyn2);
	oapiVCRegisterArea(AID_DAMAGE_BUTTON, _R(0, 153, 55, 184), PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_DAMAGE_BUTTON), AID_DAMAGE_BUTTON, true, _V(1, 0, 0));

	// create surfaces
	srf[0] = oapiCreateSurface(LOADBMP(IDB_BITMAP1));
	srf[1] = oapiCreateSurface(LOADBMP(IDB_BITMAP2));

	return true;
}

void PegasusAres::SetClickArea(MESHGROUP* gr, int id, bool rect, const VECTOR3& revert, const VECTOR3& maxShift) {
	float minX, maxX, minY, maxY, minZ, maxZ, x, y, z;
	minX = minY = minZ = 1000.0f;
	maxX = maxY = maxZ = -1000.0f;
	for (DWORD i = 0; i < gr->nVtx; i++) {
		x = gr->Vtx[i].x;
		y = gr->Vtx[i].y;
		z = gr->Vtx[i].z;
		if (x < minX) minX = x;
		if (x > maxX) maxX = x + (float) maxShift.x;
		if (y < minY) minY = y;
		if (y > maxY) maxY = y + (float) maxShift.y;
		if (z < minZ) minZ = z;
		if (z > maxZ) maxZ = z + (float) maxShift.z;
	}
	if (rect) {
		VECTOR3 p1 = _V(revert.x > 0 ? maxX : minX, revert.y > 0 ? minY : maxY, revert.z > 0 ? minZ : maxZ);
		VECTOR3 p2 = _V(revert.x > 0 ? minX : maxX, revert.y > 0 ? minY : maxY, revert.z > 0 ? minZ : maxZ);
		VECTOR3 p3 = _V(revert.x > 0 ? maxX : minX, revert.y > 0 ? maxY : minY, revert.z > 0 ? maxZ : minZ);
		VECTOR3 p4 = _V(revert.x > 0 ? minX : maxX, revert.y > 0 ? maxY : minY, revert.z > 0 ? maxZ : minZ);
		oapiVCSetAreaClickmode_Quadrilateral(id, p1, p2, p3, p4);
	} else {
		double distX = Distance(minX, maxX);
		double distY = Distance(minY, maxY);
		double distZ = Distance(minZ, maxZ);
		VECTOR3 center = _V(minX + distX / 2.0f, minY + distY / 2.0f, minZ + distZ / 2.0f);
		double rad = max(distX, max(distY, distZ));
		oapiVCSetAreaClickmode_Spherical(id, center, rad);
	}
}

void PegasusAres::SetClickArea(MESHGROUP* gr, int id, int topLeftVertex, int topRightVertex, int bottomLeftVertex, int bottomRightVertex) {
	VECTOR3 p1 = _V(gr->Vtx[topLeftVertex].x, gr->Vtx[topLeftVertex].y, gr->Vtx[topLeftVertex].z);
	VECTOR3 p2 = _V(gr->Vtx[topRightVertex].x, gr->Vtx[topRightVertex].y, gr->Vtx[topRightVertex].z);
	VECTOR3 p3 = _V(gr->Vtx[bottomLeftVertex].x, gr->Vtx[bottomLeftVertex].y, gr->Vtx[bottomLeftVertex].z);
	VECTOR3 p4 = _V(gr->Vtx[bottomRightVertex].x, gr->Vtx[bottomRightVertex].y, gr->Vtx[bottomRightVertex].z);
	if (p1.z == p2.z && p1.z == p3.z && p1.z == p4.z) {
		p1.z += 0.00001;
	}
	oapiVCSetAreaClickmode_Quadrilateral(id, p1, p2, p3, p4);
}

char* GetDamageDegree(double strength) {
	return strength < 50 ? "severe" : (strength < 80 ? "substantial" : (strength < 100 ? "light" : "no"));
}

// --------------------------------------------------------------
// Respond to virtual cockpit mouse events
// --------------------------------------------------------------
bool PegasusAres::clbkVCMouseEvent(int id, int event, VECTOR3 &p) {
	int index;
	switch (id) {
		case AID_MFD1_BUTTONS:
		case AID_MFD2_BUTTONS:
		case AID_MFD3_BUTTONS:
			if (p.x > 0.07 && p.x < 0.93) return false;
			index = (int) (p.y * 11);
			if (index == 0 || index / 2 != (index - 1) / 2) {
				index /= 2;
				if (p.x > 0.5) index += 6;
				oapiProcessMFDButton(id / 2, index, event);
				return true;
			} else {
				return false;
			}
		case AID_MFD1_MBUTTONS:
		case AID_MFD2_MBUTTONS:
		case AID_MFD3_MBUTTONS:
			if (event & PANEL_MOUSE_LBDOWN) {
				if (p.x < 0.1) {
					oapiToggleMFD_on(id / 2);
					return true;
				} else if (p.x > 0.9) {
					oapiSendMFDKey(id / 2, OAPI_KEY_GRAVE);
					return true;
				} else if (p.x > 0.8) {
					oapiSendMFDKey(id / 2, OAPI_KEY_F1);
					return true;
				}
				return false;
			}
		case AID_BTN_KILLROT:
		case AID_BTN_PROGRADE:
		case AID_BTN_RETROGRADE:
		case AID_BTN_ORBNORMAL:
		case AID_BTN_ORBANORMAL:
		case AID_BTN_HOLDALTITUDE:
			ToggleNavmode(id - 10);
			return true;
		case AID_ATT_ONOFF:
			if (GetAttitudeMode() == RCS_NONE) {
				SetAttitudeMode(RCS_ROT);
			} else {
				SetAttitudeMode(RCS_NONE);
			}
			return true;
		case AID_ATT_MODE:
			if (GetAttitudeMode() > RCS_NONE) {
				SetAttitudeMode(event == PANEL_MOUSE_LBDOWN ? RCS_ROT : RCS_LIN);
			}
			return true;
		case AID_BTN_UNDOCK:
			Undock(ALLDOCKS);
			return true;
		case AID_CREW_SCREEN1:
			HandleCrewScreenClick(p);
			return true;
		case AID_THRUST_INC:
			if (event != PANEL_MOUSE_LBUP) {
				if (GetNavmodeState(NAVMODE_HOLDALT)) {
					dAutoThrust += p.y > 0.5 ? thrustIncs[thrustIncIndex] : -thrustIncs[thrustIncIndex];
				} else {
					IncThrusterGroupLevel(thg_main, p.y > 0.5 ? thrustIncs[thrustIncIndex] : -thrustIncs[thrustIncIndex]);
				}
				if (thrustIncStatus == STOPPED || (thrustIncStatus == OPEN && p.y <= 0.5) ||
						(thrustIncStatus == CLOSED && p.y > 0.5)) {
					SetAnimation(animThrustInc, p.y < 0.5 ? 0 : 1);
					thrustIncStatus = p.y < 0.5 ? CLOSED : OPEN;
				}
			} else {
				SetAnimation(animThrustInc, 0.5);
				thrustIncStatus = STOPPED;
			}
			return true;
		case AID_THRUST_INC_STEP:
			if (event == PANEL_MOUSE_LBDOWN && thrustIncIndex > 0) {
				thrustIncIndex--;
			} else if (event == PANEL_MOUSE_RBDOWN && thrustIncIndex < 4) {
				thrustIncIndex++;
			}
			return true;
		case AID_THRUST_BUTTONS:
			DeactivateNavmode(NAVMODE_HOLDALT);
			if (p.x < 0.5) {
				SetThrusterGroupLevel(thg_main, 1);
			} else {
				SetThrusterGroupLevel(thg_main, 0);
			}
			return true;
		case AID_ORIENTATION_MODE:
			orientationSpeedVector = event == PANEL_MOUSE_LBDOWN;
			return true;
		case AID_GEAR_UP:
			if (gearStatus != CLOSED && gearStatus != STOPPED && !groundContact) {
				gearStatus = CLOSING;
				SetTouchdownPoints(TDP_GEARUP, 6);
			}
			return true;
		case AID_GEAR_DOWN:
			if (heatShieldStatus == OPEN) {
				ShowMessage("Can't deploy gear when heat shield is attached", RGB(255, 0, 0));
			} else if (gearStatus != OPEN && gearStatus != STOPPED) {
				gearStatus = OPENING;
			}
			return true;
		case AID_GEAR_JETTISON:
			if (!gearJettisonArmed || p.x < 0.2 || p.x > 0.8 || p.y < 0.2 || p.y > 0.8) {
				gearJettisonArmed = !gearJettisonArmed;
			} else if (gearJettisonArmed && gearStatus != STOPPED && heatShieldStatus != OPEN) {
				// update vessel
				gearStatus = STOPPED;
				UpdateWithGearStatus();
				SetMeshVisibilityMode(2, MESHVIS_NEVER);
				// create gear vessel
				CreateVessel("P_Ares-gear", "gear");
				// redraw status
				oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
				ShowMessage("Gear is jettisoned");
			}
			return true;
		case AID_CHUTE_DEPLOY:
			if (chuteStatus != OPEN && chuteStatus != STOPPED) {
				chuteStatus = OPEN;
				SetMeshVisibilityMode(3, MESHVIS_EXTERNAL);
				UpdateWithChuteStatus();
				ShowMessage("Parachute is deployed");
			}
			return true;
		case AID_CHUTE_JETTISON:
			if (chuteStatus == OPEN) {
				JettisonChute();
				ShowMessage("Parachute is jettisoned");
			}
			return true;
		case AID_HEAT_SHIELD_JETTISON:
			if (heatShieldStatus == OPEN) {
				// update vessel
				heatShieldStatus = STOPPED;
				SetMeshVisibilityMode(4, MESHVIS_NEVER);
				UpdateWithShieldStatus();
				// create heat shield vessel
				CreateVessel("P_Ares-heat", "heat_shield");
				// redraw status
				oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
				ShowMessage("Heat shield is jettisoned");
			}
			return true;
		case AID_ATTACH_NEW_HEAT_SHIELD:
			if (heatShieldStatus != STOPPED) {
				return true;
			} else if (!DockingStatus(0) && !IsAttached()) {
				ShowMessage("Must be docked or attached to attach a new heat shield", RGB(255, 0, 0));
				return true;
			} else if (gearStatus != CLOSED) {
				ShowMessage("Gear must be attached and retracted to attach a new heat shield", RGB(255, 0, 0));
				return true;
			} else {
				heatShieldStatus = OPEN;
				heatShieldTemperature = GetAtmTemperature();
				heatShieldStrength = 100;
				SetMeshVisibilityMode(4, MESHVIS_EXTERNAL);
				UpdateWithShieldStatus();
				oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
				ShowMessage("New heat shield is attached");
			}
			return true;
		case AID_ATTACH_NEW_CHUTE:
			if (chuteStatus != STOPPED) {
				return true;
			} else if (!DockingStatus(0) && !IsAttached()) {
				ShowMessage("Must be docked or attached to attach a new parachute", RGB(255, 0, 0));
				return true;
			} else {
				chuteStatus = CLOSED;
				UpdateWithChuteStatus();
				oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
				ShowMessage("New parachute is attached");
			}
			return true;
		case AID_ATTACH_NEW_GEAR:
			if (gearStatus != STOPPED) {
				return true;
			} else if (!DockingStatus(0) && !IsAttached()) {
				ShowMessage("Must be docked or attached to attach a new gear", RGB(255, 0, 0));
				return true;
			} else {
				gearStatus = CLOSED;
				UpdateWithGearStatus();
				SetMeshVisibilityMode(2, MESHVIS_EXTERNAL);
				gearProc = 0;
				SetAnimation(animGear, gearProc);
				oapiVCTriggerRedrawArea(-1, AID_JETTISON_STATUS);
				ShowMessage("New gear is attached");
			}
			return true;
		case AID_POWER:
			powerOn = !powerOn;
			if (!powerOn) {
				oapiVCTriggerRedrawArea(-1, AID_HORIZONTAL_SPEED);
				oapiVCTriggerRedrawArea(-1, AID_ORIENTATION);
				oapiVCTriggerRedrawArea(-1, AID_DYNAMIC_STATUS);
				oapiVCTriggerRedrawArea(-1, AID_FUEL_LEVEL_SCREEN);
				oapiVCTriggerRedrawArea(-1, AID_TEMPERATURES);
			}
			return true;
		case AID_DOCK_SCREEN_BTNS:
			if (dockScreenStatus == CLOSED || dockScreenStatus == CLOSING) {
				dockScreenStatus = OPENING;
			} else if (dockScreenStatus == OPEN && p.x > 0.17 && p.x < 0.83 && p.y > 0.17 && p.y < 0.83) {
				HandleDockScreenBtnsClick(p);
			} else {
				dockScreenStatus = CLOSING;
			}
			return true;
		case AID_DAMAGE_BUTTON:
			char message[256];
			char heatShieldMessage[64];
			if (heatShieldStatus == OPEN) {
				sprintf(heatShieldMessage, "\nHeat shield: %s damage", GetDamageDegree(heatShieldStrength));
			} else {
				sprintf(heatShieldMessage, "");
			}
			sprintf(message, "Hull top: %s damage\nHull sides: %s damage\nHull bottom: %s damage%s",
				GetDamageDegree(strength[1]),
				GetDamageDegree(min(min(min(strength[2], strength[3]), strength[4]), strength[5])),
				GetDamageDegree(strength[0]),
				heatShieldMessage
				);
			ShowAnnotation(message);
			return true;
	}
	return false;
}

void PegasusAres::clbkMFDMode(int mfd, int mode) {
	oapiVCTriggerRedrawArea(-1, mfd * 2);
}

void PegasusAres::clbkNavMode(int mode, bool active) {
	if (mode != NAVMODE_HLEVEL) {
		int gr;
		switch (mode) {
			case NAVMODE_KILLROT: gr = GR_BTN_KILLROT; break;
			case NAVMODE_PROGRADE: gr = GR_BTN_PROGRADE; break;
			case NAVMODE_RETROGRADE: gr = GR_BTN_RETROGRADE; break;
			case NAVMODE_NORMAL: gr = GR_BTN_ORBNORMAL; break;
			case NAVMODE_ANTINORMAL: gr = GR_BTN_ORBANORMAL; break;
			case NAVMODE_HOLDALT: gr = GR_BTN_HOLDALTITUDE; break;
		}
		UpdateNavmodeButton(mode, gr);
		if (mode == NAVMODE_HOLDALT && !active) {
			dAutoThrust = 0;
		}
	}
}

void PegasusAres::clbkRCSMode(int mode) {
	oapiVCTriggerRedrawArea(-1, AID_ATT_ONOFF);
	oapiVCTriggerRedrawArea(-1, AID_ATT_MODE);
}

void PegasusAres::UpdateNavmodeButton(int navmode, int meshGroup) {
	VISHANDLE visHandle = *oapiObjectVisualPtr(GetHandle());
	if (visHandle == NULL) return;
	DEVMESHHANDLE mesh = GetDevMesh(visHandle, 1);
	if (mesh == NULL) {
		oapiWriteLog("PegasusAres::UpdateNavmodeButton: mesh is null");
		return;
	}

	bool state = GetNavmodeState(navmode);

	GROUPREQUESTSPEC grs = { NULL, NULL, NULL, 0, 0, 0, 0, 0 };
	if (oapiGetMeshGroup(mesh, meshGroup, &grs) != 0) {
		oapiWriteLog("PegasusAres::UpdateNavmodeButton: mesh group is not retrieved");
		return;
	}

	// light
	MATERIAL mat = { };
	oapiMeshMaterial(mesh, meshGroup, &mat);
	if (state) {
		mat.emissive.r = mat.emissive.g = mat.emissive.b = 1.0f;
	} else {
		mat.emissive.r = mat.emissive.g = mat.emissive.b = 0.2f;
	}
	oapiSetMaterial(mesh, grs.MtrlIdx, &mat);

	// shift
	/*VECTOR3 pos = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	bool shifted = !IsEqual(pos, initPos.find(meshGroup)->second);
	if (state != shifted) {
		MESHGROUP_TRANSFORM transform;
		transform.nmesh = 1;
		transform.ngrp = meshGroup;
		transform.transform = MESHGROUP_TRANSFORM::TRANSLATE;
		switch (navmode) {
			case NAVMODE_HOLDALT:
				transform.P.transparam.shift = _V(0, 0.003, 0.000);
				break;
			default:
				transform.P.transparam.shift = _V(0, 0.001, -0.001);
				break;
		}
		if (!state) {
			transform.P.transparam.shift = -transform.P.transparam.shift;
		}
		MeshgroupTransform(visHandle, transform);
	}*/
}

// --------------------------------------------------------------
// Draw the virtual cockpit instruments
// --------------------------------------------------------------
bool PegasusAres::clbkVCRedrawEvent(int id, int event, SURFHANDLE surf) {
	switch (id) {
		case AID_MFD1_BUTTONS:
		case AID_MFD2_BUTTONS:
		case AID_MFD3_BUTTONS:
			RedrawMFDButtons(surf, id / 2);
			return true;
		case AID_FUEL_LEVEL_SCREEN:
			RedrawFuelScreen(surf);
			return true;
		case AID_ATT_ONOFF:
			SetAnimation(animRCSOnOff, GetAttitudeMode() > RCS_NONE);
			return true;
		case AID_ATT_MODE:
			if (GetAttitudeMode() == RCS_NONE) {
				SetAnimation(animRCSMode, 0.5);
			} else {
				SetAnimation(animRCSMode, GetAttitudeMode() - 1);
			}
			return true;
		case AID_MESSAGE_SCREEN:
			RedrawMessageScreen(surf);
			return true;
		case AID_CREW_SCREEN1:
			RedrawCrewScreen(surf);
			return true;
		case AID_GROUND_CONTACT:
			oapiBlt(surf, srf[0], 0, 0, GroundContact() ? 30 : 0, 0, 30, 30);
			return true;
		case AID_VSPEED_ALERT:
			oapiBlt(surf, srf[0], 0, 0, vspeedAlert && fmod(oapiGetSimTime(), 1.0) < 0.5 ? 60 : 0, 0, 30, 30);
			return true;
		case AID_GEAR_STATUS:
			if (gearStatus == OPEN) {
				oapiBlt(surf, srf[0], 0, 0, 30, 0, 30, 30);
			} else if (gearStatus == OPENING || gearStatus == CLOSING) {
				oapiBlt(surf, srf[0], 0, 0, fmod(oapiGetSimTime(), 1.0) < 0.5 ? 30 : 0, 0, 30, 30);
			} else if (groundAltitude < 50) {
				oapiBlt(surf, srf[0], 0, 0, fmod(oapiGetSimTime(), 1.0) < 0.5 ? 60 : 0, 0, 30, 30);
			} else {
				oapiBlt(surf, srf[0], 0, 0, 0, 0, 30, 30);
			}
			return true;
		case AID_THRUST_INC_STEP:
			SetAnimation(animThrustIncStep, thrustIncIndex * 0.25);
			return true;
		case AID_DYNAMIC_STATUS:
			RedrawDynamicStatus(surf);
			return true;
		case AID_HORIZONTAL_SPEED:
			RedrawHorizontalSpeed(surf);
			return true;
		case AID_ORIENTATION:
			RedrawOrientation(surf);
			return true;
		case AID_ORIENTATION_MODE:
			SetAnimation(animOrientationMode, orientationSpeedVector);
			return true;
		case AID_GEAR_JETTISON:
			SetAnimation(animGearJettisonCover, gearJettisonArmed);
			return true;
		case AID_JETTISON_STATUS:
			RedrawJettisonStatus(surf);
			return true;
		case AID_BTN_UNDOCK:
			oapiBlt(surf, srf[1], 0, 0, DockingStatus(0) ? 62 : 0, 0, 62, 62);
			return true;
		case AID_DOCK_SCREEN:
			RedrawDockScreen(surf);
			return true;
		case AID_TEMPERATURES:
			RedrawTemperatures(surf);
			return true;
		case AID_DAMAGE_BUTTON:
			oapiBlt(surf, srf[0], 0, 0, damageAlert && fmod(oapiGetSimTime(), 1.0) < 0.5 ? 145 : 90, 0, 55, 30);
			return true;
	}
	return false;
}

void PegasusAres::RedrawMFDButtons(SURFHANDLE surf, int mfd) {
	int btnWidth = 30;
	int btnHeight = 17;

	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 2 * btnWidth, 102);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[0]);
	SetTextColor(hDC, RGB(255, 255, 255));
	SetTextAlign(hDC, TA_CENTER);
	
	const char *label;
	for (int side = 0; side < 2; side++) {
		for (int bt = 0; bt < 6; bt++) {
			if (label = oapiMFDButtonLabel(mfd, side * 6 + bt)) {
				TextOut(hDC, side * btnWidth + btnWidth / 2, bt * btnHeight + btnHeight / 2 - 5, label, strlen(label));
			} else break;
		}
	}
	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawFuelScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 178, 17);
	SetBkMode(hDC, TRANSPARENT);

	if (powerOn) {
		SelectObject(hDC, g_Param.hFont[1]);
		SetTextColor(hDC, RGB(0, 255, 0));
		SetTextAlign(hDC, TA_CENTER);

		double mainFuelLevel = GetPropellantMass(ph_main) / GetPropellantMaxMass(ph_main);
		char text[10];
		sprintf(text, "%0.1lf%%", mainFuelLevel * 100);
		TextOut(hDC, 32, 0, text, strlen(text));

		SelectObject(hDC, g_Param.hBrush[1]);
		SelectObject(hDC, g_Param.hPen[0]);
		Rectangle(hDC, 62, 0, (int) (62 + 117 * mainFuelLevel), 17);
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::ShowMessage(string newMessage, COLORREF newColor) {
	if (messages.size() > 6) {
		messages.pop_front();
	}
	Message message;
	message.message = newMessage;
	message.color = newColor;
	messages.push_back(message);
	oapiVCTriggerRedrawArea(-1, AID_MESSAGE_SCREEN);
}

void PegasusAres::ShowAnnotation(char* message) {
	oapiAnnotationSetText(hNote, message);
	annotationShowTime = 4;
}

void PegasusAres::RedrawMessageScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 340, 113);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[3]);
	SetTextAlign(hDC, TA_LEFT);

	list<Message>::iterator msg;
	int line = 0;
	for (msg = messages.begin(); msg != messages.end(); ++msg) {
		SetTextColor(hDC, msg->color);
		TextOut(hDC, 2, 2 + 16 * line, msg->message.data(), msg->message.length());
		line++;
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawCrewScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 256, 256);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hBrush[2]);
	Rectangle(hDC, 2, CREW_SCREEN_FIRST_LINE + CREW_SCREEN_LINE_HEIGHT * selectedCrewMember,
		254, CREW_SCREEN_FIRST_LINE + CREW_SCREEN_LINE_HEIGHT * (selectedCrewMember + 1) + 2);

	SelectObject(hDC, g_Param.hFont[2]);
	SetTextAlign(hDC, TA_LEFT);
	SetTextColor(hDC, RGB(255, 255, 255));

	char text[100];
	sprintf(text, "Name");
	TextOut(hDC, 5, CREW_SCREEN_FIRST_LINE - CREW_SCREEN_LINE_HEIGHT, text, strlen(text));
	sprintf(text, "Age - Weight - Pulse");
	TextOut(hDC, 140, CREW_SCREEN_FIRST_LINE - CREW_SCREEN_LINE_HEIGHT, text, strlen(text));

	SelectObject(hDC, g_Param.hPen[1]);
	MoveToEx(hDC, 2, CREW_SCREEN_FIRST_LINE - 1, NULL);
	LineTo(hDC, 254, CREW_SCREEN_FIRST_LINE - 1);
	for (int i = 0; i < crew.GetCrewTotalNumber(); i++) {
		sprintf(text, "%s", crew.GetCrewNameBySlotNumber(i));
		TextOut(hDC, 5, CREW_SCREEN_FIRST_LINE + i * CREW_SCREEN_LINE_HEIGHT, text, strlen(text));
		sprintf(text, "%d - %d - %d", crew.GetCrewAgeBySlotNumber(i),
			crew.GetCrewWeightBySlotNumber(i), crew.GetCrewPulseBySlotNumber(i));
		TextOut(hDC, 190, CREW_SCREEN_FIRST_LINE + i * CREW_SCREEN_LINE_HEIGHT, text, strlen(text));
	}

	MoveToEx(hDC, 2, CREW_SUITS_FIRST_LINE - CREW_SCREEN_LINE_HEIGHT - 1, NULL);
	LineTo(hDC, 254, CREW_SUITS_FIRST_LINE - CREW_SCREEN_LINE_HEIGHT - 1);

	// suits
	SetTextColor(hDC, RGB(200, 200, 255));
	TextOut(hDC, 5, CREW_SUITS_FIRST_LINE - CREW_SCREEN_LINE_HEIGHT, "EVA Suits", 9);
	int left = selectedSuit % 4 * 64 + 5;
	int top = CREW_SUITS_FIRST_LINE + (selectedSuit / 4) * CREW_SCREEN_LINE_HEIGHT;
	Rectangle(hDC, left, top, left + 64, top + CREW_SCREEN_LINE_HEIGHT);
	for (int i = 0; i < 4; i++) {
		sprintf(text, UmmuFunctionName[EVASuitIndexes[i]]);
		TextOut(hDC, 5 + i * 64, CREW_SUITS_FIRST_LINE, text, strlen(text));
	}
	for (int i = 4; i < 8; i++) {
		sprintf(text, UmmuFunctionName[EVASuitIndexes[i]]);
		TextOut(hDC, 5 + (i - 4) * 64, CREW_SUITS_FIRST_LINE + CREW_SCREEN_LINE_HEIGHT, text, strlen(text));
	}

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 2, (int)(254 - CREW_SCREEN_LINE_HEIGHT * 1.5), 254, 254);
	TextOut(hDC, 110, 235, "EVA", 3);

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::HandleCrewScreenClick(VECTOR3 &p) {
	int x = (int) (256 * p.x);
	int y = (int) (256 * p.y);
	if (y > 254 - CREW_SCREEN_LINE_HEIGHT * 1.5) {
		if (crew.EvaCrewMember(crew.GetCrewNameBySlotNumber(selectedCrewMember)) > 0) {
			string msg = crew.GetLastEvaedCrewName();
			msg.append(" has left the ship");
			ShowMessage(msg, RGB(0, 255, 0));
			int crewNumber = crew.GetCrewTotalNumber();
			switch (crewNumber) {
				case 0:
					SetMeshVisibilityMode(5, MESHVIS_NEVER);
				case 1:
					SetMeshVisibilityMode(6, MESHVIS_NEVER);
				case 2:
					SetMeshVisibilityMode(7, MESHVIS_NEVER);
			}
		}
	} else if (y > CREW_SUITS_FIRST_LINE) {
		int row = (y - CREW_SUITS_FIRST_LINE) / CREW_SCREEN_LINE_HEIGHT;
		if (row < 2) {
			int column = x / 64;
			selectedSuit = row * 4 + column;
			char meshName[30];
			sprintf(meshName, UmmuMeshUsed[selectedSuit]);
			crew.SetAlternateMeshToUseForEVASpacesuit(meshName);
		}
	} else if (y > CREW_SCREEN_FIRST_LINE) {
		int index = (y - CREW_SCREEN_FIRST_LINE) / CREW_SCREEN_LINE_HEIGHT;
		if (index < crew.GetCrewTotalNumber()) {
			selectedCrewMember = index;
		}
	}
}

void PegasusAres::RedrawDynamicStatus(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 340, 84);
	SetBkMode(hDC, TRANSPARENT);

	if (powerOn) {
		SelectObject(hDC, g_Param.hFont[2]);
		SetTextColor(hDC, RGB(0, 255, 0));
		SetTextAlign(hDC, TA_RIGHT);

		char buf[20];
		char unitBuf[4];

		VECTOR3 weight;
		GetWeightVector(weight);
		sprintf(buf, "%0.2lf%%", length(weight) * 100 / 450000.0);
		TextOut(hDC, 238, 3, buf, strlen(buf));
		sprintf(buf, "%0.1lf�", azimuth * DEG);
		TextOut(hDC, 296, 3, buf, strlen(buf));
		sprintf(buf, "%0.2lf%%", GetThrusterGroupLevel(thg_main) * 100);
		TextOut(hDC, 52, 3, buf, strlen(buf));

		// altitude
		if (groundAltitude >= 1e6) {
			sprintf(unitBuf, "Mm");
			sprintf(buf, "%0.2lfM", groundAltitude / 1000000.0);
		} if (groundAltitude >= 1e3) {
			sprintf(unitBuf, "Km");
			sprintf(buf, "%0.2lfk", groundAltitude / 1000.0);
		} else {
			sprintf(unitBuf, "m");
			sprintf(buf, "%0.2lf", groundAltitude);
		}
		/*SelectObject(hDC, g_Param.hFont[0]);
		TextOut(hDC, 122, 3, unitBuf, strlen(unitBuf));*/
		SelectObject(hDC, g_Param.hFont[2]);
		TextOut(hDC, 122, 3, buf, strlen(buf));
		// vspeed
		if (vspeed < -20 && groundAltitude < 1000) {
			SetTextColor(hDC, RGB(255, 0, 0));
		} else if (vspeed < 0) {
			SetTextColor(hDC, RGB(255, 150, 50));
		}
		if (abs(vspeed) >= 1000) {
			sprintf(buf, "%0.1lfk", vspeed);
		} else {
			sprintf(buf, "%0.2lf", vspeed);
		}
		TextOut(hDC, 184, 3, buf, strlen(buf));
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawHorizontalSpeed(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 171, 171);
	SetBkMode(hDC, TRANSPARENT);

	if (powerOn) {
		SelectObject(hDC, g_Param.hPen[1]);
		MoveToEx(hDC, 10, 85, NULL);
		LineTo(hDC, 160, 85);
		MoveToEx(hDC, 85, 10, NULL);
		LineTo(hDC, 85, 160);

		VECTOR3 horSpeed;
		GetHorizonAirspeedVector(horSpeed);
		horSpeed.y = 0;
		RotateVector(horSpeed, _V(0, 1, 0), -azimuth);

		int factor = 1;
		if (abs(horSpeed.x) < 7.5 && abs(horSpeed.z) < 7.5) factor = 10;
		int crossCenterX = (int)(85 + (horSpeed.x > 0 ? min(horSpeed.x, 75) : max(horSpeed.x, -75)) * factor);
		int crossCenterY = (int)(85 - (horSpeed.z > 0 ? min(horSpeed.z, 75) : max(horSpeed.z, -75)) * factor);

		SelectObject(hDC, g_Param.hPen[0]);
		MoveToEx(hDC, 85, 85, NULL);
		LineTo(hDC, crossCenterX, crossCenterY);
		MoveToEx(hDC, crossCenterX - 8, crossCenterY, NULL);
		LineTo(hDC, crossCenterX + 8, crossCenterY);
		MoveToEx(hDC, crossCenterX, crossCenterY - 8, NULL);
		LineTo(hDC, crossCenterX, crossCenterY + 8);

		SelectObject(hDC, g_Param.hFont[0]);
		SetTextColor(hDC, RGB(0, 255, 0));
		SetTextAlign(hDC, TA_LEFT);

		char buf[100];
		sprintf(buf, "%0.1lf", length(horSpeed));
		SIZE textSize;
		GetTextExtentPoint32A(hDC, buf, strlen(buf), &textSize);
		int textX = crossCenterX > 160 - textSize.cx ? crossCenterX - 2 - textSize.cx : crossCenterX + 2;
		int textY = crossCenterY <= 10 ? crossCenterY + 2 : crossCenterY - 10;
		TextOut(hDC, textX, textY, buf, strlen(buf));
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawOrientation(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 171, 171);
	SetBkMode(hDC, TRANSPARENT);

	if (powerOn) {
		SelectObject(hDC, g_Param.hPen[1]);
		MoveToEx(hDC, 10, 85, NULL);
		LineTo(hDC, 160, 85);
		MoveToEx(hDC, 85, 10, NULL);
		LineTo(hDC, 85, 160);

		VECTOR3 orientation;
		if (orientationSpeedVector) {
			GetShipAirspeedVector(orientation);
			if (length(orientation) != 0) {
				orientation /= length(orientation);
			}
		} else {
			HorizonInvRot(_V(0, -1, 0), orientation);
		}
		double angle = GetAngle(_V(0, 0, -1), orientation, crossp(_V(0, 0, -1), orientation)) * DEG;
		orientation.z = 0;
		orientation *= 75;

		int factor = 1;
		if (abs(orientation.x) < 7.5 && abs(orientation.y) < 7.5) factor = 10;
		int crossCenterX = (int)(85 - orientation.x * factor);
		int crossCenterY = (int)(85 - orientation.y * factor);

		SelectObject(hDC, g_Param.hPen[0]);
		MoveToEx(hDC, 85, 85, NULL);
		LineTo(hDC, crossCenterX, crossCenterY);
		MoveToEx(hDC, crossCenterX - 8, crossCenterY, NULL);
		LineTo(hDC, crossCenterX + 8, crossCenterY);
		MoveToEx(hDC, crossCenterX, crossCenterY - 8, NULL);
		LineTo(hDC, crossCenterX, crossCenterY + 8);

		SelectObject(hDC, g_Param.hFont[0]);
		SetTextColor(hDC, RGB(0, 255, 0));
		SetTextAlign(hDC, TA_LEFT);

		char buf[100];
		sprintf(buf, "%0.0lf�", abs(angle)); // length(orientation * 90 / 75)
		TextOut(hDC, crossCenterX + 2, crossCenterY - 10, buf, strlen(buf));
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawJettisonStatus(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 234, 18);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[3]);
	SetTextAlign(hDC, TA_LEFT);

	if (heatShieldStatus == STOPPED) {
		SetTextColor(hDC, RGB(255, 0, 0));
		TextOut(hDC, 8, 1, "Jettisoned", 10);
	}
	if (chuteStatus == STOPPED) {
		SetTextColor(hDC, RGB(255, 0, 0));
		TextOut(hDC, 87, 1, "Jettisoned", 10);
	} else if (chuteStatus == OPEN) {
		SetTextColor(hDC, RGB(0, 255, 0));
		TextOut(hDC, 89, 1, "Deployed", 8);
	}
	if (gearStatus == STOPPED) {
		SetTextColor(hDC, RGB(255, 0, 0));
		TextOut(hDC, 164, 1, "Jettisoned", 10);
	} else if (gearStatus == OPEN) {
		SetTextColor(hDC, RGB(0, 255, 0));
		TextOut(hDC, 166, 1, "Deployed", 8);
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::HandleDockScreenBtnsClick(VECTOR3 &p) {
	if (p.y < 0.17 * 2) {
		dsPosition = p.x < 0.5;
	} else if (p.y < 0.17 * 3) {
		if (dsSelectedVessel == NULL) return;
		DWORD vCount = oapiGetVesselCount();
		OBJHANDLE vessel;
		OBJHANDLE firstVessel = NULL;
		VECTOR3 pos;
		bool wasCurrent = false;
		bool foundNew = false;
		for (DWORD i = 0; i < vCount; i++) {
			vessel = oapiGetVesselByIndex(i);
			if (vessel == GetHandle()) continue;
			GetRelativePos(vessel, pos);
			if (length(pos) < 10000) {
				if (wasCurrent) {
					dsSelectedVessel = vessel;
					foundNew = true;
					break;
				}
				if (vessel == dsSelectedVessel) {
					wasCurrent = true;
					continue;
				}
				if (firstVessel == NULL) firstVessel = vessel;
			}
		}
		if (foundNew) {
			dsSelectedDockIndex = -1;
		} else if (firstVessel != NULL) {
			dsSelectedVessel = firstVessel;
			dsSelectedDockIndex = -1;
		}
	} else if (p.y < 0.17 * 4) {
		if (dsSelectedVessel == NULL) return;
		VESSEL* vessel = oapiGetVesselInterface(dsSelectedVessel);
		int dockCount = vessel->DockCount();
		if (dockCount > 0) {
			dsSelectedDockIndex++;
			if (dsSelectedDockIndex >= dockCount) {
				dsSelectedDockIndex = 0;
			}
		}
	} else {
		if (p.x < 0.5) {
			dsDockDist += 10;
			if (dsDockDist > 100) dsDockDist = 100;
		} else {
			dsDockDist -= 10;
			if (dsDockDist < 0) dsDockDist = 0;
		}
	}
}

void PegasusAres::RedrawDockScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 171, 171);
	SetBkMode(hDC, TRANSPARENT);

	if (dockScreenStatus == OPEN) {
		SelectObject(hDC, g_Param.hPen[1]);
		MoveToEx(hDC, 10, 85, NULL);
		LineTo(hDC, 160, 85);
		MoveToEx(hDC, 85, 10, NULL);
		LineTo(hDC, 85, 160);

		if (dsSelectedVessel != NULL && length(dsDockPos) > 0 && length(dsDockVel) > 0) {
			SelectObject(hDC, g_Param.hFont[0]);
			SetTextColor(hDC, RGB(180, 180, 180));
			SetTextAlign(hDC, TA_LEFT);

			char buf[100];
			sprintf(buf, "%s", oapiGetVesselInterface(dsSelectedVessel)->GetName());
			TextOut(hDC, 5, 0, buf, strlen(buf));
			if (dsSelectedDockIndex >= 0) {
				sprintf(buf, "Dock %d", dsSelectedDockIndex + 1);
			} else {
				sprintf(buf, "Dock -");
			}
			TextOut(hDC, 90, 0, buf, strlen(buf));
			sprintf(buf, "dDist %d", dsDockDist);
			TextOut(hDC, 125, 0, buf, strlen(buf));
			sprintf(buf, "Dist %0.1lfm", length(dsDockPos));
			TextOut(hDC, 5, 155, buf, strlen(buf));
			sprintf(buf, "Vel %0.1lfm/s", length(dsDockVel) * (dotp(dsDockPos, dsDockVel) >= 0 ? 1 : -1));
			TextOut(hDC, 80, 155, buf, strlen(buf));
			sprintf(buf, "%s", dsPosition ? "POS" : "VEL");
			TextOut(hDC, 150, 155, buf, strlen(buf));

			VECTOR3 v;
			if (dsPosition) {
				v = dsDockPos / length(dsDockPos);
			} else {
				v = dsDockVel / length(dsDockVel);
			}

			double angle = GetAngle(_V(0, 0, -1), v, crossp(_V(0, 0, -1), v)) * DEG;
			v.z = 0;
			v *= 75;

			int factor = 1;
			if (abs(v.x) < 7.5 && abs(v.y) < 7.5) factor = 10;
			int crossCenterX = (int)(85 - v.x * factor);
			int crossCenterY = (int)(85 - v.y * factor);

			SelectObject(hDC, g_Param.hPen[0]);
			MoveToEx(hDC, 85, 85, NULL);
			LineTo(hDC, crossCenterX, crossCenterY);
			MoveToEx(hDC, crossCenterX - 8, crossCenterY, NULL);
			LineTo(hDC, crossCenterX + 8, crossCenterY);
			MoveToEx(hDC, crossCenterX, crossCenterY - 8, NULL);
			LineTo(hDC, crossCenterX, crossCenterY + 8);

			SetTextColor(hDC, RGB(0, 255, 0));

			sprintf(buf, "%0.0lf�", abs(angle));
			TextOut(hDC, crossCenterX + 2, crossCenterY - 10, buf, strlen(buf));
		}
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::RedrawTemperatures(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hFont[0]);
	SetTextAlign(hDC, TA_CENTER);
	SetBkMode(hDC, TRANSPARENT);

	if (powerOn) {
		if (heatShieldStatus == OPEN) {
			DrawTemperature(hDC, heatShieldTemperature, BOUNDARY_TEMPERATURE_HEAT_SHIELD, 40, 103);
		}
		DrawTemperature(hDC, temperatures[0], BOUNDARY_TEMPERATURE_HULL, 40, 83);
		DrawTemperature(hDC, temperatures[1], BOUNDARY_TEMPERATURE_HULL, 40, 10);
		DrawTemperature(hDC, temperatures[2], BOUNDARY_TEMPERATURE_HULL, 40, 35);
		DrawTemperature(hDC, temperatures[3], BOUNDARY_TEMPERATURE_HULL, 10, 48);
		DrawTemperature(hDC, temperatures[4], BOUNDARY_TEMPERATURE_HULL, 40, 62);
		DrawTemperature(hDC, temperatures[5], BOUNDARY_TEMPERATURE_HULL, 70, 48);

		char buf[10];
		sprintf(buf, "q:%0.1lfkPa", GetDynPressure() * 1e-3);
		SelectObject(hDC, g_Param.hFont[4]);
		SetTextColor(hDC, RGB(0, 255, 0));
		SetTextAlign(hDC, TA_LEFT);
		TextOut(hDC, 1, 1, buf, strlen(buf));
	}

	oapiReleaseDC(surf, hDC);
}

void PegasusAres::DrawTemperature(HDC hDC, double temperature, double boundaryTemperature, int x, int y) {
	char buf[10];
	sprintf(buf, "%d", (int)tC(temperature));
	double k = tC(temperature) / tC(boundaryTemperature);
	if (k > 0) {
		SetTextColor(hDC, RGB(
			(int)(255 * max(min(2 * k, 1), 0)),
			(int)(255 * max(min(2 * (1 - k), 1), 0)),
			0));
	} else {
		k = tC(temperature) / tC(0);
		SetTextColor(hDC, RGB(
			0,
			(int)(255 * max(min(2 * (1 - k), 1), 0)),
			(int)(255 * max(min(2 * k, 1), 0))));
	}
	TextOut(hDC, x, y, buf, strlen(buf));
}
