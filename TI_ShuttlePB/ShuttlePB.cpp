#define ORBITER_MODULE

#include "ShuttlePB.h"

GDIParams g_Param;

DLLCLBK void InitModule(HINSTANCE hModule) {
	g_Param.hDLL = hModule;

	g_Param.hFont[0] = CreateFont(12, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[1] = CreateFont(16, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[2] = CreateFont(20, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");
	g_Param.hFont[3] = CreateFont(40, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, "Arial");

	g_Param.hPen[0] = CreatePen(PS_SOLID, 1, RGB(0, 255, 0));

	g_Param.hBrush[0] = CreateSolidBrush(RGB(0, 0, 0));
	g_Param.hBrush[1] = CreateSolidBrush(RGB(150, 150, 150));
	g_Param.hBrush[2] = CreateSolidBrush(RGB(255, 255, 255));
}

DLLCLBK void ExitModule(HINSTANCE hModule) {
	int i;
	// deallocate GDI resources
	for (i = 0; i < 4; i++) DeleteObject (g_Param.hFont[i]);
	for (i = 0; i < 1; i++) DeleteObject (g_Param.hPen[i]);
	for (i = 0; i < 3; i++) DeleteObject (g_Param.hBrush[i]);
}

DLLCLBK VESSEL *ovcInit(OBJHANDLE hvessel, int flightmodel) {
	return new ShuttlePB(hvessel, flightmodel);
}

DLLCLBK void ovcExit (VESSEL *vessel) {
	if (vessel) delete (ShuttlePB*)vessel;
}

double LiftCoeff(double aoa) {
	const int nlift = 9;
	static const double AOA[nlift] = {-180*RAD,-60*RAD,-30*RAD,-1*RAD,15*RAD,20*RAD,25*RAD,60*RAD,180*RAD};
	static const double CL[nlift]  = {       0,      0,   -0.1,     0,   0.2,  0.25,   0.2,     0,      0};
	static const double SCL[nlift] = {(CL[1]-CL[0])/(AOA[1]-AOA[0]), (CL[2]-CL[1])/(AOA[2]-AOA[1]),
		                              (CL[3]-CL[2])/(AOA[3]-AOA[2]), (CL[4]-CL[3])/(AOA[4]-AOA[3]),
									  (CL[5]-CL[4])/(AOA[5]-AOA[4]), (CL[6]-CL[5])/(AOA[6]-AOA[5]),
									  (CL[7]-CL[6])/(AOA[7]-AOA[6]), (CL[8]-CL[7])/(AOA[8]-AOA[7])};
	int i;
	for (i = 0; i < nlift-1 && AOA[i+1] < aoa; i++);
	return CL[i] + (aoa-AOA[i])*SCL[i];
}

// --------------------------------------------------------------
// Constructor
// --------------------------------------------------------------
ShuttlePB::ShuttlePB(OBJHANDLE hVessel, int flightmodel) : VESSEL3 (hVessel, flightmodel) { // : baseClass(hVessel, flightmodel)
	for (int i = 0; i < nsurf; i++) {
		srf[i] = NULL;
	}

	isNew = true;

	gearProc = 0;
	gearStatus = CLOSED;
	dockProc = 0;
	dockStatus = CLOSED;

	prevThrust[0] = 0.5;
	prevThrust[1] = 0.5;

	altitude = -1;
	vspeed = 0;

	Message msg;
	msg.message = "Welcome onboard";
	msg.color = RGB(0, 255, 0);
	messages.push_back(msg);

	DefineAnimations();

	// UMMU
	ummuInitReturnCode = 1;
	selectedCrewMember = 0;
	selectedSuit = 0;
}

// --------------------------------------------------------------
// Destructor
// --------------------------------------------------------------
ShuttlePB::~ShuttlePB() {
	ReleaseSurfaces();
}

// animation transformation definitions
static UINT GRP_LWING = 2;
static UINT GRP_RWING = 3;
static VECTOR3 LWING_REF  = {-1.3,-0.725,-1.5};
static VECTOR3 LWING_AXIS = {-0.9619,-0.2735,0};
static VECTOR3 RWING_REF  = {1.3,-0.725,-1.5};
static VECTOR3 RWING_AXIS = {0.9619,-0.2735,0};
static float AILERON_RANGE = (float)(20.0*RAD);
static float ELEVATOR_RANGE = (float)(30.0*RAD);
MGROUP_ROTATE ShuttlePB::trans_Laileron (0, &GRP_LWING, 1, LWING_REF, LWING_AXIS, AILERON_RANGE);
MGROUP_ROTATE ShuttlePB::trans_Raileron (0, &GRP_RWING, 1, RWING_REF, RWING_AXIS, AILERON_RANGE);
MGROUP_ROTATE ShuttlePB::trans_Lelevator (0, &GRP_LWING, 1, LWING_REF, LWING_AXIS, -ELEVATOR_RANGE);
MGROUP_ROTATE ShuttlePB::trans_Relevator (0, &GRP_RWING, 1, RWING_REF, RWING_AXIS, ELEVATOR_RANGE);

void ShuttlePB::DefineAnimations() {

	meshVC = oapiLoadMeshGlobal("TotalImmersion\\ShuttlePBVC");
	meshGear = oapiLoadMeshGlobal("TotalImmersion\\ShuttlePBGear");

	// save initial positions of buttons
	MESHGROUP* gr = oapiMeshGroup(meshVC, GR_BTN_KILLROT);
	initPos[GR_BTN_KILLROT] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_HORLEVEL);
	initPos[GR_BTN_HORLEVEL] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_PROGRADE);
	initPos[GR_BTN_PROGRADE] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_RETROGRADE);
	initPos[GR_BTN_RETROGRADE] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_ORBNORMAL);
	initPos[GR_BTN_ORBNORMAL] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_ORBANORMAL);
	initPos[GR_BTN_ORBANORMAL] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	gr = oapiMeshGroup(meshVC, GR_BTN_HOLDALT);
	initPos[GR_BTN_HOLDALT] = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);

	VECTOR3 vertex, axis;

	// gear
	animGear = CreateAnimation(0);
	static UINT nicheGr[3] = {14, 20, 30}; // , 15, 16, 17, 18, 23, 24, 25, 26, 31, 32
	static MGROUP_TRANSLATE nicheTr (2, nicheGr, 3, _V(0, -0.01, 0));
	ANIMATIONCOMPONENT_HANDLE nicheAnim = AddAnimationComponent(animGear, 0, 0.05, &nicheTr);

	// right wheel
	gr = oapiMeshGroup(meshGear, 15);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT rws1Gr[2] = {15, 16};
	static MGROUP_ROTATE rws1Tr (2, rws1Gr, 2, vertex, _V(0,0,1), (float) (150 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &rws1Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 18);
	vertex = _V(gr->Vtx[1].x, gr->Vtx[1].y, gr->Vtx[1].z);
	static UINT rws2Gr[2] = {17, 18};
	static MGROUP_ROTATE rws2Tr (2, rws2Gr, 2, vertex, _V(0,0,1), (float) (-90 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &rws2Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 11);
	vertex = _V(gr->Vtx[13].x, gr->Vtx[13].y, gr->Vtx[13].z);
	static UINT rwLeverGr[1] = {11};
	static MGROUP_ROTATE rwLeverTr (2, rwLeverGr, 1, vertex, _V(0,0,1), (float) (-38 * RAD));
	ANIMATIONCOMPONENT_HANDLE rwLeverAnim = AddAnimationComponent(animGear, 0.4, 1, &rwLeverTr);

	gr = oapiMeshGroup(meshGear, 2);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT rwGr[5] = {0, 1, 2, 12, 13};
	static MGROUP_ROTATE rwTr (2, rwGr, 5, vertex, _V(0,0,1), (float) (-39 * RAD));
	AddAnimationComponent(animGear, 0.4, 1, &rwTr, rwLeverAnim);

	// left wheel
	gr = oapiMeshGroup(meshGear, 23);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT lws1Gr[2] = {23, 24};
	static MGROUP_ROTATE lws1Tr (2, lws1Gr, 2, vertex, _V(0,0,1), (float) (-150 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &lws1Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 26);
	vertex = _V(gr->Vtx[1].x, gr->Vtx[1].y, gr->Vtx[1].z);
	static UINT lws2Gr[2] = {25, 26};
	static MGROUP_ROTATE lws2Tr (2, lws2Gr, 2, vertex, _V(0,0,1), (float) (90 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &lws2Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 19);
	vertex = _V(gr->Vtx[13].x, gr->Vtx[13].y, gr->Vtx[13].z);
	static UINT lwLeverGr[1] = {19};
	static MGROUP_ROTATE lwLeverTr (2, lwLeverGr, 1, vertex, _V(0,0,1), (float) (38 * RAD));
	ANIMATIONCOMPONENT_HANDLE lwLeverAnim = AddAnimationComponent(animGear, 0.4, 1, &lwLeverTr);

	gr = oapiMeshGroup(meshGear, 6);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT lwGr[5] = {6, 7, 8, 21, 22};
	static MGROUP_ROTATE lwTr (2, lwGr, 5, vertex, _V(0,0,1), (float) (39 * RAD));
	AddAnimationComponent(animGear, 0.4, 1, &lwTr, lwLeverAnim);

	// front wheel
	gr = oapiMeshGroup(meshGear, 31);
	vertex = _V(gr->Vtx[3].x, gr->Vtx[3].y, gr->Vtx[3].z);
	axis = _V(gr->Vtx[1].x, gr->Vtx[1].y, gr->Vtx[1].z) - vertex;
	axis = axis / length(axis);
	static UINT fws1Gr[1] = {31};
	static MGROUP_ROTATE fws1Tr (2, fws1Gr, 1, vertex, axis, (float) (-90 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &fws1Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 32);
	vertex = _V(gr->Vtx[2].x, gr->Vtx[2].y, gr->Vtx[2].z);
	axis = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z) - vertex;
	axis = axis / length(axis);
	static UINT fws2Gr[2] = {32};
	static MGROUP_ROTATE fws2Tr (2, fws2Gr, 1, vertex, axis, (float) (90 * RAD));
	AddAnimationComponent(animGear, 0, 0.5, &fws2Tr, nicheAnim);

	gr = oapiMeshGroup(meshGear, 29);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT fwGr[6] = {3, 4, 5, 27, 28, 29};
	static MGROUP_ROTATE fwTr (2, fwGr, 6, vertex, _V(1, 0, 0), (float) (60 * RAD));
	AddAnimationComponent(animGear, 0.4, 1, &fwTr);

	// gear lever
	gr = oapiMeshGroup(meshVC, GR_GEAR_LEVER);
	vertex = _V(gr->Vtx[37].x, gr->Vtx[37].y, gr->Vtx[37].z);
	axis = _V(-1, 0, 0);
	RotateVector(axis, _V(0, 1, 0), -32.972 * RAD);
	static UINT gearLeverGr[1] = {GR_GEAR_LEVER};
	static MGROUP_ROTATE gearLeverTr (1, gearLeverGr, 1, vertex, axis, (float) (30 * RAD));
	animGearLever = CreateAnimation(0.5);
	AddAnimationComponent(animGearLever, 0, 1, &gearLeverTr);

	// dock
	animDock = CreateAnimation(0);
	static UINT dockNicheGr[1] = {34};
	static MGROUP_TRANSLATE dockNicheTr (2, dockNicheGr, 1, _V(0, 0.01, 0));
	ANIMATIONCOMPONENT_HANDLE dockNicheAnim = AddAnimationComponent(animDock, 0, 0.05, &dockNicheTr);

	gr = oapiMeshGroup(meshGear, 35);
	vertex = _V(gr->Vtx[1].x, gr->Vtx[1].y, gr->Vtx[1].z);
	static UINT dps1Gr[1] = {35};
	static MGROUP_ROTATE dps1Tr (2, dps1Gr, 1, vertex, _V(0,0,1), (float) (150 * RAD));
	ANIMATIONCOMPONENT_HANDLE dps1Anim = AddAnimationComponent(animDock, 0, 0.5, &dps1Tr, dockNicheAnim);

	gr = oapiMeshGroup(meshGear, 36);
	vertex = _V(gr->Vtx[1].x, gr->Vtx[1].y, gr->Vtx[1].z);
	static UINT dps2Gr[1] = {36};
	static MGROUP_ROTATE dps2Tr (2, dps2Gr, 1, vertex, _V(0,0,1), (float) (-170 * RAD));
	AddAnimationComponent(animDock, 0, 0.5, &dps2Tr, dps1Anim);

	gr = oapiMeshGroup(meshGear, 38);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT dps4Gr[1] = {38};
	static MGROUP_ROTATE dps4Tr (2, dps4Gr, 1, vertex, _V(0,0,1), (float) (-150 * RAD));
	ANIMATIONCOMPONENT_HANDLE dps4Anim = AddAnimationComponent(animDock, 0, 0.5, &dps4Tr, dockNicheAnim);

	gr = oapiMeshGroup(meshGear, 37);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	static UINT dps3Gr[1] = {37};
	static MGROUP_ROTATE dps3Tr (2, dps3Gr, 1, vertex, _V(0,0,1), (float) (170 * RAD));
	AddAnimationComponent(animDock, 0, 0.5, &dps3Tr, dps4Anim);

	static UINT dockTubeGr[1] = {33};
	static MGROUP_TRANSLATE dockTubeTr (2, dockTubeGr, 1, _V(0, 0.2, 0)); // _V(0, 0.185, 0)
	AddAnimationComponent(animDock, 0.4, 1, &dockTubeTr);

	// dock switch
	gr = oapiMeshGroup(meshVC, GR_DOCK_SWITCH);
	vertex = _V(gr->Vtx[14].x, gr->Vtx[14].y, gr->Vtx[14].z);
	axis = _V(-1, 0, 0);
	RotateVector(axis, _V(0, 1, 0), 32.604 * RAD);
	static UINT dockSwitchGr[1] = {GR_DOCK_SWITCH};
	static MGROUP_ROTATE dockSwitchTr (1, dockSwitchGr, 1, vertex, axis, (float) (-20 * RAD));
	animDockSwitch = CreateAnimation(0.5);
	AddAnimationComponent(animDockSwitch, 0, 1, &dockSwitchTr);

	// RCS mode
	gr = oapiMeshGroup(meshVC, GR_ATT_MODE);
	vertex = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	axis = _V(0, 1, 0);
	RotateVector(axis, _V(1, 0, 0), -55.495 * RAD);
	static UINT rcsModeGr[1] = {GR_ATT_MODE};
	static MGROUP_ROTATE rcsModeTr (1, rcsModeGr, 1, vertex, axis, (float) (90 * RAD));
	animRCSMode = CreateAnimation(0.5);
	AddAnimationComponent(animRCSMode, 0, 1, &rcsModeTr);

	// main thrust lever
	gr = oapiMeshGroup(meshVC, GR_MAIN_THRUST_LEVER);
	vertex = _V(gr->Vtx[50].x, gr->Vtx[50].y, gr->Vtx[50].z);
	static UINT mainThrustGr[1] = {GR_MAIN_THRUST_LEVER};
	static MGROUP_ROTATE mainThrustTr (1, mainThrustGr, 1, vertex, _V(1, 0, 0), (float) (90 * RAD));
	animMainThrust = CreateAnimation(0.5);
	AddAnimationComponent(animMainThrust, 0, 1, &mainThrustTr);

	// hover thrust lever
	gr = oapiMeshGroup(meshVC, GR_HOVER_THRUST_LEVER);
	vertex = _V(gr->Vtx[50].x, gr->Vtx[50].y, gr->Vtx[50].z);
	static UINT hoverThrustGr[1] = {GR_HOVER_THRUST_LEVER};
	static MGROUP_ROTATE hoverThrustTr (1, hoverThrustGr, 1, vertex, _V(1, 0, 0), (float) (90 * RAD));
	animHoverThrust = CreateAnimation(0.5);
	AddAnimationComponent(animHoverThrust, 0, 1, &hoverThrustTr);
}

void ShuttlePB::ReleaseSurfaces() {
	for (int i = 0; i < nsurf; i++) {
		if (srf[i]) {
			oapiDestroySurface(srf[i]);
			srf[i] = NULL;
		}
	}
}

void ShuttlePB::clbkSetClassCaps(FILEHANDLE cfg) {
	THRUSTER_HANDLE th_main, th_hover, th_rcs[14], th_group[4];

	// physical vessel parameters
	SetSize (PB_SIZE);
	SetEmptyMass (PB_EMPTYMASS);
	SetPMI (PB_PMI);
	SetCrossSections (PB_CS);
	SetRotDrag (PB_RD);
	SetTouchdownPoints (TDP_GEARDOWN, 6);

	// docking port definitions
	SetDockParams (PB_DOCK_POS, PB_DOCK_DIR, PB_DOCK_ROT);

	// airfoil definitions
	CreateAirfoil3 (LIFT_VERTICAL,   PB_COP, vlift, NULL, PB_VLIFT_C, PB_VLIFT_S, PB_VLIFT_A);
	CreateAirfoil3 (LIFT_HORIZONTAL, PB_COP, hlift, NULL, PB_HLIFT_C, PB_HLIFT_S, PB_HLIFT_A);

	// control surface animations
	UINT anim_Laileron = CreateAnimation (0.5);
	UINT anim_Raileron = CreateAnimation (0.5);
	UINT anim_elevator = CreateAnimation (0.5);
	AddAnimationComponent (anim_Laileron, 0, 1, &trans_Laileron);
	AddAnimationComponent (anim_Raileron, 0, 1, &trans_Raileron);
	AddAnimationComponent (anim_elevator, 0, 1, &trans_Lelevator);
	AddAnimationComponent (anim_elevator, 0, 1, &trans_Relevator);

	// aerodynamic control surface defintions
	CreateControlSurface (AIRCTRL_ELEVATOR, 1.5, 0.7, _V( 0,0,-2.5), AIRCTRL_AXIS_XPOS, anim_elevator);
	CreateControlSurface (AIRCTRL_AILERON, 1.5, 0.25, _V( 1,0,-2.5), AIRCTRL_AXIS_XPOS, anim_Laileron);
	CreateControlSurface (AIRCTRL_AILERON, 1.5, 0.25, _V(-1,0,-2.5), AIRCTRL_AXIS_XNEG, anim_Raileron);

	// propellant resources
	ph_main = CreatePropellantResource (PB_FUELMASS);

	// main engine
	th_main = CreateThruster (_V(0,0,-4.35), _V(0,0,1), PB_MAXMAINTH, ph_main, PB_ISP);
	CreateThrusterGroup (&th_main, 1, THGROUP_MAIN);
	AddExhaust (th_main, 8, 1, _V(0,0.3,-4.35), _V(0,0,-1));

	PARTICLESTREAMSPEC contrail_main = {
		0, 5.0, 16, 200, 0.15, 1.0, 5, 3.0, PARTICLESTREAMSPEC::DIFFUSE,
		PARTICLESTREAMSPEC::LVL_PSQRT, 0, 2,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-4, 1
	};
	PARTICLESTREAMSPEC exhaust_main = {
		0, 2.0, 20, 200, 0.05, 0.1, 8, 1.0, PARTICLESTREAMSPEC::EMISSIVE,
		PARTICLESTREAMSPEC::LVL_SQRT, 0, 1,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-5, 0.1
	};
	AddExhaustStream (th_main, _V(0,0.3,-10), &contrail_main);
	AddExhaustStream (th_main, _V(0,0.3,-5), &exhaust_main);

	// hover engine
	th_hover = CreateThruster (_V(0,-1.5,0), _V(0,1,0), PB_MAXHOVERTH, ph_main, PB_ISP);
	CreateThrusterGroup (&th_hover, 1, THGROUP_HOVER);
	AddExhaust (th_hover, 8, 1, _V(0,-1.5,1), _V(0,-1,0));
	AddExhaust (th_hover, 8, 1, _V(0,-1.5,-1), _V(0,-1,0));

	PARTICLESTREAMSPEC contrail_hover = {
		0, 5.0, 8, 200, 0.15, 1.0, 5, 3.0, PARTICLESTREAMSPEC::DIFFUSE,
		PARTICLESTREAMSPEC::LVL_PSQRT, 0, 2,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-4, 1
	};
	PARTICLESTREAMSPEC exhaust_hover = {
		0, 2.0, 10, 200, 0.05, 0.05, 8, 1.0, PARTICLESTREAMSPEC::EMISSIVE,
		PARTICLESTREAMSPEC::LVL_SQRT, 0, 1,
		PARTICLESTREAMSPEC::ATM_PLOG, 1e-5, 0.1
	};

	AddExhaustStream (th_hover, _V(0,-3, 1), &contrail_hover);
	AddExhaustStream (th_hover, _V(0,-3,-1), &contrail_hover);
	AddExhaustStream (th_hover, _V(0,-2, 1), &exhaust_hover);
	AddExhaustStream (th_hover, _V(0,-2,-1), &exhaust_hover);

	// RCS engines
	th_rcs[ 0] = CreateThruster (_V( 1,0, 3), _V(0, 1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 1] = CreateThruster (_V( 1,0, 3), _V(0,-1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 2] = CreateThruster (_V(-1,0, 3), _V(0, 1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 3] = CreateThruster (_V(-1,0, 3), _V(0,-1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 4] = CreateThruster (_V( 1,0,-3), _V(0, 1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 5] = CreateThruster (_V( 1,0,-3), _V(0,-1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 6] = CreateThruster (_V(-1,0,-3), _V(0, 1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 7] = CreateThruster (_V(-1,0,-3), _V(0,-1,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 8] = CreateThruster (_V( 1,0, 3), _V(-1,0,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[ 9] = CreateThruster (_V(-1,0, 3), _V( 1,0,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[10] = CreateThruster (_V( 1,0,-3), _V(-1,0,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[11] = CreateThruster (_V(-1,0,-3), _V( 1,0,0), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[12] = CreateThruster (_V( 0,0,-3), _V(0,0, 1), PB_MAXRCSTH, ph_main, PB_ISP);
	th_rcs[13] = CreateThruster (_V( 0,0, 3), _V(0,0,-1), PB_MAXRCSTH, ph_main, PB_ISP);

	th_group[0] = th_rcs[0];
	th_group[1] = th_rcs[2];
	th_group[2] = th_rcs[5];
	th_group[3] = th_rcs[7];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_PITCHUP);

	th_group[0] = th_rcs[1];
	th_group[1] = th_rcs[3];
	th_group[2] = th_rcs[4];
	th_group[3] = th_rcs[6];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_PITCHDOWN);

	th_group[0] = th_rcs[0];
	th_group[1] = th_rcs[4];
	th_group[2] = th_rcs[3];
	th_group[3] = th_rcs[7];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_BANKLEFT);

	th_group[0] = th_rcs[1];
	th_group[1] = th_rcs[5];
	th_group[2] = th_rcs[2];
	th_group[3] = th_rcs[6];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_BANKRIGHT);

	th_group[0] = th_rcs[0];
	th_group[1] = th_rcs[4];
	th_group[2] = th_rcs[2];
	th_group[3] = th_rcs[6];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_UP);

	th_group[0] = th_rcs[1];
	th_group[1] = th_rcs[5];
	th_group[2] = th_rcs[3];
	th_group[3] = th_rcs[7];
	CreateThrusterGroup (th_group, 4, THGROUP_ATT_DOWN);

	th_group[0] = th_rcs[8];
	th_group[1] = th_rcs[11];
	CreateThrusterGroup (th_group, 2, THGROUP_ATT_YAWLEFT);

	th_group[0] = th_rcs[9];
	th_group[1] = th_rcs[10];
	CreateThrusterGroup (th_group, 2, THGROUP_ATT_YAWRIGHT);

	th_group[0] = th_rcs[8];
	th_group[1] = th_rcs[10];
	CreateThrusterGroup (th_group, 2, THGROUP_ATT_LEFT);

	th_group[0] = th_rcs[9];
	th_group[1] = th_rcs[11];
	CreateThrusterGroup (th_group, 2, THGROUP_ATT_RIGHT);

	CreateThrusterGroup (th_rcs+12, 1, THGROUP_ATT_FORWARD);
	CreateThrusterGroup (th_rcs+13, 1, THGROUP_ATT_BACK);

	// camera parameters
	SetCameraOffset (_V(0,0.8,0));

	// associate a mesh for the visual
	//AddMesh ("ShuttlePB");

	// visual specs
	SetMeshVisibilityMode(AddMesh("ShuttlePB"), MESHVIS_EXTERNAL);

	// my
	SetSurfaceFrictionCoeff(0.1, 0.5);
	SetCameraRotationRange(RAD*170, RAD*170, RAD*80, RAD*80);

	SetMeshVisibilityMode(AddMesh(meshVC), MESHVIS_VC);
	SetMeshVisibilityMode(AddMesh(meshGear), MESHVIS_EXTERNAL);

	// UMMU
	InitUmmu();
}

// ==============================================================
// Airfoil lift/drag functions
// ==============================================================

void ShuttlePB::vlift (VESSEL *v, double aoa, double M, double Re,
	void *context, double *cl, double *cm, double *cd)
{
	static const double clp[] = {  // lift coefficient from -pi to pi in 10deg steps
		-0.1,-0.5,-0.4,-0.1,0,0,0,0,0,0,0,0,0,0,-0.2,-0.6,-0.6,-0.4,0.2,0.5,0.9,0.8,0.2,0,0,0,0,0,0,0,0,0,0.1,0.4,0.5,0.3,-0.1,-0.5
	};
	static const double aoa_step = 10.0*RAD;
	double a, fidx, saoa = sin(aoa);
	a = modf((aoa+PI)/aoa_step, &fidx);
	int idx = (int)(fidx+0.5);
	*cl = clp[idx]*(1.0-a) + clp[idx+1]*a;     // linear interpolation
	*cm = 0.0; //-0.03*sin(aoa-0.1);
	*cd = 0.03 + 0.4*saoa*saoa;                // profile drag
	*cd += oapiGetInducedDrag (*cl, 1.0, 0.5); // induced drag
	*cd += oapiGetWaveDrag (M, 0.75, 1.0, 1.1, 0.04);  // wave drag
}

void ShuttlePB::hlift (VESSEL *v, double aoa, double M, double Re,
	void *context, double *cl, double *cm, double *cd)
{
	static const double clp[] = {  // lift coefficient from -pi to pi in 45deg steps
		0,0.4,0,-0.4,0,0.4,0,-0.4,0,0.4
	};
	static const double aoa_step = 45.0*RAD;
	double a, fidx;
	a = modf((aoa+PI)/aoa_step, &fidx);
	int idx = (int)(fidx+0.5);
	*cl = clp[idx]*(1.0-a) + clp[idx+1]*a;     // linear interpolation
	*cm = 0.0;
	*cd = 0.03;
	*cd += oapiGetInducedDrag (*cl, 1.5, 0.6); // induced drag
	*cd += oapiGetWaveDrag (M, 0.75, 1.0, 1.1, 0.04);  // wave drag
}

// ==============================================================
// Final initialization. Called after SetClassCaps and LoadState.
// ==============================================================
void ShuttlePB::clbkPostCreation() {
	//soundId = ConnectToOrbiterSoundDLL3(GetHandle());
	//ReplaceStockSound3(MyID, "Sound\\Bullet\\main.wav", REPLACE_MAIN_THRUST);

	SetAnimation(animGear, gearProc);
	SetAnimation(animGearLever, gearStatus == OPEN || gearStatus == OPENING);
	SetAnimation(animDock, dockProc);
	SetAnimation(animDockSwitch, dockStatus == OPEN || dockStatus == OPENING);

	if (isNew) {
		crew.AddCrewMember("Peter Falcon",41,65,74,"Capt");
		crew.AddCrewMember("Fanny Gorgeous",27,67,55,"Eng");
		crew.AddCrewMember("George HealGood",15,70,45,"Doc");
	}
	double mass = 500;
	for (int i = 0; i < crew.GetCrewTotalNumber(); i++) {
		mass += crew.GetCrewWeightBySlotNumber(i);
	}
	SetEmptyMass(mass);
}

void ShuttlePB::clbkVisualCreated(VISHANDLE vis, int refcount) {
	oapiWriteLog("PB: visual created");
	// navmode
	UpdateNavmodeButton(NAVMODE_KILLROT, GR_BTN_KILLROT);
	UpdateNavmodeButton(NAVMODE_HLEVEL, GR_BTN_HORLEVEL);
	UpdateNavmodeButton(NAVMODE_PROGRADE, GR_BTN_PROGRADE);
	UpdateNavmodeButton(NAVMODE_RETROGRADE, GR_BTN_RETROGRADE);
	UpdateNavmodeButton(NAVMODE_NORMAL, GR_BTN_ORBNORMAL);
	UpdateNavmodeButton(NAVMODE_ANTINORMAL, GR_BTN_ORBANORMAL);
	UpdateNavmodeButton(NAVMODE_HOLDALT, GR_BTN_HOLDALT);
}

// ==============================================================
// Write status to scenario file
// ==============================================================
void ShuttlePB::clbkSaveState (FILEHANDLE scn) {
	VESSEL2::clbkSaveState(scn);

	char buf[256];

	// animations
	sprintf(buf, "%d %lf", gearStatus, gearProc);
	oapiWriteScenario_string(scn, "GEAR", buf);

	sprintf(buf, "%d %lf", dockStatus, dockProc);
	oapiWriteScenario_string(scn, "DOCK", buf);

	crew.SaveAllMembersInOrbiterScenarios(scn);
}

// ==============================================================
// Read status from scenario file. Called after SetClassCaps.
// ==============================================================
void ShuttlePB::clbkLoadStateEx (FILEHANDLE scn, void *status) {
	isNew = false;
	char *line;
	while (oapiReadScenario_nextline(scn, line)) {
		if (!strnicmp(line, "GEAR", 4)) {
			sscanf(line + 4, "%d %lf", &gearStatus, &gearProc);
		} else if (!strnicmp(line, "DOCK", 4)) {
			sscanf(line + 4, "%d %lf", &dockStatus, &dockProc);
		} else if (!strnicmp(line, "UMMUCREW", 8)) {
			crew.LoadAllMembersFromOrbiterScenario(line);
		} else {
			ParseScenarioLineEx(line, status);
		}
	}
}

// ==============================================================
// Post step processings
// ==============================================================
void ShuttlePB::clbkPostStep(double simt, double simdt, double mjd) {
	// UMMU
	oapiWriteLog("PB: Post step");

	int ummuCheckResult = crew.ProcessUniversalMMu();
	if (ummuCheckResult == UMMU_TRANSFERED_TO_OUR_SHIP || ummuCheckResult == UMMU_RETURNED_TO_OUR_SHIP) {
		/*double crewMemberMass = crew.GetCrewWeightByName(crew.GetLastEnteredCrewName());
		SetEmptyMass(GetEmptyMass() + crewMemberMass);*/
		string msg = crew.GetLastEnteredCrewName();
		msg.append(" has entered the ship");
		ShowMessage(msg, RGB(0, 255, 0));
		oapiVCTriggerRedrawArea(-1, AID_CREW_SCREEN);
	}

	// vertical speed
	if (altitude >= 0) {
		vspeed = (GetAltitude() - altitude) / simdt;
	} else {
		vspeed = 0;
	}
	altitude = GetAltitude();

	// animations
	if (gearStatus >= CLOSING) {
		double da = simdt * 0.1;
		if (gearStatus == CLOSING) {
			if (gearProc > 0.0) {
				gearProc = max(0.0, gearProc - da);
			} else {
				gearStatus = CLOSED;
			}
		} else { // opening
			if (gearProc < 1.0) {
				gearProc = min(1.0, gearProc + da);
			} else {
				gearStatus = OPEN;
			}
		}
		SetAnimation(animGear, gearProc);
	}

	if (dockStatus >= CLOSING) {
		double da = simdt * 0.1;
		if (dockStatus == CLOSING) {
			if (dockProc > 0.0) {
				dockProc = max(0.0, dockProc - da);
			} else {
				dockStatus = CLOSED;
			}
		} else { // opening
			if (dockProc < 1.0) {
				dockProc = min(1.0, dockProc + da);
			} else {
				dockStatus = OPEN;
			}
		}
		SetAnimation(animDock, dockProc);
		oapiVCTriggerRedrawArea(-1, AID_DOCK_STATUS);
	}
}

int ShuttlePB::clbkConsumeBufferedKey(DWORD key, bool down, char *kstate) {
	/*if (KEYDOWN(kstate, OAPI_KEY_DIVIDE)) { // switch RCS mode
		//if (ToggleAttitudeMode()) oapiTriggerRedrawArea (0, 0, AID_ATT_MODE);
		//return 1;
	} else*/ 
	if (KEYMOD_CONTROL(kstate) && KEYDOWN(kstate, OAPI_KEY_MULTIPLY)) {
		SetThrusterLevel(th_hover, 0);
	} else if (KEYMOD_LCONTROL(kstate) && KEYDOWN(kstate, OAPI_KEY_L)) {
		/*UINT meshCount = 1;
		MATERIAL* material;
		DEVMESHHANDLE mesh;
		for (UINT i = 0; i < meshCount; i++) {
			mesh = GetDevMesh(*oapiObjectVisualPtr(GetHandle()), i);
			DWORD materialCount = oapiMeshMaterialCount(mesh);
			for (DWORD mi = 0; mi < materialCount; mi++) {
				material = oapiMeshMaterial(mesh, mi);
				if (material->emissive.g <= 0.1) {
					material->emissive.r = material->ambient.r;
					material->emissive.g = material->ambient.g;
					material->emissive.b = material->ambient.b;
				}
			}
		}
		int vesselCount = oapiGetVesselCount();
		VECTOR3 relPos;
		for (int vesselIndex = 0; vesselIndex < vesselCount; vesselIndex++) {
			OBJHANDLE hObject = oapiGetVesselByIndex(vesselIndex);
			if (hObject == GetHandle()) {
				continue;
			}
			GetRelativePos(hObject, relPos);
			if (length(relPos) < 500) {
				VESSEL* vessel = oapiGetVesselInterface(hObject);
				int meshIndex = 0;
				while (true) {
					mesh = vessel->GetMesh(*oapiObjectVisualPtr(hObject), meshIndex);
					if (mesh == NULL) break;
					DWORD materialCount = oapiMeshMaterialCount(mesh);
					for (DWORD mi = 0; mi < materialCount; mi++) {
						material = oapiMeshMaterial(mesh, mi);
						if (material->emissive.g <= 0.1) {
							material->emissive.r = material->ambient.r;
							material->emissive.g = material->ambient.g;
							material->emissive.b = material->ambient.b;
						}
					}
					meshIndex++;
				}
			}
		}*/
		return 1;
	}
	return 0;
}

int ShuttlePB::clbkConsumeDirectKey(char *kstate) {
	/*if (KEYDOWN(kstate, OAPI_KEY_INSERT) || KEYDOWN(kstate, OAPI_KEY_DELETE)) {
		//oapiVCTriggerRedrawArea(-1, AID_TRIM_STATUS);
	} else if (KEYDOWN(kstate, OAPI_KEY_ADD) || KEYDOWN(kstate, OAPI_KEY_SUBTRACT)
			|| KEYDOWN(kstate, OAPI_KEY_MULTIPLY) || KEYDOWN(kstate, OAPI_KEY_NUMPAD0)
			|| KEYDOWN(kstate, OAPI_KEY_PERIOD)) {
		// redraw engines indicators
	} else*/ if (KEYDOWN(kstate, OAPI_KEY_DOWN) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 pos;
		GetCameraOffset(pos);
		if (KEYMOD_RCONTROL(kstate)) {
			pos.y -= 0.02;
		} else {
			VECTOR3 dir;
			GetCameraDefaultDirection(dir);
			pos -= dir * 0.02;
		}
		if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_UP) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 pos;
		GetCameraOffset(pos);
		if (KEYMOD_RCONTROL(kstate)) {
			pos.y += 0.02;
		} else {
			VECTOR3 dir;
			GetCameraDefaultDirection(dir);
			pos += dir * 0.02;
		}
		if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_LEFT) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 dir;
		GetCameraDefaultDirection(dir);
		if (KEYMOD_RALT(kstate)) {
			RotateVector(dir, _V(0, 1, 0), -0.05);
			SetCameraDefaultDirection(dir);
		} else {
			VECTOR3 pos;
			GetCameraOffset(pos);
			RotateVector(dir, _V(0, 1, 0), -PI/2);
			pos += dir * 0.02;
			if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		}
		return 1;
	} else if (KEYDOWN(kstate, OAPI_KEY_RIGHT) && KEYMOD_RSHIFT(kstate)) {
		VECTOR3 dir;
		GetCameraDefaultDirection(dir);
		if (KEYMOD_RALT(kstate)) {
			RotateVector(dir, _V(0, 1, 0), 0.05);
			SetCameraDefaultDirection(dir);
		} else {
			VECTOR3 pos;
			GetCameraOffset(pos);
			RotateVector(dir, _V(0, 1, 0), PI/2);
			pos += dir * 0.02;
			if (IsCameraPositionCorrect(pos)) SetCameraOffset(pos);
		}
		return 1;
	}
	return 0;
}

bool ShuttlePB::IsCameraPositionCorrect(const VECTOR3& pos) {
	return pos.z >= 0 && pos.z <= 1.4 && pos.x >= -1 && pos.x <= 1 && pos.y >= -0.6 && pos.y <= 1;
}

void ShuttlePB::InitUmmu() {
	ummuInitReturnCode = crew.InitUmmu(GetHandle());
	if (ummuInitReturnCode != 1) {
		return;
	}

	crew.SetCrewWeightUpdateShipWeightAutomatically(FALSE);

	crew.DefineAirLockShape(TRUE, 2, 4, -2, 2, -1.8f, 0.8f);
	crew.SetMembersPosRotOnEVA(_V(3,0,-0.8),_V(0,0,0));

	crew.SetMaxSeatAvailableInShip(3);
}

void ShuttlePB::clbkDockEvent(int dock, OBJHANDLE mate) {
	char msg[50];
	if (mate != NULL) {
		sprintf(msg, "Docked with %s", oapiGetVesselInterface(mate)->GetName());
	} else {
		sprintf(msg, "Undocking confirmed");
	}
	ShowMessage(msg);
	oapiVCTriggerRedrawArea(-1, AID_BTN_UNDOCK);
}
