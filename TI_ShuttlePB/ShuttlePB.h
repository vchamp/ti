#ifndef __ShuttlePB_H__
#define __ShuttlePB_H__

#include "Common.h"
#include "Utils.h"

// -----------------------------------
// Global GDI objects
// -----------------------------------
typedef struct {
	HINSTANCE hDLL;
	HFONT hFont[4];
	HPEN hPen[1];
	HBRUSH hBrush[3];
} GDIParams;
extern GDIParams g_Param;
#define LOADBMP(id) (LoadBitmap (g_Param.hDLL, MAKEINTRESOURCE (id)))

// ==============================================================
// Some vessel parameters
// ==============================================================
const double  PB_SIZE       = 3.5;             // mean radius [m]
const VECTOR3 PB_CS         = {10.5,15.0,5.8}; // x,y,z cross sections [m^2]
const VECTOR3 PB_PMI        = {2.28,2.31,0.79};// principal moments of inertia (mass-normalised) [m^2]
const VECTOR3 PB_RD         = {0.025,0.025,0.02};//{0.05,0.1,0.05};  // rotation drag coefficients
const double  PB_EMPTYMASS  = 500.0;           // empty vessel mass [kg]
const double  PB_FUELMASS   = 750.0;           // max fuel mass [kg]
const double  PB_ISP        = 5e4;             // fuel-specific impulse [m/s]
const VECTOR3 PB_TDP[3]     = {{0,-1.5,2},{-1,-1.5,-1.5},{1,-1.5,-1.5}}; // touchdown points [m]
const VECTOR3 PB_COP        = {0,0,0};//{0,0,-0.1};      // centre of pressure for airfoils [m]
const double  PB_VLIFT_C    = 2.0;             // chord length [m]
const double  PB_VLIFT_S    = 2.0;             // wing area [m^2]
const double  PB_VLIFT_A    = 2.5;             // wing aspect ratio
const double  PB_HLIFT_C    = 2.0;             // chord length [m]
const double  PB_HLIFT_S    = 1.5;             // wing area [m^2]
const double  PB_HLIFT_A    = 2.0;             // wing aspect ratio

const double  PB_MAXMAINTH  = 3e4;             
const double  PB_MAXHOVERTH = 1.5e4;
const double  PB_MAXRCSTH   = 2e2;

const VECTOR3 PB_DOCK_POS   = {0,1.3,-1};      // docking port location [m]
const VECTOR3 PB_DOCK_DIR   = {0,1,0};         // docking port approach direction
const VECTOR3 PB_DOCK_ROT   = {0,0,-1};        // docking port alignment direction

// ==============================================================
// VC parameters
// ==============================================================

const int GR_MFD1_SCREEN = 17;
const int GR_MFD2_SCREEN = 21;
const int GR_MFD3_SCREEN = 25;
const int GR_MFD1_BUTTONS = 16;
const int GR_MFD2_BUTTONS = 19;
const int GR_MFD3_BUTTONS = 23;
const int GR_MFD1_MBUTTONS = 15;
const int GR_MFD2_MBUTTONS = 20;
const int GR_MFD3_MBUTTONS = 24;

const int GR_HUD1 = 59;

const int GR_BTN_KILLROT = 30;
const int GR_BTN_HORLEVEL = 31;
const int GR_BTN_HOLDALT = 32;
const int GR_BTN_PROGRADE = 29;
const int GR_BTN_RETROGRADE = 28;
const int GR_BTN_ORBNORMAL = 27;
const int GR_BTN_ORBANORMAL = 26;

const int GR_MAIN_THRUST_LEVER = 37;
const int GR_HOVER_THRUST_LEVER = 36;
const int GR_ATT_MODE = 52;
const int GR_CREW_SCREEN = 44;
const int GR_HUD_BUTTONS = 51;
const int GR_BTN_UNDOCK = 41;
const int GR_GEAR_LEVER = 46;
const int GR_DOCK_SWITCH = 49;

const int TEX_DYNAMIC1 = 4;
const int TEX_DYNAMIC2 = 6;

const int nsurf = 1;

// Input callback
/*typedef struct {
	int action;
	void* param;
} InputCallbackParams;
extern InputCallbackParams gInputCallbackParams;
bool InputCallback(void *id, char *str, void *usrdata);*/

class ShuttlePB : public VESSEL3 {
public:
	ShuttlePB(OBJHANDLE hVessel, int flightmodel);
	~ShuttlePB();
	void clbkSetClassCaps(FILEHANDLE cfg);
	void clbkSaveState(FILEHANDLE scn);
	void clbkLoadStateEx(FILEHANDLE scn, void *status);
	void clbkPostCreation();
	bool clbkLoadVC(int id);
	bool clbkVCMouseEvent(int id, int event, VECTOR3 &p);
	bool clbkVCRedrawEvent(int id, int event, SURFHANDLE surf);
	void clbkMFDMode(int mfd, int mode);
	void clbkNavMode(int mode, bool active);
	void clbkHUDMode(int mode);
	void clbkRCSMode(int mode);
	void clbkPostStep(double simt, double simdt, double mjd);
	int clbkConsumeBufferedKey(DWORD key, bool down, char *kstate);
	int clbkConsumeDirectKey(char *kstate);
	void clbkDrawHUD(int mode, const HUDPAINTSPEC *hps, HDC hDC);
	void clbkDockEvent(int dock, OBJHANDLE mate);
	void clbkVisualCreated(VISHANDLE vis, int refcount);

private:

	const double TDP_FRICTION_LNG = 2;
	const double TDP_FRICTION_LAT = 4;
	const double TDP_STIFFNESS = 5e6;
	const double TDP_DUMPING = 1e5;
	const double TDP_UP_STIFFNESS = 1e6;
	const double TDP_UP_DUMPING = 5e5;
	const TOUCHDOWNVTX TDP_GEARDOWN[6] = {
		{ _V(0, -1.5, 2), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(-1, -1.5, -1.5), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(1, -1.5, -1.5), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 0, 4), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 0, -3), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 5, 0), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG } };
	const TOUCHDOWNVTX TDP_GEARUP[6] = {
		{ _V(0,-1.5, 2), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(-1, -1.5, -1.5), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(1, -1.5, -1.5), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 0, 4), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 0, -3), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG },
		{ _V(0, 5, 0), TDP_STIFFNESS, TDP_DUMPING, TDP_FRICTION_LAT, TDP_FRICTION_LNG } };

	// standard PB members
	static void vlift (VESSEL *v, double aoa, double M, double Re,
		void *context, double *cl, double *cm, double *cd);
	static void hlift (VESSEL *v, double aoa, double M, double Re,
		void *context, double *cl, double *cm, double *cd);

	// transformations for control surface animations
	static MGROUP_ROTATE trans_Laileron, trans_Raileron;
	static MGROUP_ROTATE trans_Lelevator, trans_Relevator;

	// TI members

	list<Message> messages;
	double prevThrust[2];
	double altitude, vspeed;
	bool isNew;

	MESHHANDLE meshVC, meshGear;
	SURFHANDLE srf[nsurf];

	PROPELLANT_HANDLE ph_main;
	THRUSTER_HANDLE th_main, th_hover;
	THGROUP_HANDLE thg_main, thg_hover;

	map<int, VECTOR3> initPos;

	// animations
	UINT animGear, animGearLever, animDock, animDockSwitch, animMainThrust, animHoverThrust, animRCSMode;
	double gearProc, dockProc;
	AnimStatus gearStatus, dockStatus;

	void RedrawMFDButtons(SURFHANDLE surf, int mfd);
	void RedrawMFDControls(SURFHANDLE surf, int mfd);
	void RedrawHudButtons(SURFHANDLE surf);
	void RedrawDynamicStatus(SURFHANDLE surf);
	void RedrawMessageScreen(SURFHANDLE surf);
	void RedrawCrewScreen(SURFHANDLE surf);
	void RedrawDockStatus(SURFHANDLE surf);

	void ShowMessage(string newMessage, COLORREF newColor = RGB(0, 255, 0));
	void UpdateNavmodeButton(int navmode, int meshGroup);

	void SetClickArea(MESHGROUP* gr, int id, bool rect = true, const VECTOR3& revert = _V(0,0,0), const VECTOR3& maxShift = _V(0,0,0));
	void SetClickArea(MESHGROUP* gr, int id, int topLeftVertex, int topRightVertex, int bottomLeftVertex, int bottomRightVertex);

	void HandleCrewScreenClick(VECTOR3 &p);

	void DefineAnimations();
	void ReleaseSurfaces();

	bool IsCameraPositionCorrect(const VECTOR3& pos);

	// UMMU
	//UMMUCREWMANAGMENT crew;
	Crew crew;
	int ummuInitReturnCode;
	void InitUmmu();
	int selectedCrewMember;
	int selectedSuit;
};

#define AID_MFD1_BUTTONS 0
#define AID_MFD1_MBUTTONS 1
#define AID_MFD2_BUTTONS 2
#define AID_MFD2_MBUTTONS 3
#define AID_MFD3_BUTTONS 4
#define AID_MFD3_MBUTTONS 5

#define AID_BTN_KILLROT 11
#define AID_BTN_HORLEVEL 12
#define AID_BTN_PROGRADE 13
#define AID_BTN_RETROGRADE 14
#define AID_BTN_ORBNORMAL 15
#define AID_BTN_ORBANORMAL 16
#define AID_BTN_HOLDALT 17

#define AID_MAIN_THRUST_LEVER 19
#define AID_HOVER_THRUST_LEVER 20
#define AID_DYNAMIC_STATUS 21
#define AID_ATT_MODE 22
#define AID_MESSAGE_SCREEN 23
#define AID_CREW_SCREEN 24
#define AID_HUD_BUTTONS 25
#define AID_BTN_UNDOCK 26
#define AID_GEAR_LEVER 27
#define AID_DOCK_SWITCH 28
#define AID_DOCK_STATUS 29

//#define INPCLBK_NAME_CREWMEMBER 0

#endif //__ShuttlePB_H__
