#ifndef __COMMON_H__
#define __COMMON_H__

#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif
#define _WIN32_WINNT 0x0500

#include "orbitersdk.h"
#include "Crew.h"
//#include "PayloadManager.h"
//#include "OrbiterSoundSDK35.h"
//#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <list>

using namespace std;

typedef enum { CLOSED, OPEN, STOPPED, CLOSING, OPENING } AnimStatus;

typedef struct {
	string message;
	COLORREF color;
} Message;

#endif //__COMMON_H__
