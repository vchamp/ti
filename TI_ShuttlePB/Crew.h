#ifndef __CREW_H__
#define __CREW_H__

#pragma once

#include "orbitersdk.h"

#define NUMBER_OF_MMU_TYPE	255

const int UMMU_TRANSFERED_TO_OUR_SHIP = 1;
const int UMMU_RETURNED_TO_OUR_SHIP = 2;

const char* const UmmuMeshUsed[] = { "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10",
"f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20",
"f21", "f22", "f23", "f24", "f25" };
const char* const UmmuFunctionName[] = { "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10",
"f11", "f12", "f13", "f14", "f15", "f16", "f17", "f18", "f19", "f20",
"f21", "f22", "f23", "f24", "f25" };

class Crew {
public:
	int InitUmmu(OBJHANDLE vessel);
	void SetMaxSeatAvailableInShip(int n);

	void DefineAirLockShape(bool p1, int p2, int p3, int p4, int p5, int p6, int p7);
	void SetMembersPosRotOnEVA(VECTOR3 p1, VECTOR3 p2);
	void SetAlternateMeshToUseForEVASpacesuit(char* p);
	void SetCrewWeightUpdateShipWeightAutomatically(bool p);

	void SaveAllMembersInOrbiterScenarios(FILEHANDLE scn);
	void LoadAllMembersFromOrbiterScenario(char* line);

	void AddCrewMember(char*CrewName, int CrewAge, int CrewPulse, int CrewWeight, char* MiscId = NULL);
	int GetCrewTotalNumber();
	char* GetCrewNameBySlotNumber(int Slot);
	int GetCrewAgeBySlotNumber(int Slot);
	int GetCrewWeightBySlotNumber(int Slot);
	int GetCrewPulseBySlotNumber(int Slot);

	int ProcessUniversalMMu();
	char* GetLastEnteredCrewName();

	int EvaCrewMember(char*CrewName);
	char* GetLastEvaedCrewName();
};

#endif //__CREW_H__