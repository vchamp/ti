#include "ShuttlePB.h"
#include "resource.h"

bool ShuttlePB::clbkLoadVC(int id) {
	ReleaseSurfaces();

	switch (id) {
		case 0:
			SetCameraOffset(_V(0, 0.4, 0.5));
			SetCameraDefaultDirection(_V(0, 0, 1));
			oapiVCSetNeighbours(-1, -1, 0, -1);
			break;
	}

	// camera shift
	SetCameraMovement(_V(0,0,0.3), 0, 0, 
		_V(-0.5,0,0), 60 * RAD, 0, 
		_V(0.5,0,0), -60 * RAD, 0);

	// register mfd
	static VCMFDSPEC mfds_left = {1, GR_MFD1_SCREEN};
	oapiVCRegisterMFD(MFD_LEFT, &mfds_left);
	static VCMFDSPEC mfds_right = {1, GR_MFD2_SCREEN};
	oapiVCRegisterMFD(MFD_RIGHT, &mfds_right);
	static VCMFDSPEC mfds_user1 = {1, GR_MFD3_SCREEN};
	oapiVCRegisterMFD(MFD_USER1, &mfds_user1);

	// register HUD
	MESHGROUP* gr = oapiMeshGroup(meshVC, GR_HUD1);
	VECTOR3 hudCenter = _V(gr->Vtx[1].x + (gr->Vtx[0].x - gr->Vtx[1].x) / 2.0, 
		gr->Vtx[2].y + (gr->Vtx[0].y - gr->Vtx[2].y) / 2.0,
		gr->Vtx[0].z + (gr->Vtx[2].z - gr->Vtx[0].z) / 2.0);
	gr->UsrFlag = 2;
	static VCHUDSPEC hudSpec;
	hudSpec.nmesh = 1;
	hudSpec.ngroup = GR_HUD1;
	hudSpec.hudcnt = hudCenter;
	hudSpec.size = gr->Vtx[0].x - gr->Vtx[1].x;
	oapiVCRegisterHUD(&hudSpec);

	// clickable areas
	SURFHANDLE texDyn = oapiGetTextureHandle(meshVC, TEX_DYNAMIC1);

	oapiVCRegisterArea(AID_MFD1_BUTTONS, _R(0,0,60,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD1_BUTTONS), AID_MFD1_BUTTONS);
	oapiVCRegisterArea(AID_MFD1_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD1_MBUTTONS), AID_MFD1_MBUTTONS);

	oapiVCRegisterArea(AID_MFD2_BUTTONS, _R(60,0,120,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD2_BUTTONS), AID_MFD2_BUTTONS);
	oapiVCRegisterArea(AID_MFD2_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD2_MBUTTONS), AID_MFD2_MBUTTONS);

	oapiVCRegisterArea(AID_MFD3_BUTTONS, _R(120,0,180,102), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED|PANEL_MOUSE_LBUP|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD3_BUTTONS), AID_MFD3_BUTTONS);
	oapiVCRegisterArea(AID_MFD3_MBUTTONS, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_MFD3_MBUTTONS), AID_MFD3_MBUTTONS);

	oapiVCRegisterArea(AID_BTN_KILLROT, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_KILLROT), AID_BTN_KILLROT);
	oapiVCRegisterArea(AID_BTN_HORLEVEL, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_HORLEVEL), AID_BTN_HORLEVEL);
	oapiVCRegisterArea(AID_BTN_PROGRADE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_PROGRADE), AID_BTN_PROGRADE);
	oapiVCRegisterArea(AID_BTN_RETROGRADE, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_RETROGRADE), AID_BTN_RETROGRADE);
	oapiVCRegisterArea(AID_BTN_ORBNORMAL, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_ORBNORMAL), AID_BTN_ORBNORMAL);
	oapiVCRegisterArea(AID_BTN_ORBANORMAL, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_ORBANORMAL), AID_BTN_ORBANORMAL);
	oapiVCRegisterArea(AID_BTN_HOLDALT, PANEL_REDRAW_NEVER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_HOLDALT), AID_BTN_HOLDALT);

	oapiVCRegisterArea(AID_GEAR_LEVER, PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_GEAR_LEVER), AID_GEAR_LEVER, false);

	oapiVCRegisterArea(AID_DOCK_SWITCH, PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_DOCK_SWITCH), AID_DOCK_SWITCH);

	oapiVCRegisterArea(AID_MAIN_THRUST_LEVER, PANEL_REDRAW_ALWAYS, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED);
	oapiVCSetAreaClickmode_Quadrilateral(AID_MAIN_THRUST_LEVER,
		_V(-0.320f,-0.152f,0.998f), _V(-0.294f,-0.152f,0.998f),
		_V(-0.320f,-0.152f,0.8f), _V(-0.294f,-0.152f,0.8f));

	oapiVCRegisterArea(AID_HOVER_THRUST_LEVER, PANEL_REDRAW_ALWAYS, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_LBPRESSED);
	oapiVCSetAreaClickmode_Quadrilateral(AID_HOVER_THRUST_LEVER,
		_V(-0.370f,-0.152f,0.998f), _V(-0.344f,-0.152f,0.998f),
		_V(-0.370f,-0.152f,0.8f), _V(-0.344f,-0.152f,0.8f));

	oapiVCRegisterArea(AID_ATT_MODE, PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_RBDOWN);
	SetClickArea(oapiMeshGroup(meshVC, GR_ATT_MODE), AID_ATT_MODE, false);

	oapiVCRegisterArea(AID_HUD_BUTTONS, _R(1, 187, 139, 220), PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_HUD_BUTTONS), AID_HUD_BUTTONS);

	oapiVCRegisterArea(AID_BTN_UNDOCK, _R(125, 123, 187, 185), PANEL_REDRAW_INIT|PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY, PANEL_MAP_NONE, texDyn);
	SetClickArea(oapiMeshGroup(meshVC, GR_BTN_UNDOCK), AID_BTN_UNDOCK, false);

	oapiVCRegisterArea(AID_DOCK_STATUS, _R(141, 187, 254, 208), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn);

	SURFHANDLE texDyn2 = oapiGetTextureHandle(meshVC, TEX_DYNAMIC2);
	oapiVCRegisterArea(AID_CREW_SCREEN, _R(1, 441, 489, 509), PANEL_REDRAW_MOUSE|PANEL_REDRAW_USER, PANEL_MOUSE_LBDOWN|PANEL_MOUSE_ONREPLAY, PANEL_MAP_BACKGROUND, texDyn2);
	SetClickArea(oapiMeshGroup(meshVC, GR_CREW_SCREEN), AID_CREW_SCREEN);

	oapiVCRegisterArea(AID_MESSAGE_SCREEN, _R(1, 371, 489, 439), PANEL_REDRAW_USER, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);

	oapiVCRegisterArea(AID_DYNAMIC_STATUS, _R(1, 301, 279, 369), PANEL_REDRAW_ALWAYS, PANEL_MOUSE_IGNORE, PANEL_MAP_NONE, texDyn2);

	// create surfaces
	srf[0] = oapiCreateSurface(LOADBMP(IDB_BITMAP1));

	return true;
}

void ShuttlePB::SetClickArea(MESHGROUP* gr, int id, bool rect, const VECTOR3& maxShift, const VECTOR3& revert) {
	float minX, maxX, minY, maxY, minZ, maxZ, x, y, z;
	minX = minY = minZ = 1000.0f;
	maxX = maxY = maxZ = -1000.0f;
	for (DWORD i = 0; i < gr->nVtx; i++) {
		x = gr->Vtx[i].x;
		y = gr->Vtx[i].y;
		z = gr->Vtx[i].z;
		if (x < minX) minX = x;
		if (y < minY) minY = y;
		if (z < minZ) minZ = z;
		if (x > maxX) maxX = x + (float) maxShift.x;
		if (y > maxY) maxY = y + (float) maxShift.y;
		if (z > maxZ) maxZ = z + (float) maxShift.z;
	}
	if (rect) {
		VECTOR3 p1 = _V(revert.x > 0 ? maxX : minX, revert.y > 0 ? minY : maxY, revert.z > 0 ? minZ : maxZ);
		VECTOR3 p2 = _V(revert.x > 0 ? minX : maxX, revert.y > 0 ? minY : maxY, revert.z > 0 ? minZ : maxZ);
		VECTOR3 p3 = _V(revert.x > 0 ? maxX : minX, revert.y > 0 ? maxY : minY, revert.z > 0 ? maxZ : minZ);
		VECTOR3 p4 = _V(revert.x > 0 ? minX : maxX, revert.y > 0 ? maxY : minY, revert.z > 0 ? maxZ : minZ);
		oapiVCSetAreaClickmode_Quadrilateral(id, p1, p2, p3, p4);
	} else {
		double distX = Distance(minX, maxX);
		double distY = Distance(minY, maxY);
		double distZ = Distance(minZ, maxZ);
		VECTOR3 center = _V(minX + distX / 2.0f, minY + distY / 2.0f, minZ + distZ / 2.0f);
		double rad = max(distX, max(distY, distZ));
		oapiVCSetAreaClickmode_Spherical(id, center, rad);
	}
}

void ShuttlePB::SetClickArea(MESHGROUP* gr, int id, int topLeftVertex, int topRightVertex, int bottomLeftVertex, int bottomRightVertex) {
	VECTOR3 p1 = _V(gr->Vtx[topLeftVertex].x, gr->Vtx[topLeftVertex].y, gr->Vtx[topLeftVertex].z);
	VECTOR3 p2 = _V(gr->Vtx[topRightVertex].x, gr->Vtx[topRightVertex].y, gr->Vtx[topRightVertex].z);
	VECTOR3 p3 = _V(gr->Vtx[bottomLeftVertex].x, gr->Vtx[bottomLeftVertex].y, gr->Vtx[bottomLeftVertex].z);
	VECTOR3 p4 = _V(gr->Vtx[bottomRightVertex].x, gr->Vtx[bottomRightVertex].y, gr->Vtx[bottomRightVertex].z);
	if (p1.z == p2.z && p1.z == p3.z && p1.z == p4.z) {
		p1.z += 0.00001;
	}
	oapiVCSetAreaClickmode_Quadrilateral(id, p1, p2, p3, p4);
}

void ShuttlePB::clbkDrawHUD (int mode, const HUDPAINTSPEC *hps, HDC hDC) {
	VESSEL2::clbkDrawHUD (mode, hps, hDC);

	if (oapiCockpitMode() == COCKPIT_VIRTUAL) {
		// RCS mode
		switch (GetAttitudeMode()) {
			case RCS_ROT:
				TextOut(hDC, 0, hps->H - 13, "RCS ROT", 7);
				break;
			case RCS_LIN:
				TextOut(hDC, 0, hps->H - 13, "RCS LIN", 7);
				break;
			case RCS_NONE:
				TextOut(hDC, 0, hps->H - 13, "RCS OFF", 7);
				break;
		}

		if (oapiGetHUDMode() == HUD_SURFACE) {
			// vertical spped
			char buf[30];
			sprintf(buf, "%0.1lfm/s", vspeed);
			TextOut(hDC, hps->W - 80, hps->H - 13, buf, strlen(buf));
		} else if (DockingStatus(0) == 0 && GetAtmPressure() < 0.1) {
			// acceleration
			VECTOR3 forceVector, weightVector;
			GetForceVector(forceVector);
			GetWeightVector(weightVector);
			char buf[30];
			sprintf(buf, "ACCEL %0.1lf", length(forceVector - weightVector) / GetMass());
			TextOut(hDC, hps->W - 70, hps->H - 13, buf, strlen(buf));
		}
	}
}

// --------------------------------------------------------------
// Respond to virtual cockpit mouse events
// --------------------------------------------------------------
bool ShuttlePB::clbkVCMouseEvent (int id, int event, VECTOR3 &p) {
	int index;
	float thrust;
	switch (id) {
		case AID_MFD1_BUTTONS:
		case AID_MFD2_BUTTONS:
		case AID_MFD3_BUTTONS:
			if (p.x > 0.07 && p.x < 0.93) return false;
			index = (int) (p.y * 11);
			if (index == 0 || index / 2 != (index - 1) / 2) {
				index /= 2;
				if (p.x > 0.5) index += 6;
				oapiProcessMFDButton(id / 2, index, event);
				return true;
			} else {
				return false;
			}
		case AID_MFD1_MBUTTONS:
		case AID_MFD2_MBUTTONS:
		case AID_MFD3_MBUTTONS:
			if (event & PANEL_MOUSE_LBDOWN) {
				if (p.x < 0.1) {
					oapiToggleMFD_on(id / 2);
					return true;
				} else if (p.x > 0.9) {
					oapiSendMFDKey(id / 2, OAPI_KEY_GRAVE);
					return true;
				} else if (p.x > 0.8) {
					oapiSendMFDKey(id / 2, OAPI_KEY_F1);
					return true;
				}
				return false;
			}
		case AID_BTN_KILLROT:
		case AID_BTN_HORLEVEL:
		case AID_BTN_PROGRADE:
		case AID_BTN_RETROGRADE:
		case AID_BTN_ORBNORMAL:
		case AID_BTN_ORBANORMAL:
		case AID_BTN_HOLDALT:
			ToggleNavmode(id - 10);
			return true;
		case AID_MAIN_THRUST_LEVER:
		case AID_HOVER_THRUST_LEVER:
			thrust = (float)(1 - p.y);
			if (thrust < 0.0f) thrust = 0.0f;
			if (thrust > 1.0f) thrust = 1.0f;
			SetThrusterLevel(id == AID_MAIN_THRUST_LEVER ? th_main : th_hover, thrust);
			return true;
		case AID_ATT_MODE:
			index = GetAttitudeMode();
			if (index > 0 && event == PANEL_MOUSE_LBDOWN) {
				SetAttitudeMode(index - 1);
			} else if (index < 2 && event == PANEL_MOUSE_RBDOWN) {
				SetAttitudeMode(index + 1);
			}
			return true;
		case AID_HUD_BUTTONS:
			if (p.x < 0.25) {
				oapiSetHUDMode(HUD_ORBIT);
			} else if (p.x < 0.5) {
				oapiSetHUDMode(HUD_SURFACE);
			} else if (p.x < 0.75) {
				oapiSetHUDMode(HUD_DOCKING);
			} else {
				oapiSetHUDMode(HUD_NONE);
			}
			return true;
		case AID_BTN_UNDOCK:
			Undock(ALLDOCKS);
			return true;
		case AID_CREW_SCREEN:
			HandleCrewScreenClick(p);
			return true;
		case AID_GEAR_LEVER:
			if (!GroundContact()) {
				if (gearStatus == CLOSED || gearStatus == CLOSING) {
					gearStatus = OPENING;
				} else {
					gearStatus = CLOSING;
				}
			}
			return true;
		case AID_DOCK_SWITCH:
			if (p.y < 0.5 && (dockStatus == CLOSED || dockStatus == CLOSING)) {
				dockStatus = OPENING;
			} else if (p.y > 0.5 && (dockStatus == OPEN || dockStatus == OPENING)) {
				dockStatus = CLOSING;
			}
			return true;
	}
	return false;
}

void ShuttlePB::clbkMFDMode(int mfd, int mode) {
	oapiVCTriggerRedrawArea(-1, mfd * 2);
}

void ShuttlePB::clbkNavMode(int mode, bool active) {
	int gr;
	switch (mode) {
		case NAVMODE_KILLROT: gr = GR_BTN_KILLROT; break;
		case NAVMODE_HLEVEL: gr = GR_BTN_HORLEVEL; break;
		case NAVMODE_PROGRADE: gr = GR_BTN_PROGRADE; break;
		case NAVMODE_RETROGRADE: gr = GR_BTN_RETROGRADE; break;
		case NAVMODE_NORMAL: gr = GR_BTN_ORBNORMAL; break;
		case NAVMODE_ANTINORMAL: gr = GR_BTN_ORBANORMAL; break;
		case NAVMODE_HOLDALT: gr = GR_BTN_HOLDALT; break;
	}
	UpdateNavmodeButton(mode, gr);
}

void ShuttlePB::clbkHUDMode(int mode) {
	oapiVCTriggerRedrawArea(-1, AID_HUD_BUTTONS);
}

void ShuttlePB::clbkRCSMode(int mode) {
	oapiVCTriggerRedrawArea(-1, AID_ATT_MODE);
}

void ShuttlePB::UpdateNavmodeButton(int navmode, int meshGroup) {
	VISHANDLE visHandle = *oapiObjectVisualPtr(GetHandle());
	if (visHandle == NULL) return;
	DEVMESHHANDLE mesh = GetDevMesh(visHandle, 1);
	if (mesh == NULL) {
		oapiWriteLog("ShuttlePB::UpdateNavmodeButton: mesh is null");
		return;
	}

	bool state = GetNavmodeState(navmode);

	GROUPREQUESTSPEC grs = { NULL, NULL, NULL, 0, 0, 0, 0, 0 };
	if (oapiGetMeshGroup(mesh, meshGroup, &grs) != 0) {
		oapiWriteLog("ShuttlePB::UpdateNavmodeButton: mesh group is not retrieved");
		return;
	}

	// light
	MATERIAL mat = {};
	oapiMeshMaterial(mesh, meshGroup, &mat);
	if (state) {
		mat.emissive.r = mat.emissive.g = mat.emissive.b = 1.0f;
	}
	else {
		mat.emissive.r = mat.emissive.g = mat.emissive.b = 0.25f;
	}
	oapiSetMaterial(mesh, grs.MtrlIdx, &mat);

	// shift
	/*VECTOR3 pos = _V(gr->Vtx[0].x, gr->Vtx[0].y, gr->Vtx[0].z);
	bool shifted = !IsEqual(pos, initPos.find(meshGroup)->second);
	if (state != shifted) {
		MESHGROUP_TRANSFORM transform;
		transform.nmesh = 1;
		transform.ngrp = meshGroup;
		transform.transform = MESHGROUP_TRANSFORM::TRANSLATE;
		if (state) {
			transform.P.transparam.shift = _V(0, 0, 0.001);
		} else {
			transform.P.transparam.shift = _V(0, 0, -0.001);
		}
		MeshgroupTransform(visHandle, transform);
	}*/
}

// --------------------------------------------------------------
// Draw the virtual cockpit instruments
// --------------------------------------------------------------
bool ShuttlePB::clbkVCRedrawEvent(int id, int event, SURFHANDLE surf) {
	switch (id) {
		case AID_MFD1_BUTTONS:
		case AID_MFD2_BUTTONS:
		case AID_MFD3_BUTTONS:
			RedrawMFDButtons(surf, id / 2);
			return true;
		case AID_MAIN_THRUST_LEVER:
			/*if (GetThrusterLevel(th_main) != prevThrust[0]) {
				SetAnimation(animMainThrust, GetThrusterLevel(th_main));
			}
			prevThrust[0] = GetThrusterLevel(th_main);*/
			return true;
		case AID_HOVER_THRUST_LEVER:
			/*if (GetThrusterLevel(th_hover) != prevThrust[1]) {
				SetAnimation(animHoverThrust, GetThrusterLevel(th_hover));
			}
			prevThrust[1] = GetThrusterLevel(th_hover);*/
			return true;
		case AID_DYNAMIC_STATUS:
			RedrawDynamicStatus(surf);
			return true;
		case AID_ATT_MODE:
			SetAnimation(animRCSMode, GetAttitudeMode() * 0.5);
			return true;
		case AID_MESSAGE_SCREEN:
			RedrawMessageScreen(surf);
			return true;
		case AID_CREW_SCREEN:
			RedrawCrewScreen(surf);
			return true;
		case AID_HUD_BUTTONS:
			RedrawHudButtons(surf);
			return true;
		case AID_GEAR_LEVER:
			SetAnimation(animGearLever, gearStatus == OPEN || gearStatus == OPENING);
			return true;
		case AID_DOCK_SWITCH:
			SetAnimation(animDockSwitch, dockStatus == OPEN || dockStatus == OPENING);
			return true;
		case AID_DOCK_STATUS:
			RedrawDockStatus(surf);
			return true;
		case AID_BTN_UNDOCK:
			oapiBlt(surf, srf[0], 0, 0, DockingStatus(0) ? 62 : 0, 0, 62, 62);
			return true;
	}
	return false;
}

void ShuttlePB::RedrawMFDButtons(SURFHANDLE surf, int mfd) {
	int btnWidth = 30;
	int btnHeight = 17;

	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 2 * btnWidth, 102);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[0]);
	SetTextColor(hDC, RGB(255, 255, 255));
	SetTextAlign(hDC, TA_CENTER);
	
	const char *label;
	for (int side = 0; side < 2; side++) {
		for (int bt = 0; bt < 6; bt++) {
			if (label = oapiMFDButtonLabel(mfd, side * 6 + bt)) {
				TextOut(hDC, side * btnWidth + btnWidth / 2, bt * btnHeight + btnHeight / 2 - 5, label, strlen(label));
			} else break;
		}
	}
	oapiReleaseDC(surf, hDC);
}

void ShuttlePB::RedrawDynamicStatus(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 279, 69);
	SetBkMode(hDC, TRANSPARENT);

	SetTextAlign(hDC, TA_CENTER);

	SelectObject(hDC, g_Param.hFont[2]);
	SetTextColor(hDC, RGB(0, 150, 150));

	TextOut(hDC, 70, 5, "Fuel level", 10);

	SelectObject(hDC, g_Param.hFont[1]);
	int info = ((int) fmod(oapiGetSimTime(), 20.0)) / 10;
	switch (info) {
		case 0:
			TextOut(hDC, 210, 5, "Max dV in vacuum", 16);
			break;
		case 1:
			TextOut(hDC, 210, 5, "Ship mass", 9);
			break;
	}

	DebugFloat("max propellant: ", GetPropellantMaxMass(ph_main));
	DebugFloat("current propellant: ", GetPropellantMass(ph_main));
	SelectObject(hDC, g_Param.hFont[3]);
	SetTextColor(hDC, RGB(0, 255, 0));
	char text[20];
	sprintf(text, "%0.1lf%%", GetPropellantMass(ph_main) * 100 / GetPropellantMaxMass(ph_main));
	TextOut(hDC, 70, 20, text, strlen(text));

	SelectObject(hDC, g_Param.hFont[2]);
	switch (info) {
		case 0:
			sprintf(text, "%0.0lf m/s", PB_ISP * log(GetMass() / GetEmptyMass()));
			TextOut(hDC, 210, 20, text, strlen(text));
			break;
		case 1:
			sprintf(text, "%0.1lf kg", GetMass());
			TextOut(hDC, 210, 20, text, strlen(text));
			break;
	}

	oapiReleaseDC(surf, hDC);
}

void ShuttlePB::ShowMessage(string newMessage, COLORREF newColor) {
	if (messages.size() > 3) {
		messages.pop_front();
	}
	Message message;
	message.message = newMessage;
	message.color = newColor;
	messages.push_back(message);
	oapiVCTriggerRedrawArea(-1, AID_MESSAGE_SCREEN);
}

void ShuttlePB::RedrawMessageScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 489, 69);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[1]);
	SetTextAlign(hDC, TA_LEFT);

	list<Message>::iterator msg;
	int line = 0;
	for (msg = messages.begin(); msg != messages.end(); ++msg) {
		SetTextColor(hDC, msg->color);
		TextOut(hDC, 5, 2 + 16 * line, msg->message.data(), msg->message.length());
		line++;
	}

	oapiReleaseDC(surf, hDC);
}

void ShuttlePB::RedrawCrewScreen(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[2]);
	SetTextAlign(hDC, TA_CENTER);
	SetTextColor(hDC, RGB(200, 200, 255));

	char text[100];
	sprintf(text, "%s", crew.GetCrewNameBySlotNumber(selectedCrewMember));
	TextOut(hDC, 262, 9, text, strlen(text));
	sprintf(text, "%d / %d", crew.GetCrewTotalNumber() > 0 ? selectedCrewMember + 1 : 0, crew.GetCrewTotalNumber());
	TextOut(hDC, 455, 9, text, strlen(text));
	sprintf(text, "%s", UmmuFunctionName[selectedSuit]);
	TextOut(hDC, 262, 40, text, strlen(text));

	oapiReleaseDC(surf, hDC);
}

void ShuttlePB::HandleCrewScreenClick(VECTOR3 &p) {
	int x = (int) (488 * p.x);
	int y = (int) (68 * p.y);
	if (IsPointInRect(x, y, 115, 9, 145, 24)) {
		selectedCrewMember--;
		if (selectedCrewMember < 0) selectedCrewMember = 
			crew.GetCrewTotalNumber() > 0 ? crew.GetCrewTotalNumber() - 1 : 0;
	} else if (IsPointInRect(x, y, 389, 9, 419, 24)) {
		selectedCrewMember++;
		if (selectedCrewMember >= crew.GetCrewTotalNumber()) selectedCrewMember = 0;
	} else if (IsPointInRect(x, y, 115, 40, 145, 55)) {
		selectedSuit--;
		if (selectedSuit < 0) selectedSuit = NUMBER_OF_MMU_TYPE - 1;
	} else if (IsPointInRect(x, y, 389, 40, 419, 55)) {
		selectedSuit++;
		if (selectedSuit >= NUMBER_OF_MMU_TYPE) selectedSuit = 0;
	} else if (IsPointInRect(x, y, 424, 40, 484, 55)) {
		if (crew.GetCrewTotalNumber() > 0) {
			//double crewMemberMass = crew.GetCrewWeightBySlotNumber(selectedCrewMember);
			if (crew.EvaCrewMember(crew.GetCrewNameBySlotNumber(selectedCrewMember)) > 0) {
				string msg = crew.GetLastEvaedCrewName();
				//SetEmptyMass(GetEmptyMass() - crewMemberMass);
				msg.append(" has left the ship");
				ShowMessage(msg);
				if (selectedCrewMember >= crew.GetCrewTotalNumber() && crew.GetCrewTotalNumber() > 0) {
					selectedCrewMember = crew.GetCrewTotalNumber() - 1;
				}
			}
		}
	}
}

void ShuttlePB::RedrawHudButtons(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[1]);
	Rectangle(hDC, 0, 0, 139, 34);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hBrush[2]);
	int hudMode = oapiGetHUDMode();
	if (hudMode == HUD_ORBIT) {
		Rectangle(hDC, 0, 0, 34, 34);
	} else if (hudMode == HUD_SURFACE) {
		Rectangle(hDC, 35, 0, 69, 34);
	} else if (hudMode == HUD_DOCKING) {
		Rectangle(hDC, 70, 0, 104, 34);
	} else if (hudMode == HUD_NONE) {
		Rectangle(hDC, 105, 0, 139, 34);
	}

	SelectObject(hDC, g_Param.hFont[0]);
	SetTextAlign(hDC, TA_CENTER);
	SetTextColor(hDC, RGB(0, 0, 0));
	TextOut(hDC, 17, 10, "Orbit", 5);
	TextOut(hDC, 52, 10, "Srfc", 4);
	TextOut(hDC, 87, 10, "Dock", 4);
	TextOut(hDC, 122, 10, "Off", 3);

	oapiReleaseDC(surf, hDC);
}

void ShuttlePB::RedrawDockStatus(SURFHANDLE surf) {
	HDC hDC = oapiGetDC(surf);

	SelectObject(hDC, g_Param.hBrush[0]);
	Rectangle(hDC, 0, 0, 114, 22);
	SetBkMode(hDC, TRANSPARENT);

	SelectObject(hDC, g_Param.hFont[1]);
	SetTextAlign(hDC, TA_CENTER);
	SetTextColor(hDC, RGB(0, 255, 0));

	switch (dockStatus) {
		case OPEN:
			TextOut(hDC, 57, 3, "Deployed", 8);
			break;
		case CLOSED:
			TextOut(hDC, 57, 3, "Retracted", 9);
			break;
		case OPENING:
			if (fmod(oapiGetSimTime(), 1.0) < 0.5) TextOut(hDC, 57, 3, "Deploying", 9);
			break;
		case CLOSING:
			if (fmod(oapiGetSimTime(), 1.0) < 0.5) TextOut(hDC, 57, 3, "Retracting", 10);
			break;
	}

	oapiReleaseDC(surf, hDC);
}
